###########
Change Log
###########

All notable changes to this project will be documented in this file.  
This project adheres to `Semantic Versioning <http://semver.org/>`_.


## [0.30.1]
***********
### Fixed

* Fixed configure-dish impmlementation as per ADR-63

## [0.30.0]
***********
### Added

* FQDN Updated as per ADR-9

## [0.29.1]
***********

### fixed

* Resolved bug SKB-709

## [0.29.0]
***********

### Added

* Implementation of SetAdminMode command on SubarrayNode

## [0.28.0]
***********

### Added
- ADR-63 based changes in configure command implementations

## [0.27.6]
***********

### Fixed
- Common tag 0.24.3 updated for observer fix  

## [0.27.5]
***********

### Fixed
- Resolved bug SKB-658

## [0.27.4]
***********

### Fixed
- Bug fix for SKB-646
- Fixed timeout issue in subsequent configure commands

## [0.27.3]
***********

### Added
- Introduced Error Propagation for Scan /End Scan /End commands

## [0.27.2]
***********

### Added
- Utilised ska-tmc-cdm v.12.6.0

## [0.27.1]
***********

### Fixed
- assign resources attribute is updated once assign resource command completed

## [0.27.0]
***********

### Fixed
- Improved the obsState aggregation rules on SubarrayNode
- Fixed ABORTED obsState aggregation related issues
- Fixed Configure CSP Schema generation related issue

## [0.26.6]
***********

### Added
- Updated configure json to v.4.1(ADR-99)

## [0.26.5]
***********

### Added
- Updated configure json to v.4.1(ADR-99)

## [0.26.4]
***********

### Added
- Fix bug SKB-634

## [0.26.3]
***********

### Added
- Included dish LongRunningCommandResult events while reporting LongRunningCommandResult of SubarrayNode

## [0.26.2]
***********

### Added
- Included dish LongRunningCommandResult events while reporting LongRunningCommandResult of SubarrayNode

## [0.26.1]
***********

### Fixed
- Fixed SKB-643

## [0.26.0]
***********

### Added
- Scan command is updated to perform mapping scans

## [0.25.1]
***********

### Fixed
- Fixed the is_operation_aborted flag bug by utilizing a variable from command class.

## [0.25.0]
***********

### Added
- Updated ska-tmc-common v0.21.0 to utilize latest command tracker using events.
- CDM tag updated to 12.3.0
- Holography mosaic pattern validation added

## [0.24.0]
***********

### Added
- ska-tmc-common updated to 0.20.2
- accomodated changes for liveliness probe

## [0.23.4]
***********

### Fixed
- Resolve SKB-509 TMC support CSP configure v.3.0

## [0.23.3]
***********

### Added
- Resolve SKB-512 to allow SubarratyNode.EndScan() to execute on other Leaf Nodes even if one Leaf Node reports failure
- Added subscription point for PST Beam Delay Model in Configure JSON.

## [0.23.2]
***********

### Added
- Resolve SKB-511 to Allow SubarratyNode.Abort() in all Dish pointingStates

## [0.23.1]
***********

### Added
- Obs State Aggregator Logic improved
- Rule engine is used to define rules for ObsState
- EventDataStorage class introduced to store event related data

## [0.23.0]
***********

### Added
- Obs State Aggregator Logic improved
- Rule engine is used to define rules for ObsState
- EventDataStorage class introduced to store event related data

## [0.23.0-rc]
**************

### Added
- Obs State Aggregator Logic improved
- Rule engine is used to define rules for ObsState
- EventDataStorage class introduced to store event related data

## [0.22.3]
***********

### Added
- Utilised SKA Tel Model v1.19.2 with applied bug fix SKB-477(REL-1665)
- Verified low SubarrayNode to reject Scan Command with unnecessary subarray_id in Scan schema
- Added unit test for verification
- Implemeted BDD XTP-60776 for SKB-477 verification

## [0.22.2]
***********

### Added
- Fixed issue[SKB-476] by updating configure command to make timing beams key optional in configure json.

## [0.22.1]
***********

### Added
- Enable polling of all attributes by setting and pushing archive events to resolve SKB-434

## [0.22.0]
***********

### Added
- Implemented Mid Configure schema changes as per ADR-99
- Added Configure example for v4.0
- To support ADR-99, updated Configure command for TMC to add -> subarray_id, output_link_map and delay_model_subscription_point to midcbf configuration
- Utilised SKA Tel Model v1.19.0 for Configure ADR-99 schema validations
- Added/updated unit tests to verify
- Upgraded and added integration tests to verify ADR-99 schema for mid TMC-CSP
- Utilised ska-tmc-common v0.18.0 supporting ADR-99

## [0.21.4]
***********

### Fixed
- Update long running command result after abort command

## [0.21.3]
***********

### Added
- Created JonesURI device proeprty and sent as jones key value
- Added PSS and PSt key values in csp input json.
- Added test cases for CSP json validation and invalid jones key.
- Update the ska-telmodel version to 1.18.2

## [0.21.2]
***********

### Added
- Sync master with tag 0.19.1 updates

## [0.21.1]
***********

### Fixed
- Fixed issue in Abort command related to update_task_status method

## [0.21.0]
***********

### Added
- Updated version of CDM to support validations for Non-sidereal tracking

## [0.20.1]
***********

### Added
- Enable obsState check for Abort command

## [0.20.0]
***********

### Added
- Utilised base class v1.0.0 and pytango 9.5.0
- Utilised ska-tmc-common v0.17.1
- Implemented queue according to support base classes v1.0.0
- Refactored command allowed method to put commands in queue
- Implemented command allowed methods for observation specific commands to allow/reject the queued  task, ResultCode.NOT_ALLOWED/ResultCode.REJECTED
- Refactored error propogation implementation to handle longrunningcommandresult attribute new format in case of raised exceptions
- Refactored error propogation implementation to handle longrunningcommandresult event for ResultCode.NOT_ALLOWED, ResultCode.REJECTED and ResultCode.FAILED
- Refactored all command unit test cases to support task_callback result format as tuple (result, message)
- Parametrized unit tests to avoid duplication
- Refactored integration tests for all observation specific commands with longrunningcommandresult attribute assertions
- Refactored intergration tests for error propogation to handle ResultCode.REJECTED
- Implemented unit tests to varify StateModelError in case of not permitted obsStates

## [0.19.1]
***********

### Added
- Utilised OSO-TMC low AssignResources v4.0(supporting TMC-MCCS v3.0) and Configure schema v4.0 (PST observations)
- Utilised Tel Model validations for AssignResources, Configure and Scan schema
- Modified unit test cases to support tel model validations
- Upgraded integration tests for invalid json key Configure and Scan scenario according to Tel Model Validations

## [0.19.0]
***********

### Added
- SDP Queue connector FQDN received as a part of receive address event set on Dish Leaf Node

## [0.18.2]
***********

### Fixed
- Fix for Abort() command not getting invoked on the DishLeafNode

## [0.18.1]
***********

### Added
- Updated device property to include MccsScanInterfaceURL
- Removed start_time that is optional key

## [0.18.0]
***********

### Added
- Added K-Value validation in AssignResources command. The command passes if the K-Value are all same or all different.

## [0.17.7]
***********

### Added
- Using **CDM** v9.1.0 to allow **CORRECTION** key in **Configure** schema for **MID** telescope

## [0.17.6]
***********

### Added
- Bug fix on interface URL harcodeding for CSP, SDP and MCCS.
- Updated device property LowCspAssignResourcesInterfaceURL -> CspAssignResourcesInterfaceURL to set interface version for Low and Mid CSP AssignResources command.
- Added new device property CspScanInterfaceURL to set interface version for Low and Mid CSP Scan command.
- Added new device property SdpScanInterfaceURL to set interface version for Low and Mid SDP Scan command.
- Added new device property MccsConfigureInterfaceURL to set interface version for Low MCCS Configure command.

## [0.17.5]
***********

### Added
- Update sdp interface version in configure command.
- Added new device property LowCspAssignResourcesInterfaceURL to set interface version for AssignResources command.

## [0.17.4]
***********

### Fixed
- Fix for multiple events of long running command result is added

## [0.17.3]
***********

### Added
- Utilised dishMode and pointingState attributes from DishLeafNode instead from DisMaster device.

## [0.17.2]
***********

### Added
- Updated SubarrayNode to work with dish-lmc chart 3.0.0
- Added a DishMasterIdentifier property as an indicator to identify dish device

## [0.17.1]
***********

### Fixed
- Abort timeout issue resolved
- ObsState and LRCR event processed as soon as it is received to avoid delay
- Scan command aggregation issue resolved
- Updated .readthedocs.yaml and Pyproject.toml to fix the RTD generation issue
- Removed locks from component manager
- Deploying mocks in seperate instances
- Improved logging for the aggregation logic

## [0.17.0]
***********

### Added
- Using **CDM** for **Configure** command validations for **LOW** telescope. Custom validations are present for the **tmc** key.

## [0.16.0]
***********

### Added
- Updated Scan Command interface to pass scan_id as a json argument for DishLeafNode and update **scanID** attribute of Subarray Node.
- Updated EndScan Command interface to reset **scanID** attribute and invoke EndScan command on DishLeafNode from Subarray Node.

## [0.15.0]
***********

### Added
- **Error Propagation** and **Timeout** functionality is enabled for **MCCS Subarray Leaf Node** devices
- **EventReceiver** class is refactored to improve commonality and code quality
- Method names are updated to be more inline with the function performed
- Clean up for **ReleaseAllResources**, **Failed AssignResources** and **Restart** command is optimized.
- Using **HelperSubarrayLeafDevice** for MCCS SLN mock
- Devices will be removed from monitoring list of liveliness probe if they are not to be monitored any more.

### Fixed
- Multiple subscriptions in **EventReceiver** are fixed
