# SubarrayNode

This project is developing the SubarrayNode component of Telescope Monitoring and Control (TMC) system, for the Square Kilometre Array. Subarray Node is a coordinator observation specific operations.

Documentation
-------------
[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-tmc-subarraynode/badge/?version=latest)](https://developer.skao.int/projects/ska-tmc-subarraynode/en/latest/)

The documentation for this project, including how to get started with it, can be found in the `docs` folder, and can be better browsed in the SKA development portal:

* [SubarrayNode documentation](https://developer.skatelescope.org/projects/ska-tmc-subarraynode/en/latest  "SKA Developer Portal: SubarrayNode documentation")


