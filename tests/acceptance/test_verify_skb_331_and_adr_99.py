""" This module has test case to verify skb-331 and ADR-99 changes."""
import json

import pytest
from pytest_bdd import given, scenarios, then, when
from ska_tango_base.control_model import ObsState

from tests.integration.common import (
    ensure_checked_devices,
    simulate_configure_on_dish_master,
)
from tests.settings import get_input_json_from_command_call_info


@given("a SubarrayNode device in obsState IDLE")
def check_subarray_node_obsState_idle(
    subarray_node, event_recorder, json_factory
):
    ensure_checked_devices(subarray_node.subarray_node)
    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)
    assign_str = json_factory("AssignResources_mid")
    assign_id = subarray_node.invoke_command("AssignResources", assign_str)
    subarray_node.assert_command_completion(assign_id, event_recorder)
    assert subarray_node.subarray_node.obsState == ObsState.IDLE


@when("I invoke configure command on SubarrayNode")
def invoke_configure_command(subarray_node, event_recorder, json_factory):
    configure_str = json_factory("Configure_mid")
    configure_id = subarray_node.invoke_command("Configure", configure_str)
    pytest.input_json = get_input_json_from_command_call_info(
        subarray_node, "Configure"
    )
    simulate_configure_on_dish_master()
    subarray_node.assert_command_completion(configure_id, event_recorder)


@then(
    "SubarrayNode invokes configure on csp with json "
    + "containing output_host and output_port"
)
def check_csp_json(subarray_node):
    processing_list = json.loads(pytest.input_json)["midcbf"]["correlation"][
        "processing_regions"
    ]
    for pr_config in processing_list:
        assert "output_host" in pr_config.keys()
        assert "output_port" in pr_config.keys()
        assert "output_link_map" in pr_config.keys()


@then("SubarrayNode obsState moves to READY")
def check_subarray_node_obs_state_ready(subarray_node):
    subarray_node.subarray_node.obsState == ObsState.READY


scenarios("../features/XTP-42680_skb-331.feature")
