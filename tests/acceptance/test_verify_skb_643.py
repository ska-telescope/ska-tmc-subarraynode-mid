"""
This module contains BDD test cases to verify the assign resources
functionality on the Subarray Node as part of SKB-643 changes.

The test cases are parameterized to handle different CSP resources
(`pst` and `pss`)
"""
import json
from copy import deepcopy

import pytest
from pytest_bdd import given, parsers, scenarios, then, when
from ska_tango_base.control_model import ObsState

from tests.integration.common import (
    ensure_checked_devices,
    set_mccs_subarray_ln_obsstate,
)
from tests.settings import get_input_json_from_command_call_info, logger

# Parameterize the feature file scenarios
scenarios("../features/skb_643.feature")

# Dictionary mapping for resource-specific parameters
RESOURCE_CONFIG = {
    "pst": {
        "beams_id_key": "pst_beams_ids",
        "beams_id_values": [1],
    },  # Use 'beams_id' as expected after processing
    "pss": {
        "beams_id_key": "pss_beams_ids",
        "beams_id_values": [1, 2, 3],
    },  # Same for 'pss'
}


@given("Subarray Node is in observation state EMPTY")
def check_subarray_node_obsState_empty(subarray_node, event_recorder):
    ensure_checked_devices(subarray_node.subarray_node)
    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)
    assert subarray_node.subarray_node.obsState == ObsState.EMPTY


@when(
    parsers.parse(
        "I invoke the assign command on Subarray Node with "
        + "only {resource_type} resource"
    )
)
def invoke_assign_with_resource(
    subarray_node, event_recorder, json_factory, resource_type
):
    config = RESOURCE_CONFIG[resource_type]

    # Parse the JSON string to a dictionary if `json_factory` returns a JSON string
    assign_str = json_factory("AssignResources_low")
    assign_data = deepcopy(
        json.loads(assign_str) if isinstance(assign_str, str) else assign_str
    )

    if "csp" not in assign_data:
        assign_data["csp"] = {}

    assign_data["csp"][resource_type] = {
        config["beams_id_key"]: config["beams_id_values"]
    }
    # Convert assign_data to JSON string if needed
    assign_data_json = json.dumps(assign_data)

    # Invoke the assign command with the JSON string and assert completion
    assign_id = subarray_node.invoke_command(
        "AssignResources", assign_data_json
    )
    set_mccs_subarray_ln_obsstate(ObsState.IDLE)
    subarray_node.assert_command_completion(assign_id, event_recorder)

    pytest.input_json = get_input_json_from_command_call_info(
        subarray_node, "AssignResources"
    )


@then("SubarrayNode invokes assign on csp with json containing beams_id")
def check_csp_remapped_beams_id_json(subarray_node):
    # Extract the relevant JSON input for the test from the subarray node
    input_json = json.loads(pytest.input_json)

    # Navigate to the 'csp' section of the JSON
    argin_csp = input_json.get("csp", {})

    # Check for "pss" and verify the presence of "beams_id"
    if "pss" in argin_csp:
        pss = argin_csp["pss"]
        # After assign is invoked, simply check for the presence of "beams_id"
        assert (
            "beams_id" in pss
        ), "'beams_id' not found in pss after assignment"

    # Check for "pst" and verify the presence of "beams_id"
    if "pst" in argin_csp:
        pst = argin_csp["pst"]
        # After assign is invoked, simply check for the presence of "beams_id"
        assert (
            "beams_id" in pst
        ), "'beams_id' not found in pst after assignment"


@then("Subarray Node transitions to observation state IDLE")
def check_subarray_node_obs_state_idle(subarray_node):
    assert subarray_node.subarray_node.obsState == ObsState.IDLE
