""" This module has test case to verify skb-476 changes."""
import json

from pytest_bdd import given, scenarios, then, when
from ska_tango_base.control_model import ObsState

from tests.integration.common import (
    ensure_checked_devices,
    set_mccs_subarray_ln_obsstate,
)


@given("Subarray Node in observation state IDLE")
def check_subarray_node_obsState_idle(
    subarray_node, event_recorder, json_factory
):
    ensure_checked_devices(subarray_node.subarray_node)
    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)
    assign_str = json_factory("AssignResources_low")
    assign_id = subarray_node.invoke_command("AssignResources", assign_str)
    set_mccs_subarray_ln_obsstate(ObsState.IDLE)
    subarray_node.assert_command_completion(assign_id, event_recorder)
    assert subarray_node.subarray_node.obsState == ObsState.IDLE


@when(
    "I invoke configure command on Subarray Node without "
    + "key `timing_beams` in csp json"
)
def check_csp_json(subarray_node, event_recorder, json_factory):
    configure_str = json_factory("Configure_low_without_timing_beams")
    assert "timing_beams" not in list(
        json.loads(configure_str)["csp"]["lowcbf"].keys()
    )
    configure_id = subarray_node.invoke_command("Configure", configure_str)

    subarray_node.assert_command_completion(configure_id, event_recorder)


@then("Subarray Node transitions to observation state READY")
def check_subarray_node_obs_state_ready(subarray_node):
    subarray_node.subarray_node.obsState == ObsState.READY


scenarios("../features/XTP_59919_skb_476.feature")
