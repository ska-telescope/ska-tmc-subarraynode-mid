import json

import pytest
import tango
from ska_tango_base.commands import ResultCode
from ska_tmc_common.dev_factory import DevFactory

from tests.integration.common import ensure_checked_devices
from tests.settings import logger
from tests.test_helpers.constants import (
    LOW_SUBARRAY_NODE_NAME,
    MID_SUBARRAY_NODE_NAME,
)


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_off_command_mid(
    tango_context,
    change_event_callbacks,
    set_mid_sdp_csp_leaf_node_availability_for_aggregation,
):
    logger.info("%s", tango_context)
    dev_factory = DevFactory()
    subarray_node = dev_factory.get_device(MID_SUBARRAY_NODE_NAME)
    ensure_checked_devices(subarray_node)

    result, unique_id = subarray_node.On()
    assert unique_id[0].endswith("On")
    assert result[0] == ResultCode.QUEUED

    subarray_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["longRunningCommandResult"],
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandResult",
        (unique_id[0], json.dumps((int(ResultCode.OK), "Command Completed"))),
        lookahead=4,
    )

    result, unique_id = subarray_node.Off()
    assert unique_id[0].endswith("Off")
    assert result[0] == ResultCode.QUEUED

    subarray_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["longRunningCommandResult"],
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandResult",
        (unique_id[0], json.dumps((int(ResultCode.OK), "Command Completed"))),
        lookahead=6,
    )


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_off_command_low(
    tango_context,
    change_event_callbacks,
    set_low_sdp_csp_leaf_node_availability_for_aggregation,
):
    logger.info("%s", tango_context)
    dev_factory = DevFactory()
    subarray_node = dev_factory.get_device(LOW_SUBARRAY_NODE_NAME)
    ensure_checked_devices(subarray_node)

    result, unique_id = subarray_node.On()
    assert unique_id[0].endswith("On")
    assert result[0] == ResultCode.QUEUED

    subarray_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["longRunningCommandResult"],
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandResult",
        (unique_id[0], json.dumps((int(ResultCode.OK), "Command Completed"))),
        lookahead=2,
    )

    result, unique_id = subarray_node.Off()
    assert unique_id[0].endswith("Off")
    assert result[0] == ResultCode.QUEUED

    subarray_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["longRunningCommandResult"],
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandResult",
        (unique_id[0], json.dumps((int(ResultCode.OK), "Command Completed"))),
        lookahead=4,
    )
