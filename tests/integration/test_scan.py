import json
import time

import pytest
from ska_control_model import ObsState

from tests.integration.common import (
    ensure_checked_devices,
    set_mccs_subarray_ln_obsstate,
    simulate_configure_on_dish_master,
)
from tests.settings import (
    ERROR_PROPAGATION_DEFECT,
    EXCEPTION_ALL_DISH,
    EXCEPTION_DISH,
    EXCEPTION_LOW_CSP,
    EXCEPTION_LOW_CSP_SDP,
    EXCEPTION_LOW_SDP,
    EXCEPTION_MCCS,
    EXCEPTION_MID_CSP,
    EXCEPTION_MID_SDP,
    FAILED_RESULT_DEFECT,
    FAILED_RESULT_EXCEPTION,
    TIMEOUT_DEFECT,
    TIMEOUT_DEFECT_DISH,
    TIMEOUT_EXCEPTION,
    logger,
)
from tests.test_helpers.common_utils import SubarrayNode
from tests.test_helpers.constants import (
    DISH_LEAF_NODE,
    DISH_LEAF_NODE36,
    DISH_LEAF_NODE100,
    LOW_CSPSUBARRAY_LEAF_NODE,
    LOW_SDPSUBARRAY_LEAF_NODE,
    MCCS_SUBARRAY_LEAF_NODE,
    MID_CSPSUBARRAY_LEAF_NODE,
    MID_SDPSUBARRAY_LEAF_NODE,
    MID_SUBARRAY_NODE_NAME,
)


def mapping_scan_command_failed(
    subarray_node: SubarrayNode,
    inputs: dict,
    event_recorder,
    defective_device_names,
):
    """Validate mapping scan is stopped
    after scan is invoked on failed device
    """
    try:
        ensure_checked_devices(subarray_node.subarray_node)

        on_id = subarray_node.invoke_command("On")
        subarray_node.assert_command_completion(on_id, event_recorder)

        assign_id = subarray_node.invoke_command(
            "AssignResources", inputs["ASSIGN"]
        )

        configure_json = json.loads(inputs["CONFIGURE"])

        scan_duration = configure_json.get("tmc", {}).get(
            "scan_duration", None
        )

        subarray_node.assert_command_completion(assign_id, event_recorder)
        configure_id = subarray_node.invoke_command(
            "Configure", inputs["CONFIGURE"]
        )
        simulate_configure_on_dish_master()
        subarray_node.wait_for_subarray_obsstate(ObsState.READY)
        subarray_node.assert_command_completion(configure_id, event_recorder)

        command_timeout = subarray_node.subarray_node.read_attribute(
            "CommandTimeout"
        ).value

        scan_json = json.loads(inputs["SCAN"])
        scan_json["scan_ids"] = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        inputs["SCAN"] = json.dumps(scan_json)

        scan_id = subarray_node.invoke_command("Scan", inputs["SCAN"])
        logger.info("scan_id %s", scan_id)
        subarray_node.wait_for_attribute_to_change_to(
            MID_SUBARRAY_NODE_NAME, "scanID", str(2)
        )
        # After second scan induce fault on device
        timeout_defect = json.loads(TIMEOUT_DEFECT)
        timeout_defect["intermediate_state"] = ObsState.SCANNING
        subarray_node.set_devices_defective(
            defective_device_names, json.dumps(timeout_defect)
        )

        # This wait is needed since mapping scan gets timed out after scan duration is over.
        time.sleep(scan_duration + command_timeout)
        subarray_node.assert_command_failure(
            scan_id,
            "Timed out while waiting for obs state to ready for scan id 2",
            event_recorder,
        )

        subarray_node.reset_defects_for_devices(defective_device_names)

    except Exception as exception:
        logger.error("Error occurred - %s", exception)


def mapping_scan_command_after_abort_and_endscan(
    tango_context,
    subarray_node: SubarrayNode,
    inputs: dict,
    command_name,
    event_recorder,
):
    """Run the Scan sequence.
    Invoke Abort command
    Validate that current ongoing scan is aborted
    """
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    assign_id = subarray_node.invoke_command(
        "AssignResources", inputs["ASSIGN"]
    )
    subarray_node.assert_command_completion(assign_id, event_recorder)
    configure_id = subarray_node.invoke_command(
        "Configure", inputs["CONFIGURE"]
    )
    if subarray_node.deployment == "MID":
        simulate_configure_on_dish_master()
        scan_json = json.loads(inputs["SCAN"])
        scan_json["scan_ids"] = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        inputs["SCAN"] = json.dumps(scan_json)
    subarray_node.assert_command_completion(configure_id, event_recorder)

    subarray_node.invoke_command("Scan", inputs["SCAN"])
    subarray_node.wait_for_attribute_to_change_to(
        MID_SUBARRAY_NODE_NAME, "scanID", str(1)
    )
    if command_name == "abort":
        # After first scan id changed to 1 then invoke Abort command
        subarray_node.invoke_abort_command(event_recorder)
        subarray_node.wait_for_subarray_obsstate(ObsState.ABORTED)
        logger.info("Abort command has been sent")
    elif command_name == "endscan":
        scan_end_id = subarray_node.invoke_command("EndScan")
        subarray_node.assert_command_completion(scan_end_id, event_recorder)
        subarray_node.wait_for_subarray_obsstate(ObsState.READY)

        logger.info("endscan command has been sent")
    with pytest.raises(AssertionError):
        # Validate that Scan id not changed to 3
        subarray_node.wait_for_attribute_to_change_to(
            MID_SUBARRAY_NODE_NAME, "scanID", str(9)
        )


def mapping_scan_command(
    tango_context, subarray_node: SubarrayNode, inputs: dict, event_recorder
):
    """Test mapping scan"""
    """Run the Scan sequence."""
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    assign_id = subarray_node.invoke_command(
        "AssignResources", inputs["ASSIGN"]
    )
    subarray_node.assert_command_completion(assign_id, event_recorder)
    configure_id = subarray_node.invoke_command(
        "Configure", inputs["CONFIGURE"]
    )
    if subarray_node.deployment == "MID":
        simulate_configure_on_dish_master()
        scan_json = json.loads(inputs["SCAN"])
        scan_json["scan_ids"] = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        inputs["SCAN"] = json.dumps(scan_json)
    subarray_node.assert_command_completion(configure_id, event_recorder)

    scan_id = subarray_node.invoke_command("Scan", inputs["SCAN"])

    subarray_node.wait_for_attribute_to_change_to(
        MID_SUBARRAY_NODE_NAME, "scanID", str(9)
    )

    subarray_node.assert_command_completion(scan_id, event_recorder)
    subarray_node.wait_for_subarray_obsstate(ObsState.READY)


def scan_command(
    tango_context, subarray_node: SubarrayNode, inputs: dict, event_recorder
):
    """Run the Scan sequence."""
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(2)
    assign_id = subarray_node.invoke_command(
        "AssignResources", inputs["ASSIGN"]
    )
    subarray_node.assert_command_completion(assign_id, event_recorder)

    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(4)
    configure_id = subarray_node.invoke_command(
        "Configure", inputs["CONFIGURE"]
    )
    if subarray_node.deployment == "MID":
        simulate_configure_on_dish_master()
    subarray_node.assert_command_completion(configure_id, event_recorder)

    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(5)
    scan_id = subarray_node.invoke_command("Scan", inputs["SCAN"])
    subarray_node.assert_command_completion(scan_id, event_recorder)
    if subarray_node.deployment == "MID":
        subarray_node.wait_for_attribute_to_change_to(
            MID_SUBARRAY_NODE_NAME, "scanID", str(1)
        )
    subarray_node.wait_for_subarray_obsstate(ObsState.READY)
    logger.info("test case complete.")


@pytest.mark.parametrize(
    "assign_str, configure_str, scan_str",
    [
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_scan_command(
    tango_context,
    subarray_node,
    assign_str,
    configure_str,
    scan_str,
    json_factory,
    event_recorder,
):
    """Test Scan command."""
    inputs = {}
    inputs["ASSIGN"] = json_factory(assign_str)
    inputs["CONFIGURE"] = json_factory(configure_str)
    inputs["SCAN"] = json_factory(scan_str)
    scan_command(
        tango_context,
        subarray_node,
        inputs,
        event_recorder,
    )


@pytest.mark.parametrize(
    "assign_str, configure_str, scan_str",
    [
        pytest.param(
            "AssignResources_mid",
            "Configure_holography",
            "Scan_mid",
            marks=[pytest.mark.SKA_mid],
        ),
    ],
)
@pytest.mark.post_deployment
def test_mapping_scan_command(
    tango_context,
    subarray_node,
    assign_str,
    configure_str,
    scan_str,
    json_factory,
    event_recorder,
):
    """Test Scan command."""
    inputs = {}
    inputs["ASSIGN"] = json_factory(assign_str)
    inputs["CONFIGURE"] = json_factory(configure_str)
    inputs["SCAN"] = json_factory(scan_str)
    mapping_scan_command(
        tango_context,
        subarray_node,
        inputs,
        event_recorder,
    )


@pytest.mark.parametrize(
    "command_name, configure_str, scan_str",
    [
        pytest.param(
            "endscan",
            "Configure_holography",
            "Scan_mid",
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "abort",
            "Configure_holography",
            "Scan_mid",
            marks=[pytest.mark.SKA_mid],
        ),
    ],
)
@pytest.mark.post_deployment
def test_aborting_mapping_scan_command(
    tango_context,
    subarray_node,
    command_name,
    configure_str,
    scan_str,
    json_factory,
    event_recorder,
):
    """Test Scan command."""
    inputs = {}
    inputs["ASSIGN"] = json_factory("AssignResources_mid")
    inputs["CONFIGURE"] = json_factory(configure_str)
    inputs["SCAN"] = json_factory(scan_str)
    mapping_scan_command_after_abort_and_endscan(
        tango_context,
        subarray_node,
        inputs,
        command_name,
        event_recorder,
    )


@pytest.mark.parametrize(
    "defective_device_names, configure_str, scan_str",
    [
        pytest.param(
            [MID_CSPSUBARRAY_LEAF_NODE],
            "Configure_holography",
            "Scan_mid",
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            [MID_SDPSUBARRAY_LEAF_NODE],
            "Configure_holography",
            "Scan_mid",
            marks=[pytest.mark.SKA_mid],
        ),
    ],
)
@pytest.mark.post_deployment
def test_mapping_scan_command_failed(
    tango_context,
    subarray_node,
    defective_device_names,
    configure_str,
    scan_str,
    json_factory,
    event_recorder,
):
    """Test Scan command."""
    inputs = {}
    inputs["ASSIGN"] = json_factory("AssignResources_mid")
    inputs["CONFIGURE"] = json_factory(configure_str)
    inputs["SCAN"] = json_factory(scan_str)
    mapping_scan_command_failed(
        subarray_node, inputs, event_recorder, defective_device_names
    )


def scan_command_failed_on_subsystem_before_queueing(
    tango_context,
    subarray_node: SubarrayNode,
    defective_device_names: list,
    assign_str: str,
    configure_str: str,
    scan_str: str,
    event_recorder,
):
    """Run Configure sequence with defective devices."""
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    assign_id = subarray_node.invoke_command("AssignResources", assign_str)
    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(ObsState.IDLE)
    subarray_node.assert_command_completion(assign_id, event_recorder)
    configure_id = subarray_node.invoke_command("Configure", configure_str)
    if subarray_node.deployment == "MID":
        simulate_configure_on_dish_master()
    subarray_node.wait_for_subarray_obsstate(ObsState.READY)
    subarray_node.assert_command_completion(configure_id, event_recorder)

    subarray_node.set_devices_defective(
        defective_device_names, FAILED_RESULT_DEFECT
    )
    scan_id = subarray_node.invoke_command("Scan", scan_str)
    subarray_node.assert_command_failure(
        scan_id, FAILED_RESULT_EXCEPTION, event_recorder
    )
    subarray_node.reset_defects_for_devices(defective_device_names)


def scan_command_exception_handling(
    tango_context,
    subarray_node: SubarrayNode,
    defective_device_names: list,
    assign_str: str,
    configure_str: str,
    scan_str: str,
    event_recorder,
    defect_param: str,
    exception_message: str,
):
    """Run Scan sequence with defective devices."""
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    assign_id = subarray_node.invoke_command("AssignResources", assign_str)
    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(ObsState.IDLE)
    subarray_node.assert_command_completion(assign_id, event_recorder)
    configure_id = subarray_node.invoke_command("Configure", configure_str)
    if subarray_node.deployment == "MID":
        simulate_configure_on_dish_master()
    subarray_node.wait_for_subarray_obsstate(ObsState.READY)
    subarray_node.assert_command_completion(configure_id, event_recorder)

    subarray_node.set_devices_defective(defective_device_names, defect_param)
    scan_id = subarray_node.invoke_command("Scan", scan_str)
    subarray_node.assert_command_failure(
        scan_id, exception_message, event_recorder
    )
    subarray_node.reset_defects_for_devices(defective_device_names)


@pytest.mark.parametrize(
    "assign_str, configure_str, scan_str, defective_device_names,",
    [
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [MID_CSPSUBARRAY_LEAF_NODE],
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [MID_SDPSUBARRAY_LEAF_NODE],
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [LOW_CSPSUBARRAY_LEAF_NODE],
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [MCCS_SUBARRAY_LEAF_NODE],
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [LOW_SDPSUBARRAY_LEAF_NODE],
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_scan_command_failed_on_subsystem_before_queueing(
    tango_context,
    subarray_node,
    defective_device_names,
    assign_str,
    configure_str,
    scan_str,
    json_factory,
    event_recorder,
):
    """Test Configure command with defective devices
    for Timeout and Error Propagation."""
    scan_command_failed_on_subsystem_before_queueing(
        tango_context,
        subarray_node,
        defective_device_names,
        json_factory(assign_str),
        json_factory(configure_str),
        json_factory(scan_str),
        event_recorder,
    )


@pytest.mark.parametrize(
    "assign_str, configure_str, scan_str,defective_device_names,"
    "exception_message, defective_params",
    [
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [MID_CSPSUBARRAY_LEAF_NODE],
            EXCEPTION_MID_CSP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [DISH_LEAF_NODE, DISH_LEAF_NODE36, DISH_LEAF_NODE100],
            EXCEPTION_ALL_DISH,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [DISH_LEAF_NODE],
            EXCEPTION_DISH,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [MID_CSPSUBARRAY_LEAF_NODE],
            EXCEPTION_MID_CSP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [MID_SDPSUBARRAY_LEAF_NODE],
            EXCEPTION_MID_SDP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [DISH_LEAF_NODE],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT_DISH,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [MID_CSPSUBARRAY_LEAF_NODE],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [MID_SDPSUBARRAY_LEAF_NODE],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [LOW_CSPSUBARRAY_LEAF_NODE],
            EXCEPTION_LOW_CSP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [MCCS_SUBARRAY_LEAF_NODE],
            EXCEPTION_MCCS,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [LOW_SDPSUBARRAY_LEAF_NODE],
            EXCEPTION_LOW_SDP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [LOW_SDPSUBARRAY_LEAF_NODE, LOW_CSPSUBARRAY_LEAF_NODE],
            EXCEPTION_LOW_CSP_SDP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [LOW_CSPSUBARRAY_LEAF_NODE],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [MCCS_SUBARRAY_LEAF_NODE],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [LOW_SDPSUBARRAY_LEAF_NODE],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_scan_command_exception_handling(
    tango_context,
    subarray_node,
    defective_device_names,
    assign_str,
    configure_str,
    scan_str,
    json_factory,
    event_recorder,
    defective_params,
    exception_message,
):
    """Test Configure command with defective devices
    for Timeout and Error Propagation."""
    scan_command_exception_handling(
        tango_context,
        subarray_node,
        defective_device_names,
        json_factory(assign_str),
        json_factory(configure_str),
        json_factory(scan_str),
        event_recorder,
        defective_params,
        exception_message,
    )
