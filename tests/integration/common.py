import json
import time

import pytest
from ska_tango_base.control_model import ObsState
from ska_tmc_common import DishMode, PointingState
from ska_tmc_common.test_helpers.helper_csp_subarray_leaf_device import (
    HelperCspSubarrayLeafDevice,
)
from ska_tmc_common.test_helpers.helper_dish_device import HelperDishDevice
from ska_tmc_common.test_helpers.helper_dish_ln_device import (
    HelperDishLNDevice,
)
from ska_tmc_common.test_helpers.helper_sdp_subarray import HelperSdpSubarray
from ska_tmc_common.test_helpers.helper_sdp_subarray_leaf_device import (
    HelperSdpSubarrayLeafDevice,
)
from ska_tmc_common.test_helpers.helper_subarray_device import (
    HelperSubArrayDevice,
)
from tango import DeviceProxy

from tests.settings import SLEEP_TIME, TIMEOUT, logger
from tests.test_helpers.constants import (
    DISH_LEAF_NODE,
    DISH_MASTER_DEVICE,
    DISH_MASTER_DEVICE1,
    DISH_MASTER_DEVICE2,
    DISH_MASTER_DEVICE63,
    LOW_CSPSUBARRAY_LEAF_NODE,
    LOW_CSPSUBARRAY_NODE_NAME,
    LOW_SDPSUBARRAY_LEAF_NODE,
    LOW_SDPSUBARRAY_NODE_NAME,
    MCCS_SUBARRAY_LEAF_NODE,
    MCCSSUBARRAY_NODE_NAME,
    MID_CSPSUBARRAY_LEAF_NODE,
    MID_CSPSUBARRAY_NODE_NAME,
    MID_SDPSUBARRAY_LEAF_NODE,
    MID_SDPSUBARRAY_NODE_NAME,
    TMC_LOW_DOMAIN,
)

pytest.event_arrived = False


@pytest.fixture()
def devices_to_load():
    return (
        {
            "class": HelperSubArrayDevice,
            "devices": [
                {"name": MID_CSPSUBARRAY_NODE_NAME},
                {"name": LOW_CSPSUBARRAY_NODE_NAME},
                {"name": MCCSSUBARRAY_NODE_NAME},
            ],
        },
        {
            "class": HelperSdpSubarrayLeafDevice,
            "devices": [
                {"name": MID_SDPSUBARRAY_LEAF_NODE},
                {"name": LOW_SDPSUBARRAY_LEAF_NODE},
            ],
        },
        {
            "class": HelperCspSubarrayLeafDevice,
            "devices": [
                {"name": MID_CSPSUBARRAY_LEAF_NODE},
                {"name": LOW_CSPSUBARRAY_LEAF_NODE},
            ],
        },
        {
            "class": HelperDishDevice,
            "devices": [
                {"name": DISH_MASTER_DEVICE},
            ],
        },
        {
            "class": HelperDishLNDevice,
            "devices": [
                {"name": DISH_LEAF_NODE},
            ],
        },
        {
            "class": HelperSdpSubarray,
            "devices": [
                {"name": LOW_SDPSUBARRAY_NODE_NAME},
                {"name": MID_SDPSUBARRAY_NODE_NAME},
            ],
        },
    )


def checked_devices(json_model):
    result = 0
    for dev in json_model["devices"]:
        if int(dev["ping"]) > 0 and dev["unresponsive"] == "False":
            result += 1
    return result


def ensure_checked_devices(subarray_node):
    json_model = json.loads(subarray_node.internalModel)
    logger.debug("json_model devices: %s", json_model["devices"])
    start_time = time.time()
    # TODO: Remove below commented code when the monitoring loop is removed
    checked_devs = checked_devices(json_model)
    logger.debug("checked_devs: %s", checked_devs)
    logger.debug(
        "Initial len(json_model[devices']): %s", len(json_model["devices"])
    )
    # 4 devices is a minimum number available devices. It includes CSP and SDP
    # Subarrays and Subarray Leaf Nodes.
    while len(json_model["devices"]) < 4:
        # TODO: Remove below commented code when the monitoring loop is removed
        # new_checked_devs = checked_devices(json_model)
        # if checked_devs != new_checked_devs:
        #     checked_devs = new_checked_devs
        #     logger.debug("checked devices: %s", checked_devs)
        logger.debug(
            "len(json_model[devices']): %s", len(json_model["devices"])
        )
        time.sleep(SLEEP_TIME)
        elapsed_time = time.time() - start_time
        if elapsed_time > TIMEOUT:
            logger.debug("Internal Model:", subarray_node.internalModel)
            pytest.fail("Timeout occurred while executing the test")
        json_model = json.loads(subarray_node.internalModel)


def assert_event_arrived():
    start_time = time.time()
    while not pytest.event_arrived:
        time.sleep(SLEEP_TIME)
        elapsed_time = time.time() - start_time
        if elapsed_time > TIMEOUT:
            pytest.fail("Timeout occurred while executing the test")

    assert pytest.event_arrived


def set_mid_subsystem_devices_for_aggregation(
    dev_factory, obs_state, dish_mode, pointing_state
):
    logger.debug("Setting ObsState to : %s", obs_state)
    logger.debug("Setting DishMode to : %s", dish_mode)

    proxy_dish = dev_factory.get_device("ska001/elt/master")
    proxy_dish.SetDirectDishMode(dish_mode)
    proxy_dish.SetDirectPointingState(pointing_state)

    proxy_sdp_saln = dev_factory.get_device(MID_SDPSUBARRAY_LEAF_NODE)
    proxy_sdp_saln.SetSdpSubarrayLeafNodeObsState(obs_state)

    proxy_csp_saln = dev_factory.get_device(MID_CSPSUBARRAY_LEAF_NODE)
    proxy_csp_saln.SetCspSubarrayLeafNodeObsState(obs_state)

    time.sleep(0.1)

    logger.debug(
        "ObsState of csp subarray leaf node: %s",
        proxy_csp_saln.cspSubarrayObsState,
    )
    logger.debug(
        "ObsState of sdp subarray leaf node: %s",
        proxy_sdp_saln.sdpSubarrayObsState,
    )
    logger.debug("DishMode of DishMaster: %s", proxy_dish.DishMode)
    logger.debug("PointingState of DishMaster: %s", proxy_dish.PointingState)


def set_low_subsystem_devices_for_aggregation(dev_factory, obs_state):
    logger.debug("Setting ObsState to : %s", obs_state)

    proxy_sdp_saln = dev_factory.get_device(LOW_SDPSUBARRAY_LEAF_NODE)
    proxy_sdp_saln.SetSdpSubarrayLeafNodeObsState(obs_state)

    proxy_csp_saln = dev_factory.get_device(LOW_CSPSUBARRAY_LEAF_NODE)
    proxy_csp_saln.SetCspSubarrayLeafNodeObsState(obs_state)

    time.sleep(0.1)

    logger.debug(
        "ObsState of csp subarray leaf node: %s",
        proxy_csp_saln.cspSubarrayObsState,
    )
    logger.debug(
        "ObsState of sdp subarray leaf node: %s",
        proxy_sdp_saln.sdpSubarrayObsState,
    )


def set_mccs_sa_obsstate(dev_factory, obs_state):
    proxy_sdp_sa = dev_factory.get_device(MCCSSUBARRAY_NODE_NAME)
    proxy_sdp_sa.SetDirectObsState(obs_state)


def set_mccs_subarray_ln_obsstate(obs_state):
    mccs_subarray_ln_proxy = DeviceProxy(MCCS_SUBARRAY_LEAF_NODE)
    mccs_subarray_ln_proxy.SetDirectObsState(obs_state)
    mccs_subarray_ln_obsstate = mccs_subarray_ln_proxy.read_attribute(
        "obsState"
    ).value
    logger.debug(
        f"MccsSubarrayLeafNode's obsState is {mccs_subarray_ln_obsstate}"
    )
    wait_time = 0
    while mccs_subarray_ln_obsstate != obs_state:
        time.sleep(0.5)
        mccs_subarray_ln_obsstate = mccs_subarray_ln_proxy.read_attribute(
            "obsState"
        ).value
        wait_time = wait_time + 1
        logger.debug(f"wait_time in teardown  {wait_time}")
        if wait_time > TIMEOUT:
            pytest.fail(
                "Timeout occurred in transitioning MccsSubarrayLeafNode "
                + f"obsState to {obs_state}"
            )


def tear_down(dev_factory, subarray_node_name):
    subarray_node = dev_factory.get_device(subarray_node_name)
    subarray_obsstate = subarray_node.read_attribute("obsState")
    logger.debug(f"TMC Subarray ObsState: {subarray_obsstate.value}")

    if subarray_obsstate.value == ObsState.EMPTY:
        logger.debug("When SubarrayNode's obsState is EMPTY")
        subarray_node.Off()

    if subarray_obsstate.value == ObsState.IDLE:
        logger.debug("When SubarrayNode's obsState is IDLE")
        logger.debug("Invoking ReleaseAllResources command on subarray node")
        if TMC_LOW_DOMAIN in subarray_node_name:
            set_mccs_subarray_ln_obsstate(ObsState.EMPTY)
        subarray_node.ReleaseAllResources()
        wait_for_final_subarray_obsstate(subarray_node, ObsState.EMPTY)
        subarray_node.Off()
        subarray_obsstate = subarray_node.read_attribute("obsState")
        logger.debug(
            f"After tear downTMC Subarray ObsState: {subarray_obsstate.value}"
        )

    if subarray_obsstate.value == ObsState.READY:
        logger.debug("When SubarrayNode's obsState is READY")
        logger.debug("Invoking End command on subarray node")
        subarray_node.End()
        wait_for_final_subarray_obsstate(subarray_node, ObsState.IDLE)

        logger.debug("Invoking ReleaseResources command on subarray node")
        if TMC_LOW_DOMAIN in subarray_node_name:
            set_mccs_subarray_ln_obsstate(ObsState.EMPTY)
        subarray_node.ReleaseAllResources()
        wait_for_final_subarray_obsstate(subarray_node, ObsState.EMPTY)
        subarray_node.Off()
        subarray_obsstate = subarray_node.read_attribute("obsState")
        logger.debug(
            f"After tear down TMC Subarray ObsState: {subarray_obsstate.value}"
        )

    if subarray_obsstate.value == ObsState.ABORTED:
        logger.debug("When SubarrayNode's obsState is ABORTED")
        logger.debug("Invoking Restart command on subarray node")
        if TMC_LOW_DOMAIN in subarray_node_name:
            set_mccs_subarray_ln_obsstate(ObsState.EMPTY)
        subarray_node.Restart()
        wait_for_final_subarray_obsstate(subarray_node, ObsState.EMPTY)

        subarray_node.Off()
        subarray_obsstate = subarray_node.read_attribute("obsState")
        logger.debug(
            f"After tear down TMC Subarray ObsState: {subarray_obsstate.value}"
        )

    if subarray_obsstate.value in [
        ObsState.RESOURCING,
        ObsState.CONFIGURING,
        ObsState.SCANNING,
    ]:
        logger.debug(
            "When SubarrayNode's obsState is %s", subarray_obsstate.value
        )
        logger.debug("Invoking Abort Restart command on subarray node")
        subarray_node.Abort()
        wait_for_final_subarray_obsstate(subarray_node, ObsState.ABORTED)

        if TMC_LOW_DOMAIN in subarray_node_name:
            set_mccs_subarray_ln_obsstate(ObsState.EMPTY)
        subarray_node.Restart()
        wait_for_final_subarray_obsstate(subarray_node, ObsState.EMPTY)

        subarray_node.Off()
        subarray_obsstate = subarray_node.read_attribute("obsState")
        logger.debug(
            f"After tear down TMC Subarray ObsState: {subarray_obsstate.value}"
        )


def wait_for_final_subarray_obsstate(subarray_node, obs_state):
    logger.debug(f"Waiting for SubarrayNode obsState to be {obs_state}")
    subarray_obsstate = subarray_node.read_attribute("obsState").value
    logger.debug(f"SubarrayNode obsState is {subarray_obsstate}")
    wait_time = 0
    while subarray_obsstate != obs_state:
        time.sleep(0.5)
        subarray_obsstate = subarray_node.read_attribute("obsState").value
        logger.debug(f"SubarrayNode obsState in loop: {subarray_obsstate}")
        logger.debug(f"Expected SubarrayNode obsState: {obs_state}")
        wait_time = wait_time + 1
        logger.debug(f"wait_time in teardown  {wait_time}")
        if wait_time > TIMEOUT:
            pytest.fail(
                "Timeout occurred in transitioning SubarrayNode "
                + f"obsState to {obs_state}"
            )


def simulate_configure_on_dish_master():
    """Simulate the configure command flow on Dish Master device."""
    dish_master = DeviceProxy(DISH_MASTER_DEVICE1)
    current_dish_mode = dish_master.dishMode

    # Setting the DishMode to CONFIG
    dish_master.SetDirectDishMode(DishMode.CONFIG)
    # Setting the DishMode back to its original value.
    dish_master.SetDirectDishMode(current_dish_mode)
    # If the DishMode is not OPERATE, setting it to OPERATE
    if current_dish_mode != DishMode.OPERATE:
        dish_master.SetDirectDishMode(DishMode.OPERATE)

    # Setting the Pointing State
    dish_master.SetDirectPointingState(PointingState.TRACK)


def simulate_configure_on_multiple_dish_master():
    """Simulate the configure command flow on Dish Master devices."""
    dish_master_list = [
        DISH_MASTER_DEVICE1,
        DISH_MASTER_DEVICE2,
        DISH_MASTER_DEVICE63,
    ]

    for dish_device in dish_master_list:
        dish_master = DeviceProxy(dish_device)
        current_dish_mode = dish_master.dishMode

        # Setting the DishMode to CONFIG
        dish_master.SetDirectDishMode(DishMode.CONFIG)
        # Setting the DishMode back to its original value.
        dish_master.SetDirectDishMode(current_dish_mode)
        # If the DishMode is not OPERATE, setting it to OPERATE
        if current_dish_mode != DishMode.OPERATE:
            dish_master.SetDirectDishMode(DishMode.OPERATE)

        # Setting the Pointing State
        dish_master.SetDirectPointingState(PointingState.TRACK)


def set_and_wait_for_delay(device_fqdn: str, value: int):
    """A method to wait till delay attribute value is updated"""
    logger.debug(f"Waiting for device delay to be {value}")
    device_proxy = DeviceProxy(device_fqdn)
    device_proxy.SetDelay(value)
    leaf_node_delay = device_proxy.read_attribute("delay").value
    logger.debug(f"Leaf node delay is {leaf_node_delay}")
    wait_time = 0
    while leaf_node_delay != value:
        time.sleep(0.5)
        leaf_node_delay = device_proxy.read_attribute("delay").value
        logger.debug(f"leaf node delay in loop: {leaf_node_delay}")
        logger.debug(f"Expected leaf node delay: {value}")
        wait_time = wait_time + 1
        logger.debug(f"wait_time in teardown  {wait_time}")
        if wait_time > TIMEOUT:
            pytest.fail(
                "Timeout occurred in setting leaf node " + f"delay to {50}"
            )
