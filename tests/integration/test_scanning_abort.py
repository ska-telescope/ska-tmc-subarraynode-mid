import pytest
from ska_control_model import ObsState

from tests.integration.common import (
    ensure_checked_devices,
    set_mccs_subarray_ln_obsstate,
    simulate_configure_on_dish_master,
)
from tests.test_helpers.common_utils import SubarrayNode


def abort_command(
    tango_context, subarray_node: SubarrayNode, inputs: dict, event_recorder
):
    """Run the Abort sequence in SCANNING obsState."""
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(2)
    assign_id = subarray_node.invoke_command(
        "AssignResources", inputs["ASSIGN"]
    )
    subarray_node.assert_command_completion(assign_id, event_recorder)

    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(4)
    configure_id = subarray_node.invoke_command(
        "Configure", inputs["CONFIGURE"]
    )
    if subarray_node.deployment == "MID":
        simulate_configure_on_dish_master()
    subarray_node.assert_command_completion(configure_id, event_recorder)

    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(5)
    scan_id = subarray_node.invoke_command("Scan", inputs["SCAN"])
    subarray_node.assert_command_completion(scan_id, event_recorder)

    subarray_node.wait_for_subarray_obsstate(ObsState.SCANNING)

    subarray_node.invoke_abort_command(event_recorder)
    subarray_node.wait_for_subarray_obsstate(ObsState.ABORTED)


@pytest.mark.parametrize(
    "assign_str, configure_str, scan_str",
    [
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_abort_command(
    tango_context,
    subarray_node,
    assign_str,
    configure_str,
    scan_str,
    json_factory,
    event_recorder,
):
    """Test Abort command."""
    inputs = {}
    inputs["ASSIGN"] = json_factory(assign_str)
    inputs["CONFIGURE"] = json_factory(configure_str)
    inputs["SCAN"] = json_factory(scan_str)
    abort_command(
        tango_context,
        subarray_node,
        inputs,
        event_recorder,
    )
