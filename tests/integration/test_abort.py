import pytest
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus

from tests.conftest import SubarrayNode
from tests.integration.common import (
    ensure_checked_devices,
    set_mccs_subarray_ln_obsstate,
)
from tests.settings import (
    ERROR_PROPAGATION_DEFECT,
    EXCEPTION_LOW_CSP,
    EXCEPTION_LOW_SDP,
    EXCEPTION_MCCS,
    EXCEPTION_MID_CSP,
    EXCEPTION_MID_SDP,
    LOW_CSPSUBARRAY_LEAF_NODE,
    LOW_SDPSUBARRAY_LEAF_NODE,
    MCCS_SUBARRAY_LEAF_NODE,
    MID_CSPSUBARRAY_LEAF_NODE,
    MID_SDPSUBARRAY_LEAF_NODE,
    TIMEOUT_DEFECT,
    TIMEOUT_EXCEPTION,
)
from tests.test_helpers.event_recorder import EventRecorder


def abort_in_resourcing_command(
    tango_context,
    subarray_node: SubarrayNode,
    defective_device_names: list,
    input_str: str,
    event_recorder,
):
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    subarray_node.set_devices_defective(defective_device_names, TIMEOUT_DEFECT)

    assign_id = subarray_node.invoke_command("AssignResources", input_str)
    subarray_node.wait_for_subarray_obsstate(ObsState.RESOURCING)
    for device_name in defective_device_names:
        if "csp" in device_name:
            subarray_node.wait_for_leaf_node_obsstate(
                "CSPSUBARRAYLEAFNODE", ObsState.RESOURCING
            )
            subarray_node.wait_for_leaf_node_obsstate(
                "SDPSUBARRAYLEAFNODE", ObsState.IDLE
            )

        else:
            subarray_node.wait_for_leaf_node_obsstate(
                "CSPSUBARRAYLEAFNODE", ObsState.IDLE
            )
            subarray_node.wait_for_leaf_node_obsstate(
                "SDPSUBARRAYLEAFNODE", ObsState.RESOURCING
            )

    # Set MCCS SA LN obsState to IDLE as AssignResources command flows via
    # MCCS Controller Leaf Node to MCCS Controller
    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(ObsState.IDLE)

    subarray_node.assert_command_failure(
        assign_id, TIMEOUT_EXCEPTION, event_recorder
    )
    subarray_node.reset_defects_for_devices(defective_device_names)

    subarray_node.invoke_abort_command(event_recorder)
    subarray_node.wait_for_subarray_obsstate(ObsState.ABORTED)
    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(ObsState.EMPTY)

    restart_id = subarray_node.invoke_command("Restart")
    subarray_node.assert_command_completion(restart_id, event_recorder)
    subarray_node.wait_for_subarray_obsstate(ObsState.EMPTY)


@pytest.mark.parametrize(
    "json_used, defective_device_names",
    [
        pytest.param(
            "AssignResources_mid",
            [MID_CSPSUBARRAY_LEAF_NODE],
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_low",
            [LOW_CSPSUBARRAY_LEAF_NODE],
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_abort_in_resourcing(
    tango_context,
    subarray_node,
    defective_device_names,
    json_used,
    json_factory,
    event_recorder,
):
    abort_in_resourcing_command(
        tango_context,
        subarray_node,
        defective_device_names,
        json_factory(json_used),
        event_recorder,
    )


def abort_in_resourcing_empty_command(
    tango_context,
    subarray_node: SubarrayNode,
    defective_device_names: list,
    input_str: str,
    exception_message: str,
    event_recorder,
):
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    subarray_node.set_devices_defective(
        defective_device_names, ERROR_PROPAGATION_DEFECT
    )

    assign_id = subarray_node.invoke_command("AssignResources", input_str)

    for device_name in defective_device_names:
        if "csp" in device_name:
            subarray_node.wait_for_leaf_node_obsstate(
                "CSPSUBARRAYLEAFNODE", ObsState.EMPTY
            )
            subarray_node.wait_for_leaf_node_obsstate(
                "SDPSUBARRAYLEAFNODE", ObsState.IDLE
            )

        elif "sdp" in device_name:
            subarray_node.wait_for_leaf_node_obsstate(
                "CSPSUBARRAYLEAFNODE", ObsState.IDLE
            )
            subarray_node.wait_for_leaf_node_obsstate(
                "SDPSUBARRAYLEAFNODE", ObsState.EMPTY
            )
        else:
            # In case of MCCS Subarray Leaf Node Failure
            subarray_node.wait_for_leaf_node_obsstate(
                "CSPSUBARRAYLEAFNODE", ObsState.IDLE
            )
            subarray_node.wait_for_leaf_node_obsstate(
                "SDPSUBARRAYLEAFNODE", ObsState.IDLE
            )

    # Set MCCS SA LN obsState to IDLE as AssignResources command flows via
    # MCCS Controller Leaf Node to MCCS Controller
    if subarray_node.deployment == "LOW":
        if MCCS_SUBARRAY_LEAF_NODE not in defective_device_names:
            set_mccs_subarray_ln_obsstate(ObsState.IDLE)

    subarray_node.wait_for_subarray_obsstate(ObsState.RESOURCING)
    subarray_node.assert_command_failure(
        assign_id, exception_message, event_recorder
    )
    subarray_node.reset_defects_for_devices(defective_device_names)

    subarray_node.invoke_abort_command(event_recorder)
    subarray_node.wait_for_subarray_obsstate(ObsState.ABORTED)

    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(ObsState.EMPTY)
    restart_id = subarray_node.invoke_command("Restart")
    subarray_node.assert_command_completion(restart_id, event_recorder)
    subarray_node.wait_for_subarray_obsstate(ObsState.EMPTY)


@pytest.mark.parametrize(
    "json_used, defective_device_names, exception_message",
    [
        pytest.param(
            "AssignResources_mid",
            [MID_CSPSUBARRAY_LEAF_NODE],
            EXCEPTION_MID_CSP,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_low",
            [LOW_CSPSUBARRAY_LEAF_NODE],
            EXCEPTION_LOW_CSP,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_mid",
            [MID_SDPSUBARRAY_LEAF_NODE],
            EXCEPTION_MID_SDP,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_low",
            [LOW_SDPSUBARRAY_LEAF_NODE],
            EXCEPTION_LOW_SDP,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            [MCCS_SUBARRAY_LEAF_NODE],
            TIMEOUT_EXCEPTION,
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_abort_in_resourcing_empty(
    tango_context,
    subarray_node,
    defective_device_names,
    json_used,
    exception_message,
    json_factory,
    event_recorder,
):
    abort_in_resourcing_empty_command(
        tango_context,
        subarray_node,
        defective_device_names,
        json_factory(json_used),
        exception_message,
        event_recorder,
    )


def assign_after_abort(
    tango_context,
    subarray_node: SubarrayNode,
    defective_device_names: list,
    input_str: str,
    event_recorder,
):
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    subarray_node.set_devices_defective(defective_device_names, TIMEOUT_DEFECT)
    assign_id = subarray_node.invoke_command("AssignResources", input_str)

    # Set MCCS SA LN obsState to IDLE as AssignResources command flows via
    # MCCS Controller Leaf Node to MCCS Controller
    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(ObsState.IDLE)

    subarray_node.wait_for_subarray_obsstate(ObsState.RESOURCING)

    subarray_node.wait_for_leaf_node_obsstate(
        "CSPSUBARRAYLEAFNODE", ObsState.RESOURCING
    )
    subarray_node.assert_command_failure(
        assign_id, TIMEOUT_EXCEPTION, event_recorder
    )
    subarray_node.reset_defects_for_devices(defective_device_names)

    subarray_node.invoke_abort_command(event_recorder)
    subarray_node.wait_for_subarray_obsstate(ObsState.ABORTED)
    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(ObsState.EMPTY)
    restart_id = subarray_node.invoke_command("Restart")
    subarray_node.assert_command_completion(restart_id, event_recorder)
    subarray_node.wait_for_subarray_obsstate(ObsState.EMPTY)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)
    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(ObsState.IDLE)
    assign_id = subarray_node.invoke_command("AssignResources", input_str)
    subarray_node.assert_command_completion(assign_id, event_recorder)


@pytest.mark.parametrize(
    "json_used, defective_device_names",
    [
        pytest.param(
            "AssignResources_mid",
            [MID_CSPSUBARRAY_LEAF_NODE],
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_low",
            [LOW_CSPSUBARRAY_LEAF_NODE],
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_assign_after_abort(
    tango_context,
    subarray_node,
    defective_device_names,
    json_used,
    json_factory,
    event_recorder,
):
    assign_after_abort(
        tango_context,
        subarray_node,
        defective_device_names,
        json_factory(json_used),
        event_recorder,
    )


def abort_after_assign_resources_command(
    tango_context,
    subarray_node: SubarrayNode,
    defective_device: str,
    input_str: str,
    event_recorder: EventRecorder,
):
    event_recorder.subscribe_event(
        subarray_node.subarray_node, "longRunningCommandStatus"
    )
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    subarray_node.set_devices_defective([defective_device], TIMEOUT_DEFECT)

    assign_id = subarray_node.invoke_command("AssignResources", input_str)
    subarray_node.wait_for_subarray_obsstate(ObsState.RESOURCING)

    subarray_node.reset_defects_for_devices([defective_device])
    subarray_node.invoke_abort_command(event_recorder)
    subarray_node.assert_command_failure(
        assign_id,
        "Command has been aborted",
        event_recorder,
        ResultCode.ABORTED,
    )
    subarray_node.wait_for_subarray_obsstate(ObsState.ABORTED)
    restart_id = subarray_node.invoke_command("Restart")
    subarray_node.assert_command_completion(restart_id, event_recorder)
    subarray_node.wait_for_subarray_obsstate(ObsState.EMPTY)


@pytest.mark.parametrize(
    "json_used, defective_device",
    [
        pytest.param(
            "AssignResources_mid",
            MID_CSPSUBARRAY_LEAF_NODE,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_low",
            LOW_CSPSUBARRAY_LEAF_NODE,
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_abort_after_assign_resources_command(
    tango_context,
    subarray_node,
    defective_device,
    json_used,
    json_factory,
    event_recorder,
):
    abort_after_assign_resources_command(
        tango_context,
        subarray_node,
        defective_device,
        json_factory(json_used),
        event_recorder,
    )
