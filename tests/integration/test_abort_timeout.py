import pytest
from ska_tango_base.control_model import ObsState

from tests.conftest import SubarrayNode
from tests.integration.common import (
    ensure_checked_devices,
    set_and_wait_for_delay,
)
from tests.settings import MID_CSPSUBARRAY_LEAF_NODE


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_abort_timeout(
    tango_context, subarray_node: SubarrayNode, event_recorder, json_factory
):
    """Run Abort timeout sequence, with cspsubarrayleafnode
    delayed reponse for obsState ABORTED."""

    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    assign_json = json_factory("AssignResources_mid")
    assign_id = subarray_node.invoke_command("AssignResources", assign_json)

    subarray_node.assert_command_completion(assign_id, event_recorder)

    set_and_wait_for_delay(MID_CSPSUBARRAY_LEAF_NODE, 50)
    subarray_node.invoke_abort_command(event_recorder)

    subarray_node.wait_for_subarray_obsstate(ObsState.FAULT)
    subarray_node.wait_for_leaf_node_obsstate(
        "CSPSUBARRAYLEAFNODE", ObsState.ABORTED
    )
    subarray_node.wait_for_leaf_node_obsstate(
        "SDPSUBARRAYLEAFNODE", ObsState.ABORTED
    )

    set_and_wait_for_delay(MID_CSPSUBARRAY_LEAF_NODE, 2)
