import logging

import pytest
from ska_tango_base.control_model import ObsState

from tests.conftest import (
    LOW_CSPSUBARRAY_LEAF_NODE,
    LOW_SDPSUBARRAY_LEAF_NODE,
    MID_CSPSUBARRAY_LEAF_NODE,
    MID_SDPSUBARRAY_LEAF_NODE,
)
from tests.integration.common import (
    ensure_checked_devices,
    set_mccs_subarray_ln_obsstate,
)
from tests.settings import (
    ERROR_PROPAGATION_DEFECT,
    EXCEPTION_LOW_CSP,
    EXCEPTION_LOW_SDP,
    EXCEPTION_MID_CSP,
    EXCEPTION_MID_CSP_SDP,
    EXCEPTION_MID_SDP,
    FAILED_RESULT_DEFECT,
    FAILED_RESULT_EXCEPTION,
    TIMEOUT_DEFECT,
    TIMEOUT_EXCEPTION,
)
from tests.test_helpers.common_utils import SubarrayNode


def assign_resources_with_exception(
    tango_context,
    subarray_node: SubarrayNode,
    defective_device_names: list,
    defect_param: str,
    input_str: str,
    exception_message: str,
    event_recorder,
):
    """Run AssignResources sequence with defective devices."""
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    subarray_node.set_devices_defective(defective_device_names, defect_param)
    assign_id = subarray_node.invoke_command("AssignResources", input_str)

    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(ObsState.IDLE)

    subarray_node.assert_command_failure(
        assign_id, exception_message, event_recorder
    )
    subarray_node.assert_obs_state(ObsState.RESOURCING)

    subarray_node.reset_defects_for_devices(defective_device_names)


@pytest.mark.parametrize(
    "json_used, defective_device_names, exception_message, defective_params",
    [
        pytest.param(
            "AssignResources_mid",
            [MID_CSPSUBARRAY_LEAF_NODE],
            EXCEPTION_MID_CSP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            [MID_SDPSUBARRAY_LEAF_NODE],
            EXCEPTION_MID_SDP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            [MID_CSPSUBARRAY_LEAF_NODE],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            [MID_SDPSUBARRAY_LEAF_NODE],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_low",
            [LOW_CSPSUBARRAY_LEAF_NODE],
            EXCEPTION_LOW_CSP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            [LOW_SDPSUBARRAY_LEAF_NODE],
            EXCEPTION_LOW_SDP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            [LOW_CSPSUBARRAY_LEAF_NODE],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            [LOW_SDPSUBARRAY_LEAF_NODE],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_assign_resources_exception(
    tango_context,
    subarray_node,
    defective_device_names,
    defective_params,
    json_used,
    json_factory,
    exception_message,
    event_recorder,
):
    """Test AssignResources command with defective devices
    for Timeout and Error Propagation."""
    assign_resources_with_exception(
        tango_context,
        subarray_node,
        defective_device_names,
        defective_params,
        json_factory(json_used),
        exception_message,
        event_recorder,
    )


def assign_resources_with_exception_handling(
    tango_context,
    subarray_node: SubarrayNode,
    defective_device_names: list,
    defect_param: str,
    input_str: str,
    exception_message: str,
    event_recorder,
):
    """Run AssignResources sequence with defective devices."""
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    subarray_node.set_devices_defective(defective_device_names, defect_param)
    assign_id = subarray_node.invoke_command("AssignResources", input_str)

    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(ObsState.IDLE)

    subarray_node.assert_command_failure(
        assign_id, exception_message, event_recorder
    )
    subarray_node.wait_for_subarray_obsstate(ObsState.EMPTY)

    subarray_node.reset_defects_for_devices(defective_device_names)


@pytest.mark.parametrize(
    "json_used, defective_device_names, exception_message, defective_params",
    [
        pytest.param(
            "AssignResources_mid",
            [MID_SDPSUBARRAY_LEAF_NODE, MID_CSPSUBARRAY_LEAF_NODE],
            EXCEPTION_MID_CSP_SDP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        # TODO: Enable when tear down is updated to include MCCS and SP-3714
        # updates
        # pytest.param(
        #     "AssignResources_low",
        #     [LOW_SDPSUBARRAY_LEAF_NODE, LOW_CSPSUBARRAY_LEAF_NODE],
        #     EXCEPTION_LOW_CSP_SDP,
        #     ERROR_PROPAGATION_DEFECT,
        #     marks=[pytest.mark.SKA_low],
        # ),
    ],
)
@pytest.mark.post_deployment
def test_assign_resources_exception_handling(
    tango_context,
    subarray_node,
    defective_device_names,
    defective_params,
    json_used,
    json_factory,
    exception_message,
    event_recorder,
):
    """Test AssignResources command with defective devices
    for Timeout and Error Propagation."""
    assign_resources_with_exception_handling(
        tango_context,
        subarray_node,
        defective_device_names,
        defective_params,
        json_factory(json_used),
        exception_message,
        event_recorder,
    )


def assign_resources_command(
    tango_context, subarray_node: SubarrayNode, input_str: str, event_recorder
):
    """Run the AssignResources sequence."""
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(ObsState.IDLE)

    assign_id = subarray_node.invoke_command("AssignResources", input_str)
    subarray_node.assert_command_completion(assign_id, event_recorder)

    if subarray_node.deployment == "MID":
        assert subarray_node.subarray_node.assignedResources == (
            "SKA001",
            "SKA036",
            "SKA100",
        )


@pytest.mark.parametrize(
    "json_used",
    [
        pytest.param("AssignResources_mid", marks=[pytest.mark.SKA_mid]),
        pytest.param(
            "AssignResources_low",
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_assign_resources(
    tango_context, subarray_node, json_used, json_factory, event_recorder
):
    """Test AssignResources command."""
    assign_resources_command(
        tango_context,
        subarray_node,
        json_factory(json_used),
        event_recorder,
    )


def assignresources_command_failed_on_sdp_subsystem(
    tango_context,
    subarray_node: SubarrayNode,
    defective_device_names: list,
    input_str: str,
    event_recorder,
):
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    subarray_node.set_devices_defective(
        defective_device_names, FAILED_RESULT_DEFECT
    )

    assign_id = subarray_node.invoke_command("AssignResources", input_str)

    subarray_node.assert_command_failure(
        assign_id, FAILED_RESULT_EXCEPTION, event_recorder
    )
    subarray_node.wait_for_subarray_obsstate(ObsState.RESOURCING)
    # Check that the assignedResources list is empty
    assert (
        not subarray_node.subarray_node.assignedResources
    ), "The assignedResources list is not empty"
    subarray_node.reset_defects_for_devices(defective_device_names)


@pytest.mark.parametrize(
    "json_used, defective_device_names",
    [
        pytest.param(
            "AssignResources_mid",
            [MID_SDPSUBARRAY_LEAF_NODE],
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_low",
            [LOW_SDPSUBARRAY_LEAF_NODE],
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_assignresources_command_failed_on_sdp_subsystem(
    tango_context,
    subarray_node,
    defective_device_names,
    json_used,
    json_factory,
    event_recorder,
):
    assignresources_command_failed_on_sdp_subsystem(
        tango_context,
        subarray_node,
        defective_device_names,
        json_factory(json_used),
        event_recorder,
    )


def assignresources_command_failed_on_csp_subsystem(
    tango_context,
    subarray_node: SubarrayNode,
    defective_device_names: list,
    input_str: str,
    event_recorder,
):
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    subarray_node.set_devices_defective(
        defective_device_names, FAILED_RESULT_DEFECT
    )

    assign_id = subarray_node.invoke_command("AssignResources", input_str)

    subarray_node.assert_command_failure(
        assign_id, FAILED_RESULT_EXCEPTION, event_recorder
    )
    # Check that the assignedResources list is empty
    assert (
        not subarray_node.subarray_node.assignedResources
    ), "The assignedResources list is not empty"
    subarray_node.wait_for_subarray_obsstate(ObsState.EMPTY)
    subarray_node.reset_defects_for_devices(defective_device_names)


@pytest.mark.parametrize(
    "json_used, defective_device_names",
    [
        pytest.param(
            "AssignResources_mid",
            [MID_CSPSUBARRAY_LEAF_NODE],
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_low",
            [LOW_CSPSUBARRAY_LEAF_NODE],
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_assignresources_command_failed_on_csp_subsystem(
    tango_context,
    subarray_node,
    defective_device_names,
    json_used,
    json_factory,
    event_recorder,
):
    assignresources_command_failed_on_csp_subsystem(
        tango_context,
        subarray_node,
        defective_device_names,
        json_factory(json_used),
        event_recorder,
    )
