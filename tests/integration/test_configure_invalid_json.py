import json

import pytest
from ska_tango_base.control_model import ObsState

from tests.integration.common import (
    ensure_checked_devices,
    set_mccs_subarray_ln_obsstate,
)
from tests.test_helpers.common_utils import SubarrayNode


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_configure_low_invalid_json(
    tango_context,
    json_factory,
    subarray_node: SubarrayNode,
    event_recorder,
):
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    set_mccs_subarray_ln_obsstate(ObsState.IDLE)
    assign_id = subarray_node.invoke_command(
        "AssignResources", json_factory("AssignResources_low")
    )
    subarray_node.assert_command_completion(assign_id, event_recorder)

    # Test configure with empty string as scan configuration
    subarray_node.invoke_and_assert_command_rejected(
        "Configure",
        "Malformed input JSON",
        input_str="",
    )

    subarray_node.wait_for_subarray_obsstate(ObsState.IDLE)

    # Test configure with empty dict as scan configuration
    with pytest.raises(Exception):
        subarray_node.invoke_and_assert_command_rejected(
            "Configure",
            "Malformed input JSON",
            input_str={},
        )
    subarray_node.wait_for_subarray_obsstate(ObsState.IDLE)

    configure_string = json_factory("Configure_low")
    # Test configure without csp scan configuration
    scan_configuration = json.loads(configure_string)
    del scan_configuration["csp"]
    subarray_node.invoke_and_assert_command_rejected(
        "Configure",
        "Missing key: 'csp'",
        input_str=json.dumps(scan_configuration),
    )
    subarray_node.wait_for_subarray_obsstate(ObsState.IDLE)

    # Test cofigure without sdp scan configuration
    scan_configuration = json.loads(configure_string)
    del scan_configuration["sdp"]
    subarray_node.invoke_and_assert_command_rejected(
        "Configure",
        "Missing key: 'sdp'",
        input_str=json.dumps(scan_configuration),
    )
    subarray_node.wait_for_subarray_obsstate(ObsState.IDLE)

    # Test cofigure without mccs scan configuration
    scan_configuration = json.loads(configure_string)
    del scan_configuration["mccs"]
    subarray_node.invoke_and_assert_command_rejected(
        "Configure",
        "Missing key: 'mccs'",
        input_str=json.dumps(scan_configuration),
    )
    subarray_node.wait_for_subarray_obsstate(ObsState.IDLE)

    # Test configure without scan duration
    scan_configuration = json.loads(configure_string)
    del scan_configuration["tmc"]["scan_duration"]
    subarray_node.invoke_and_assert_command_rejected(
        "Configure",
        "Missing key: 'scan_duration'",
        input_str=json.dumps(scan_configuration),
    )
    subarray_node.wait_for_subarray_obsstate(ObsState.IDLE)

    # TODO: Enable this once CDM validations are accomodated
    # Tel Model validations doesn't raise error for missing
    # "tmc" key since its optional in schema generation

    # # Test configure without tmc scan configuration
    # scan_configuration = json.loads(configure_string)
    # del scan_configuration["tmc"]
    # subarray_node.invoke_and_assert_command_rejected(
    #     "Configure",
    #     "Malformed input JSON",
    #     input_str=json.dumps(scan_configuration),
    # )
    # subarray_node.wait_for_subarray_obsstate(ObsState.IDLE)


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_configure_mid_invalid_json(
    tango_context,
    subarray_node: SubarrayNode,
    json_factory,
    event_recorder,
):
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    assign_id = subarray_node.invoke_command(
        "AssignResources", json_factory("AssignResources_mid")
    )
    subarray_node.assert_command_completion(assign_id, event_recorder)

    # Test configure with empty string as scan configuration
    subarray_node.invoke_and_assert_command_rejected(
        "Configure",
        "Malformed input JSON",
        input_str="",
    )

    subarray_node.wait_for_subarray_obsstate(ObsState.IDLE)

    # Test configure with empty dict as scan configuration
    with pytest.raises(Exception):
        subarray_node.invoke_and_assert_command_rejected(
            "Configure",
            "Malformed input JSON",
            input_str={},
        )
    subarray_node.wait_for_subarray_obsstate(ObsState.IDLE)
