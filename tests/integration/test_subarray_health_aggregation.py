# Enable the test when TANGO device commands are refactored as per bc 0.13

# import time

# import pytest
# from ska_tango_base.commands import ResultCode
# from ska_tango_base.control_model import HealthState
# from ska_tmc_common.dev_factory import DevFactory

# from tests.integration.common import devices_to_load  # noqa F401
# from tests.integration.common import ensure_checked_devices, tear_down
# from tests.settings import SLEEP_TIME, TIMEOUT, logger


# def check_subarrynode_healthstate(subarray_node, expected_health):
#     start_time = time.time()
#     while subarray_node.HealthState != expected_health:
#         time.sleep(SLEEP_TIME)
#         elapsed_time = time.time() - start_time
#         if elapsed_time > TIMEOUT:
#             pytest.fail(
#                 "Timeout occurred checking the SubarrayNode healthstate."
#             )


# def subarraynode_health_state(tango_context, subarraynode_name):
#     logger.info("%s", tango_context)
#     dev_factory = DevFactory()
#     subarray_node = dev_factory.get_device(subarraynode_name)
#     ensure_checked_devices(subarray_node)
#     initial_len = len(subarray_node.commandExecuted)
#     (_, unique_id) = subarray_node.On()

#     start_time = time.time()
#     while len(subarray_node.commandExecuted) != initial_len + 1:
#         time.sleep(SLEEP_TIME)
#         elapsed_time = time.time() - start_time
#         if elapsed_time > TIMEOUT:
#             pytest.fail("Timeout occurred while executing the test")

#     for command in subarray_node.commandExecuted:
#         if command[0] == unique_id[0]:
#             assert command[2] == "ResultCode.OK"

#     if "ska_mid" in subarraynode_name:
#         proxy = dev_factory.get_device("mid-csp/subarray/01")
#     else:
#         proxy = dev_factory.get_device("low-mccs/subarray/01")

#     # Set lower level device healthState to DEGRADED
#       and Check healthState DEGRADED aggregation
#     proxy.SetDirectHealthState(HealthState.DEGRADED)
#     assert proxy.HealthState == HealthState.DEGRADED

#     healtstate = subarray_node.read_attribute("healthState")
#     logger.info(f"SubarrayNode healthState is: {healtstate}")

#     check_subarrynode_healthstate(subarray_node, HealthState.DEGRADED)
#     assert subarray_node.HealthState == HealthState.DEGRADED

#     # Set lower level device healthState to OK
#       and Check healthState OK aggregation
#     proxy.SetDirectHealthState(HealthState.OK)
#     assert proxy.HealthState == HealthState.OK

#     healtstate = subarray_node.read_attribute("healthState")
#     logger.info(f"SubarrayNode healthState is: {healtstate}")

#     check_subarrynode_healthstate(subarray_node, HealthState.OK)
#     assert subarray_node.HealthState == HealthState.OK


# @pytest.mark.post_deployment
# @pytest.mark.SKA_mid
# def test_subarray_health_aggregation_mid(tango_context):
#     subarraynode_health_state(tango_context, "ska_mid/tm_subarray_node/1")

# @pytest.mark.post_deployment
# @pytest.mark.SKA_low
# def test_subarray_health_aggregation_low(tango_context):
#     subarraynode_health_state(tango_context, "ska_low/tm_subarray_node/1")
