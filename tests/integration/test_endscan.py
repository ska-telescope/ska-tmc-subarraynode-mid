import json

import pytest
from ska_control_model import ObsState
from ska_tango_base.commands import ResultCode
from ska_tango_testing.mock.placeholders import Anything

from tests.integration.common import (
    ensure_checked_devices,
    set_mccs_subarray_ln_obsstate,
    simulate_configure_on_dish_master,
)
from tests.settings import (
    ERROR_PROPAGATION_DEFECT,
    EXCEPTION_ALL_DISH,
    EXCEPTION_DISH,
    EXCEPTION_LOW_CSP,
    EXCEPTION_LOW_CSP_SDP,
    EXCEPTION_LOW_SDP,
    EXCEPTION_MCCS,
    EXCEPTION_MID_CSP,
    EXCEPTION_MID_SDP,
    FAILED_RESULT_DEFECT,
    FAILED_RESULT_EXCEPTION,
    TIMEOUT_DEFECT,
    TIMEOUT_DEFECT_DISH,
    TIMEOUT_EXCEPTION,
)
from tests.test_helpers.common_utils import SubarrayNode
from tests.test_helpers.constants import (
    DISH_LEAF_NODE,
    DISH_LEAF_NODE36,
    DISH_LEAF_NODE100,
    LOW_CSPSUBARRAY_LEAF_NODE,
    LOW_SDPSUBARRAY_LEAF_NODE,
    MCCS_SUBARRAY_LEAF_NODE,
    MID_CSPSUBARRAY_LEAF_NODE,
    MID_SDPSUBARRAY_LEAF_NODE,
    MID_SUBARRAY_NODE_NAME,
)


def endscan_command(
    tango_context, subarray_node: SubarrayNode, inputs: dict, event_recorder
):
    """Run the EndScan sequence."""
    ensure_checked_devices(subarray_node.subarray_node)
    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(2)
    assign_id = subarray_node.invoke_command(
        "AssignResources", inputs["ASSIGN"]
    )
    subarray_node.assert_command_completion(assign_id, event_recorder)

    configure_id = subarray_node.invoke_command(
        "Configure", inputs["CONFIGURE"]
    )
    if subarray_node.deployment == "MID":
        simulate_configure_on_dish_master()
    subarray_node.assert_command_completion(configure_id, event_recorder)

    scan_id = subarray_node.invoke_command("Scan", inputs["SCAN"])
    subarray_node.wait_for_subarray_obsstate(ObsState.SCANNING)
    subarray_node.assert_command_completion(scan_id, event_recorder)
    if subarray_node.deployment == "MID":
        subarray_node.wait_for_attribute_to_change_to(
            MID_SUBARRAY_NODE_NAME, "scanID", str(1)
        )

    endscan_id = subarray_node.invoke_command("EndScan")
    subarray_node.assert_command_completion(endscan_id, event_recorder)
    if subarray_node.deployment == "MID":
        subarray_node.wait_for_attribute_to_change_to(
            MID_SUBARRAY_NODE_NAME, "scanID", ""
        )
    subarray_node.wait_for_subarray_obsstate(ObsState.READY)


@pytest.mark.parametrize(
    "assign_str, configure_str, scan_str",
    [
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_end_scan_command(
    tango_context,
    subarray_node,
    assign_str,
    configure_str,
    scan_str,
    json_factory,
    event_recorder,
):
    """Test EndScan command."""
    inputs = {}
    inputs["ASSIGN"] = json_factory(assign_str)
    inputs["CONFIGURE"] = json_factory(configure_str)
    inputs["SCAN"] = json_factory(scan_str)
    endscan_command(
        tango_context,
        subarray_node,
        inputs,
        event_recorder,
    )


def endscan_command_failed_on_subsystem_before_queueing(
    tango_context,
    subarray_node: SubarrayNode,
    defective_device_names: list,
    healthy_device_names: list,
    assign_str: str,
    configure_str: str,
    scan_str: str,
    event_recorder,
):
    """Run Configure sequence with defective devices."""
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    assign_id = subarray_node.invoke_command("AssignResources", assign_str)
    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(ObsState.IDLE)
    subarray_node.assert_command_completion(assign_id, event_recorder)
    configure_id = subarray_node.invoke_command("Configure", configure_str)
    if subarray_node.deployment == "MID":
        simulate_configure_on_dish_master()
    subarray_node.wait_for_subarray_obsstate(ObsState.READY)
    subarray_node.assert_command_completion(configure_id, event_recorder)

    scan_id = subarray_node.invoke_command("Scan", scan_str)
    subarray_node.wait_for_subarray_obsstate(ObsState.SCANNING)
    subarray_node.assert_command_completion(scan_id, event_recorder)
    subarray_node.set_devices_defective(
        defective_device_names, FAILED_RESULT_DEFECT
    )
    endscan_id = subarray_node.invoke_command("EndScan")
    FAILED_RESULT = (
        "Error while invoking EndScan on the following devices: "
        + f"{defective_device_names[0]}: [3, {FAILED_RESULT_EXCEPTION}]"
    )
    subarray_node.assert_command_failure(
        endscan_id, FAILED_RESULT, event_recorder
    )

    # Verify that other devices have transitioned to READY obsState
    for device in healthy_device_names:
        subarray_node.wait_for_leaf_node_obsstate(device, ObsState.READY)
    subarray_node.reset_defects_for_devices(defective_device_names)


def explict_endscan_command_exception_handling(
    tango_context,
    subarray_node: SubarrayNode,
    defective_device_names: list,
    healthy_device_names: list,
    assign_str: str,
    configure_str: str,
    scan_str: str,
    event_recorder,
    defect_param: str,
    exception_message: str,
):
    """Run Configure sequence with defective devices."""
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    assign_id = subarray_node.invoke_command("AssignResources", assign_str)
    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(ObsState.IDLE)
    subarray_node.assert_command_completion(assign_id, event_recorder)
    configure_id = subarray_node.invoke_command("Configure", configure_str)
    if subarray_node.deployment == "MID":
        simulate_configure_on_dish_master()
    subarray_node.wait_for_subarray_obsstate(ObsState.READY)
    subarray_node.assert_command_completion(configure_id, event_recorder)

    scan_id = subarray_node.invoke_command("Scan", scan_str)
    subarray_node.wait_for_subarray_obsstate(ObsState.SCANNING)
    subarray_node.assert_command_completion(scan_id, event_recorder)

    subarray_node.set_devices_defective(defective_device_names, defect_param)
    endscan_id = subarray_node.invoke_command("EndScan")

    subarray_node.assert_command_failure(
        endscan_id, exception_message, event_recorder
    )

    # Verify that other devices have transitioned to READY obsState
    for device in healthy_device_names:
        subarray_node.wait_for_leaf_node_obsstate(device, ObsState.READY)

    subarray_node.reset_defects_for_devices(defective_device_names)


def implicit_endscan_command_exception_handling(
    tango_context,
    subarray_node: SubarrayNode,
    defective_device_names: list,
    healthy_device_names: list,
    assign_str: str,
    configure_str: str,
    scan_str: str,
    event_recorder,
    defect_param: str,
    exception_message: str,
):
    """Run Configure sequence with defective devices."""
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    assign_id = subarray_node.invoke_command("AssignResources", assign_str)
    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(ObsState.IDLE)
    subarray_node.assert_command_completion(assign_id, event_recorder)
    configure_id = subarray_node.invoke_command("Configure", configure_str)
    if subarray_node.deployment == "MID":
        simulate_configure_on_dish_master()
    subarray_node.wait_for_subarray_obsstate(ObsState.READY)
    subarray_node.assert_command_completion(configure_id, event_recorder)

    scan_id = subarray_node.invoke_command("Scan", scan_str)
    subarray_node.wait_for_subarray_obsstate(ObsState.SCANNING)
    subarray_node.assert_command_completion(scan_id, event_recorder)

    subarray_node.set_devices_defective(defective_device_names, defect_param)

    event_recorder.subscribe_event(
        subarray_node.subarray_node, "longRunningCommandResult"
    )
    assert event_recorder.has_change_event_occurred(
        subarray_node.subarray_node,
        "longRunningCommandResult",
        (
            Anything,
            json.dumps((int(ResultCode.FAILED), exception_message)),
        ),
    )

    # Verify that other devices have transitioned to READY obsState
    for device in healthy_device_names:
        subarray_node.wait_for_leaf_node_obsstate(device, ObsState.READY)

    subarray_node.reset_defects_for_devices(defective_device_names)


@pytest.mark.parametrize(
    "assign_str, configure_str, scan_str, defective_device_names, healthy_device_names",
    [
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [MID_CSPSUBARRAY_LEAF_NODE],
            ["SDPSUBARRAYLEAFNODE"],
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [MID_SDPSUBARRAY_LEAF_NODE],
            ["CSPSUBARRAYLEAFNODE"],
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [LOW_CSPSUBARRAY_LEAF_NODE],
            ["SDPSUBARRAYLEAFNODE", "MCCSSUBARRAYLEAFNODE"],
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [MCCS_SUBARRAY_LEAF_NODE],
            ["CSPSUBARRAYLEAFNODE", "SDPSUBARRAYLEAFNODE"],
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [LOW_SDPSUBARRAY_LEAF_NODE],
            ["MCCSSUBARRAYLEAFNODE", "CSPSUBARRAYLEAFNODE"],
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_endscan_command_failed_on_subsystem_before_queueing(
    tango_context,
    subarray_node,
    defective_device_names,
    healthy_device_names,
    assign_str,
    configure_str,
    scan_str,
    json_factory,
    event_recorder,
):
    """Test Configure command with defective devices
    for Timeout and Error Propagation."""
    endscan_command_failed_on_subsystem_before_queueing(
        tango_context,
        subarray_node,
        defective_device_names,
        healthy_device_names,
        json_factory(assign_str),
        json_factory(configure_str),
        json_factory(scan_str),
        event_recorder,
    )


@pytest.mark.parametrize(
    "assign_str, configure_str, scan_str,"
    + "defective_device_names, healthy_device_names,"
    + "exception_message, defective_params",
    [
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [DISH_LEAF_NODE, DISH_LEAF_NODE36, DISH_LEAF_NODE100],
            ["CSPSUBARRAYLEAFNODE", "SDPSUBARRAYLEAFNODE"],
            EXCEPTION_ALL_DISH,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [DISH_LEAF_NODE],
            ["CSPSUBARRAYLEAFNODE", "SDPSUBARRAYLEAFNODE"],
            EXCEPTION_DISH,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [MID_CSPSUBARRAY_LEAF_NODE],
            ["SDPSUBARRAYLEAFNODE"],
            EXCEPTION_MID_CSP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [MID_SDPSUBARRAY_LEAF_NODE],
            ["CSPSUBARRAYLEAFNODE"],
            EXCEPTION_MID_SDP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [DISH_LEAF_NODE],
            ["CSPSUBARRAYLEAFNODE", "SDPSUBARRAYLEAFNODE"],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT_DISH,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [MID_CSPSUBARRAY_LEAF_NODE],
            ["SDPSUBARRAYLEAFNODE"],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [MID_SDPSUBARRAY_LEAF_NODE],
            ["CSPSUBARRAYLEAFNODE"],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [LOW_CSPSUBARRAY_LEAF_NODE],
            ["SDPSUBARRAYLEAFNODE", "MCCSSUBARRAYLEAFNODE"],
            EXCEPTION_LOW_CSP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [MCCS_SUBARRAY_LEAF_NODE],
            ["CSPSUBARRAYLEAFNODE", "SDPSUBARRAYLEAFNODE"],
            EXCEPTION_MCCS,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [LOW_SDPSUBARRAY_LEAF_NODE],
            ["CSPSUBARRAYLEAFNODE", "MCCSSUBARRAYLEAFNODE"],
            EXCEPTION_LOW_SDP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [LOW_CSPSUBARRAY_LEAF_NODE],
            ["SDPSUBARRAYLEAFNODE", "MCCSSUBARRAYLEAFNODE"],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [MCCS_SUBARRAY_LEAF_NODE],
            ["SDPSUBARRAYLEAFNODE", "CSPSUBARRAYLEAFNODE"],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [LOW_SDPSUBARRAY_LEAF_NODE],
            ["CSPSUBARRAYLEAFNODE", "MCCSSUBARRAYLEAFNODE"],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [LOW_SDPSUBARRAY_LEAF_NODE, LOW_CSPSUBARRAY_LEAF_NODE],
            ["MCCSSUBARRAYLEAFNODE"],
            EXCEPTION_LOW_CSP_SDP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_explict_endscan_command_exception_handling(
    tango_context,
    subarray_node,
    defective_device_names,
    healthy_device_names,
    assign_str,
    configure_str,
    scan_str,
    json_factory,
    event_recorder,
    defective_params,
    exception_message,
):
    """Test End Scan command with defective devices
    for Timeout and Error Propagation."""
    explict_endscan_command_exception_handling(
        tango_context,
        subarray_node,
        defective_device_names,
        healthy_device_names,
        json_factory(assign_str),
        json_factory(configure_str),
        json_factory(scan_str),
        event_recorder,
        defective_params,
        exception_message,
    )


@pytest.mark.parametrize(
    "assign_str, configure_str, scan_str,"
    + "defective_device_names, healthy_device_names,"
    + "exception_message, defective_params",
    [
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [DISH_LEAF_NODE, DISH_LEAF_NODE36, DISH_LEAF_NODE100],
            ["CSPSUBARRAYLEAFNODE", "SDPSUBARRAYLEAFNODE"],
            EXCEPTION_ALL_DISH,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [DISH_LEAF_NODE],
            ["CSPSUBARRAYLEAFNODE", "SDPSUBARRAYLEAFNODE"],
            EXCEPTION_DISH,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [MID_CSPSUBARRAY_LEAF_NODE],
            ["SDPSUBARRAYLEAFNODE"],
            EXCEPTION_MID_CSP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [MID_SDPSUBARRAY_LEAF_NODE],
            ["CSPSUBARRAYLEAFNODE"],
            EXCEPTION_MID_SDP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [DISH_LEAF_NODE],
            ["CSPSUBARRAYLEAFNODE", "SDPSUBARRAYLEAFNODE"],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT_DISH,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [MID_CSPSUBARRAY_LEAF_NODE],
            ["SDPSUBARRAYLEAFNODE"],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            "Scan_mid",
            [MID_SDPSUBARRAY_LEAF_NODE],
            ["CSPSUBARRAYLEAFNODE"],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [LOW_CSPSUBARRAY_LEAF_NODE],
            ["SDPSUBARRAYLEAFNODE", "MCCSSUBARRAYLEAFNODE"],
            EXCEPTION_LOW_CSP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [MCCS_SUBARRAY_LEAF_NODE],
            ["CSPSUBARRAYLEAFNODE", "SDPSUBARRAYLEAFNODE"],
            EXCEPTION_MCCS,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [LOW_SDPSUBARRAY_LEAF_NODE],
            ["CSPSUBARRAYLEAFNODE", "MCCSSUBARRAYLEAFNODE"],
            EXCEPTION_LOW_SDP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [LOW_CSPSUBARRAY_LEAF_NODE],
            ["SDPSUBARRAYLEAFNODE", "MCCSSUBARRAYLEAFNODE"],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [MCCS_SUBARRAY_LEAF_NODE],
            ["SDPSUBARRAYLEAFNODE", "CSPSUBARRAYLEAFNODE"],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [LOW_SDPSUBARRAY_LEAF_NODE],
            ["CSPSUBARRAYLEAFNODE", "MCCSSUBARRAYLEAFNODE"],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            "Scan_low",
            [LOW_SDPSUBARRAY_LEAF_NODE, LOW_CSPSUBARRAY_LEAF_NODE],
            ["MCCSSUBARRAYLEAFNODE"],
            EXCEPTION_LOW_CSP_SDP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_implicit_endscan_command_exception_handling_new(
    tango_context,
    subarray_node,
    defective_device_names,
    healthy_device_names,
    assign_str,
    configure_str,
    scan_str,
    json_factory,
    event_recorder,
    defective_params,
    exception_message,
):
    """Test End Scan command with defective devices
    for Timeout and Error Propagation. In this case end scan is invoked from Scan command
    """
    implicit_endscan_command_exception_handling(
        tango_context,
        subarray_node,
        defective_device_names,
        healthy_device_names,
        json_factory(assign_str),
        json_factory(configure_str),
        json_factory(scan_str),
        event_recorder,
        defective_params,
        exception_message,
    )
