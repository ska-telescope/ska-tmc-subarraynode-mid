@XTP-4896
Feature: Subarray Node acceptance

	#Test the ability to generically run a a set of commands and that the execution is completed withing 5 seconds.
	@XTP-4893 @post_deployment @acceptance @SKA_mid @SKA_low
	Scenario: Ability to run commands on Subarray node
		Given a SubarrayNode device
		When I call the command <command_name>
		Then the <command_name> command is executed successfully on lower level devices

		Examples:
		| command_name		   |
		| On                   |
		| Off                  |
		| AssignResources      |
		| Configure            |
		| Scan                 |
		| EndScan              |
		| End                  |
		| ReleaseAllResources  |

	#Check subarray node correctly report failed and working devices defined within its scope of monitoring (internal model)
	@XTP-4895 @XTP-4893 @post_deployment @acceptance @SKA_mid @SKA_low
	Scenario Outline: Monitor SubarrayNode sub-devices
		Given a TANGO ecosystem with a set of devices deployed
		And a SubarrayNode device
		When I get the attribute InternalModel of the SubarrayNode device
		Then it correctly reports the failed and working devices
