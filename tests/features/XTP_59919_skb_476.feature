@XTP-59919 @XTP-28347 @post_deployment @acceptance @SKA_low
Scenario: Optional timing beams key in configure schema to resolve SKB-476
	Given Subarray Node in observation state IDLE
	When I invoke configure command on Subarray Node without key `timing_beams` in csp json
	Then Subarray Node transitions to observation state READY