#This test verifies TMC SubarrayNode does not accept "subarray_id" key in Scan Schema.
#This verifies the SKB-477
@XTP-60776 @XTP-28348 @Team_SAHYADRI @post_deployment @acceptance @SKA_low
Scenario: Verify SKB-477 - Unnecessary subarray_id key in Scan schema
    Given Subarray Node in observation state READY
    When I invoke Scan command on Subarray Node with key subarray_id
    Then TMC SubarrayNode raises exception with ResultCode.REJECTED
    And TMC SubarrayNode remains in observation state READY