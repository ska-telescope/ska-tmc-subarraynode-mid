"""Common utils class for a simple test harness."""
import json
import time
from functools import wraps
from operator import methodcaller
from typing import Any, Callable, List, Tuple

from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import AdminMode, HealthState, ObsState
from tango import DevFailed, DeviceProxy

from tests.integration.common import set_mccs_subarray_ln_obsstate
from tests.settings import RESET_DEFECT, TIMEOUT, logger
from tests.test_helpers.constants import (
    DISH_LEAF_NODE,
    DISH_MASTER_DEVICE1,
    LOW_CSPSUBARRAY_LEAF_NODE,
    LOW_CSPSUBARRAY_NODE_NAME,
    LOW_SDPSUBARRAY_LEAF_NODE,
    LOW_SDPSUBARRAY_NODE_NAME,
    LOW_SUBARRAY_NODE_NAME,
    MCCS_SUBARRAY_LEAF_NODE,
    MCCSSUBARRAY_NODE_NAME,
    MID_CSPSUBARRAY_LEAF_NODE,
    MID_CSPSUBARRAY_NODE_NAME,
    MID_SDPSUBARRAY_LEAF_NODE,
    MID_SDPSUBARRAY_NODE_NAME,
    MID_SUBARRAY_NODE_NAME,
)
from tests.test_helpers.event_recorder import EventRecorder

# The ObsStates in this dictionary lookup are in the order of ->
# SubarrayNode | CspSubarrayLeafNode | SdpSubarrayLeafNode
# RECOVERY = {
#   SubarrayNode ObsState: {
#       CspSubarrayLeafNode ObsState: {
#           SdpSubarrayLeafNode ObsState: {
#               DEVICES: [Devices on which commands are to be invoked]
#               ACTIONS: [Commands to be invoked for recovery]
#           }
#       }
#   }
# }

# TODO: Update the dictionary as per the approaches finalised in SP-3714
# RECOVERY: Dict[
#     ObsState, Dict[ObsState, Dict[ObsState, Dict[str, List[str]]]]
# ] = {
#     ObsState.RESOURCING: {
#         ObsState.EMPTY: {
#             ObsState.EMPTY: {
#                 "DEVICES": ["CSPSUBARRAYLEAFNODE"],
#                 "ACTIONS": ["RELEASERESOURCES"],
#             },
#             ObsState.RESOURCING: {
#                 "DEVICES": ["SDPSUBARRAYLEAFNODE"],
#                 "ACTIONS": ["ABORT", "RESTART"],
#             },
#             ObsState.IDLE: {
#                 "DEVICES": ["SDPSUBARRAYLEAFNODE"],
#                 "ACTIONS": ["RELEASERESOURCES"],
#             },
#         },
#         ObsState.RESOURCING: {
#             ObsState.EMPTY: {
#                 "DEVICES": ["CSPSUBARRAYLEAFNODE"],
#                 "ACTIONS": ["ABORT", "RESTART"],
#             },
#             ObsState.RESOURCING: {
#                 "DEVICES": ["SUBARRAYNODE"],
#                 "ACTIONS": ["ABORT", "RESTART"],
#             },
#             ObsState.IDLE: {
#                 "DEVICES": ["SUBARRAYNODE"],
#                 "ACTIONS": ["ABORT", "RESTART"],
#             },
#         },
#         ObsState.IDLE: {
#             ObsState.EMPTY: {
#                 "DEVICES": ["CSPSUBARRAYLEAFNODE"],
#                 "ACTIONS": ["RELEASERESOURCES"],
#             },
#             ObsState.RESOURCING: {
#                 "DEVICES": ["SUBARRAYNODE"],
#                 "ACTIONS": ["ABORT", "RESTART"],
#             },
#             ObsState.IDLE: {
#                 "DEVICES": ["CSPSUBARRAYLEAFNODE", "SDPSUBARRAYLEAFNODE"],
#                 "ACTIONS": ["RELEASERESOURCES"],
#             },
#         },
#     }
# }


def retry_invocation(function: Callable) -> Callable:
    """A decorator to retry command invocation if it fails due to
    unavailability."""

    @wraps(function)
    def wrapper(*args, **kwargs) -> Tuple[ResultCode, str]:
        """Wrapper method"""
        class_instance = args[0]
        try:
            return function(*args, **kwargs)
        except DevFailed as exception:
            logger.exception(
                "Command call raised a command not allowed exception: %s, "
                + "retrying after resetting availability",
                exception,
            )
            # If it is a 'TRANSIENT CORBA system exception', raise it
            if (
                "TRANSIENT CORBA system exception: TRANSIENT_CallTimedout"
                in str(exception)
            ):
                raise

            devices = [
                class_instance.devices["CSPSUBARRAYLEAFNODE"],
                class_instance.devices["SDPSUBARRAYLEAFNODE"],
            ]
            if class_instance.devices.get("DISHLEAFNODE"):
                devices.extend(class_instance.devices["DISHLEAFNODE"])
            else:
                devices.append(class_instance.devices["MCCSSUBARRAYLEAFNODE"])

            try:
                class_instance.set_devices_availability(devices, False)
                class_instance.wait_for_attribute_to_change_to(
                    class_instance.subarray_node, "isSubarrayAvailable", False
                )
                class_instance.set_devices_availability(devices, True)
                class_instance.wait_for_attribute_to_change_to(
                    class_instance.subarray_node, "isSubarrayAvailable", True
                )
                return function(*args, **kwargs)
            except AssertionError as assertion_error:
                logger.exception(
                    "Wait for Subarray Node's availability to be True failed. "
                    + "Exception: %s",
                    assertion_error,
                )
                raise
            except DevFailed as exception:
                logger.exception(
                    "Command call after retry raised a command not allowed "
                    + "exception: %s",
                    exception,
                )
                raise

    return wrapper


class SubarrayNode:
    """Class for Subarray Node device for test harness"""

    def __init__(self, deployment: str = "MID") -> None:
        self.deployment = deployment
        if deployment == "MID":
            self.subarray_node = DeviceProxy(MID_SUBARRAY_NODE_NAME)
            self.subarray_node.set_timeout_millis(5000)
            self.set_devices_availability(
                [
                    MID_CSPSUBARRAY_LEAF_NODE,
                    MID_SDPSUBARRAY_LEAF_NODE,
                    DISH_LEAF_NODE,
                ]
            )
            self.devices: dict = {
                "SUBARRAYNODE": MID_SUBARRAY_NODE_NAME,
                "CSPSUBARRAYLEAFNODE": MID_CSPSUBARRAY_LEAF_NODE,
                "SDPSUBARRAYLEAFNODE": MID_SDPSUBARRAY_LEAF_NODE,
                "DISHMASTERDEVICE": DISH_MASTER_DEVICE1,
                "DISHLEAFNODE": [DISH_LEAF_NODE],
            }
            self.subarray_devices: list = [
                MID_CSPSUBARRAY_NODE_NAME,
                MID_SDPSUBARRAY_NODE_NAME,
            ]
            self.csp_subarray_leaf_node = DeviceProxy(
                MID_CSPSUBARRAY_LEAF_NODE
            )
        else:
            # LOW deployment block
            self.subarray_node = DeviceProxy(LOW_SUBARRAY_NODE_NAME)
            self.subarray_node.set_timeout_millis(5000)
            self.set_devices_availability(
                [LOW_CSPSUBARRAY_LEAF_NODE, LOW_SDPSUBARRAY_LEAF_NODE]
            )
            self.devices: dict = {
                "SUBARRAYNODE": LOW_SUBARRAY_NODE_NAME,
                "CSPSUBARRAYLEAFNODE": LOW_CSPSUBARRAY_LEAF_NODE,
                "SDPSUBARRAYLEAFNODE": LOW_SDPSUBARRAY_LEAF_NODE,
                "MCCSSUBARRAYLEAFNODE": MCCS_SUBARRAY_LEAF_NODE,
            }
            self.subarray_devices: list = [
                LOW_CSPSUBARRAY_NODE_NAME,
                LOW_SDPSUBARRAY_NODE_NAME,
                MCCSSUBARRAY_NODE_NAME,
            ]
            # Initialize csp_subarray_leaf_node for LOW deployment
            self.csp_subarray_leaf_node = DeviceProxy(
                LOW_CSPSUBARRAY_LEAF_NODE
            )

    @retry_invocation
    def invoke_command(self, command_name: str, input_str: str = "") -> str:
        """Method to invoke given command on subarray node with given
        input json if any.

        Returns the unique id from command invocation."""
        logger.info(
            "Invoking %s command on Subarray Node: %s with input json: %s",
            command_name,
            self.subarray_node,
            input_str,
        )
        if input_str:
            command = methodcaller(command_name, input_str)
            result, unique_id = command(self.subarray_node)
        else:
            command = methodcaller(command_name)
            result, unique_id = command(self.subarray_node)

        logger.info(
            "Command invoked with unique_id: %s and result: %s",
            unique_id,
            result,
        )
        assert command_name in unique_id[0]
        assert result == ResultCode.QUEUED

        return unique_id[0]

    def assert_command_completion(
        self,
        unique_id: str,
        event_recorder: EventRecorder,
    ):
        """Checks if the command was completed successfully.

        Raises AssertionError in case of failure."""
        event_recorder.subscribe_event(
            self.subarray_node, "longRunningCommandResult"
        )
        assert event_recorder.has_change_event_occurred(
            self.subarray_node,
            "longRunningCommandResult",
            (unique_id, json.dumps((int(ResultCode.OK), "Command Completed"))),
        )

    def assert_command_failure(
        self,
        unique_id: str,
        exception_message: str,
        event_recorder: EventRecorder,
        result_code: ResultCode = ResultCode.FAILED,
    ):
        """Checks if the command pushed the correct error message event on its
        longRunningCommandResult attribute.

        Raises AssertionError in case of failure."""

        event_recorder.subscribe_event(
            self.subarray_node, "longRunningCommandResult"
        )
        assert event_recorder.has_change_event_occurred(
            self.subarray_node,
            "longRunningCommandResult",
            (
                unique_id,
                json.dumps((int(result_code), exception_message)),
            ),
        )

    def assert_obs_state(
        self,
        obsstate: ObsState,
    ):
        """Asserts the SubarrayNode ObsState to be the ObsState specified.

        Raises AssertionError in case of failure."""
        subarray_node_obs_state = self.subarray_node.read_attribute(
            "obsState"
        ).value
        logger.info("Subarray Node obsState is: %s", subarray_node_obs_state)
        assert subarray_node_obs_state == obsstate

    def assert_health_state(
        self,
        healthState: HealthState,
    ):
        """Asserts the SubarrayNode HealthState
        to be the HealthState specified.

        Raises AssertionError in case of failure."""
        subarray_node_health_state = self.subarray_node.read_attribute(
            "healthState"
        ).value
        logger.info(
            "Subarray Node healthState is: %s", subarray_node_health_state
        )
        assert subarray_node_health_state == healthState

    @retry_invocation
    def invoke_and_assert_command_rejected(
        self,
        command_name: str,
        exception_message: str,
        **kwargs,
    ) -> None:
        """Invokes the command on Subarray Node and asserts its rejection.
        Raises AssertionError in case of failure.

        :param command_name: Name of the command to be invoked.
        :type command_name: `str`
        :param exception_message: Exception message to assert after the command
            is rejected
        :type exception_message: `str`
        :param kwargs: Addtional keyword arguments. Example- input_str which is
            the input parameter for command call.
        """
        logger.info(
            "Invoking %s command on Subarray Node: %s with input json: %s",
            command_name,
            self.subarray_node,
            kwargs.get("input_str", ""),
        )
        if isinstance(kwargs.get("input_str", 0), str):
            command = methodcaller(command_name, kwargs["input_str"])
            result, message = command(self.subarray_node)
        else:
            command = methodcaller(command_name)
            result, message = command(self.subarray_node)

        logger.info(
            "Command invoked with message: %s and result: %s",
            message,
            result,
        )
        assert result == ResultCode.REJECTED
        assert exception_message in message[0]

    def invoke_abort_command(self, event_recorder: EventRecorder):
        """Invoke Abort command on Subarray Node"""
        logger.info(
            "Invoking Abort on Subarray Node: %s",
            self.subarray_node,
        )
        result, unique_id = self.subarray_node.Abort()

        logger.info(
            "Abort invoked with unique_id: %s and result: %s",
            unique_id,
            result,
        )
        event_recorder.subscribe_event(
            self.subarray_node, "longRunningCommandResult"
        )
        assert event_recorder.has_change_event_occurred(
            self.subarray_node,
            "longRunningCommandResult",
            (
                unique_id[0],
                json.dumps((int(ResultCode.STARTED), "Command Started")),
            ),
        )

    def set_subarray_health_state_to_ok(self, event_recorder):
        """Set Subarray Health state to OK"""
        subarray_node_health_state = self.subarray_node.read_attribute(
            "healthState"
        ).value
        if subarray_node_health_state != HealthState.OK:
            if self.subarray_node.isAdminModeEnabled:
                if self.deployment == "LOW":
                    proxy = DeviceProxy(LOW_CSPSUBARRAY_LEAF_NODE)
                    proxy.SetCspSubarrayLeafNodeAdminMode(AdminMode.ONLINE)

                    proxy = DeviceProxy(LOW_SDPSUBARRAY_LEAF_NODE)
                    proxy.SetSdpSubarrayLeafNodeAdminMode(AdminMode.ONLINE)

                    proxy = DeviceProxy(MCCS_SUBARRAY_LEAF_NODE)
                    proxy.SetMccsSubarrayLeafNodeAdminMode(AdminMode.ONLINE)
                else:
                    proxy = DeviceProxy(MID_CSPSUBARRAY_LEAF_NODE)
                    proxy.SetCspSubarrayLeafNodeAdminMode(AdminMode.ONLINE)

                    proxy = DeviceProxy(MID_SDPSUBARRAY_LEAF_NODE)
                    proxy.SetSdpSubarrayLeafNodeAdminMode(AdminMode.ONLINE)

            for device in self.subarray_devices:
                proxy = DeviceProxy(device)
                proxy.SetDirectHealthState(HealthState.OK)

            # If Health state is degraded because of k-value validation then
            # invoke release resources to clear k value validation
            obs_state = self.subarray_node.read_attribute("obsState").value
            if obs_state == ObsState.IDLE:
                if self.deployment == "LOW":
                    set_mccs_subarray_ln_obsstate(ObsState.EMPTY)
                release_id = self.invoke_command("ReleaseAllResources")
                self.assert_command_completion(release_id, event_recorder)

            self.wait_for_subarray_healthstate(HealthState.OK)
            if self.subarray_node.isAdminModeEnabled:
                # Keep this feature toggle false until all admin mode
                # functionality is implemented
                self.subarray_node.isAdminModeEnabled = False
        subarray_node_health_state = self.subarray_node.read_attribute(
            "healthState"
        ).value
        logger.debug("Subarray Health State is %s", subarray_node_health_state)

    def updated_tear_down(self, event_recorder: EventRecorder):
        """Teardown for integration and acceptance testing.
        Brings the subarray node back to EMPTY state.

        Raises given exception if the flag is set True."""

        self.set_subarray_health_state_to_ok(event_recorder)
        subarray_node_obsstate = self.subarray_node.read_attribute(
            "obsState"
        ).value
        device_states = self.get_device_obs_states()
        logger.info("Device ObsStates before teardown are: %s", device_states)

        match subarray_node_obsstate:
            case (
                ObsState.RESOURCING | ObsState.CONFIGURING | ObsState.SCANNING
            ):
                logger.debug(
                    "Doing an Abort Restart sequence to bring Subarray \
                    Node back to a stable state"
                )

                self.invoke_abort_command(event_recorder)
                self.wait_for_subarray_obsstate(ObsState.ABORTED)
                if self.deployment == "LOW":
                    set_mccs_subarray_ln_obsstate(ObsState.EMPTY)
                restart_id = self.invoke_command("Restart")
                self.assert_command_completion(restart_id, event_recorder)
                self.wait_for_subarray_obsstate(ObsState.EMPTY)

                off_id = self.invoke_command("Off")
                self.assert_command_completion(off_id, event_recorder)

            case (ObsState.FAULT | ObsState.ABORTED):
                logger.debug(
                    "Doing a Restart sequence to bring \
                    Subarray Node back to a stable state"
                )
                if self.deployment == "LOW":
                    set_mccs_subarray_ln_obsstate(ObsState.EMPTY)
                restart_id = self.invoke_command("Restart")
                self.assert_command_completion(restart_id, event_recorder)
                self.wait_for_subarray_obsstate(ObsState.EMPTY)

                off_id = self.invoke_command("Off")
                self.assert_command_completion(off_id, event_recorder)

            case ObsState.IDLE:
                logger.debug(
                    "Doing a ReleaseAllResources sequence to \
                    bring Subarray Node back to a stable state"
                )
                if self.deployment == "LOW":
                    set_mccs_subarray_ln_obsstate(ObsState.EMPTY)
                release_id = self.invoke_command("ReleaseAllResources")
                self.assert_command_completion(release_id, event_recorder)

                off_id = self.invoke_command("Off")
                self.assert_command_completion(off_id, event_recorder)

            case ObsState.READY:
                logger.debug(
                    "Doing an End and ReleaseAllResources \
                    sequence to bring Subarray Node back to a stable state"
                )
                end_id = self.invoke_command("End")
                self.assert_command_completion(end_id, event_recorder)
                self.wait_for_subarray_obsstate(ObsState.IDLE)

                if self.deployment == "LOW":
                    set_mccs_subarray_ln_obsstate(ObsState.EMPTY)
                release_id = self.invoke_command("ReleaseAllResources")
                self.assert_command_completion(release_id, event_recorder)

                off_id = self.invoke_command("Off")
                self.assert_command_completion(off_id, event_recorder)

            # TODO: Update the dictionary as per the approaches finalised in
            # SP-3714
            # case ObsState.RESOURCING:
            #     devices, actions = self.get_recovery_device_name_and_action(
            #         device_states
            #     )
            #     if "SUBARRAYNODE" in devices:
            #         logger.debug(
            #             "Doing an Abort Restart sequence to \
            #             bring Subarray Node back to a stable state"
            #         )
            #         self.invoke_abort_command(event_recorder)
            #         self.wait_for_subarray_obsstate(ObsState.ABORTED)

            #         if self.deployment == "LOW":
            #             set_mccs_subarray_ln_obsstate(ObsState.EMPTY)
            #         restart_id = self.invoke_command("Restart")
            #         self.assert_command_completion(
            # restart_id, event_recorder)
            #     else:
            #         self.perform_action(devices, actions)

            #     self.wait_for_subarray_obsstate(ObsState.EMPTY)
            #     off_id = self.invoke_command("Off")
            #     self.assert_command_completion(off_id, event_recorder)

            case ObsState.EMPTY:
                logger.debug("Turning Off Subarray Node")
                off_id = self.invoke_command("Off")
                self.assert_command_completion(off_id, event_recorder)

            case ObsState.ABORTED:
                if self.deployment == "LOW":
                    set_mccs_subarray_ln_obsstate(ObsState.EMPTY)
                restart_id = self.invoke_command("Restart")
                self.assert_command_completion(restart_id, event_recorder)

                self.wait_for_subarray_obsstate(ObsState.EMPTY)

                off_id = self.invoke_command("Off")
                self.assert_command_completion(off_id, event_recorder)

        subarray_node_obsstate = self.subarray_node.read_attribute("obsState")
        logger.debug(
            "Subarray ObsState after teardown is: %s",
            subarray_node_obsstate.value,
        )
        logger.info("Tear down complete.")

    def perform_action(self, device_names: List[str], actions: List[str]):
        """Performs the said action on the given device"""
        if device_names:
            for device in device_names:
                for action in actions:
                    match action:
                        case "RELEASERESOURCES":
                            self.invoke_command_on_device(
                                device, "ReleaseAllResources"
                            )
                            self.assert_command_completion_on_device(
                                device, ObsState.EMPTY
                            )
                        case "ABORT":
                            self.invoke_command_on_device(device, "Abort")
                            self.assert_command_completion_on_device(
                                device, ObsState.ABORTED
                            )
                        case "RESTART":
                            self.invoke_command_on_device(device, "Restart")
                            self.assert_command_completion_on_device(
                                device, ObsState.EMPTY
                            )
        else:
            logger.warning(
                "Device names list is Empty, actions cannot be performed"
            )

    def invoke_command_on_device(
        self, device_name: str, command: str, input_data=None
    ):
        """Invokes the given command on the device with provided input."""
        logger.info(
            "Invoking %s command on device: %s",
            command,
            self.devices[device_name],
        )
        device_proxy = DeviceProxy(self.devices[device_name])
        if input_data is not None:
            command_obj = methodcaller(command, input_data)
        else:
            command_obj = methodcaller(command)
        command_obj(device_proxy)

    def assert_command_completion_on_device(
        self, device_name: str, expected_state: ObsState
    ):
        """Check for device obsState to transition to the correct state."""
        self.wait_for_leaf_node_obsstate(device_name, expected_state)

    def get_device_obs_states(self) -> dict:
        """Returns a dictionary with current device ObsStates"""
        device_states = {}
        subarray_node_obsstate = self.subarray_node.read_attribute(
            "obsState"
        ).value
        device_states["SUBARRAYNODE"] = subarray_node_obsstate
        for device in self.devices.values():
            if isinstance(device, list):
                continue
            device_proxy = DeviceProxy(device)
            if "csp" in device:
                csp_leaf_node_obs_state = device_proxy.read_attribute(
                    "cspSubarrayObsState"
                ).value
                device_states["CSPSUBARRAYLEAFNODE"] = csp_leaf_node_obs_state
            elif "sdp" in device:
                sdp_leaf_node_obs_state = device_proxy.read_attribute(
                    "sdpSubarrayObsState"
                ).value
                device_states["SDPSUBARRAYLEAFNODE"] = sdp_leaf_node_obs_state
            elif "mccs" in device:
                mccs_leaf_node_obs_state = device_proxy.read_attribute(
                    "obsState"
                ).value
                device_states[
                    "MCCSSUBARRAYLEAFNODE"
                ] = mccs_leaf_node_obs_state
        return device_states

    # TODO: Update the dictionary as per the approaches finalised in
    # SP-3714
    # def get_recovery_device_name_and_action(
    #     self, current_states: dict
    # ) -> Tuple[List[str], List[str]]:
    #     """Matches the current device states with required states"""
    #     try:
    #         devices_and_actions: Dict[str, List[str]] = RECOVERY[
    #             current_states["SUBARRAYNODE"]
    #         ][current_states["CSPSUBARRAYLEAFNODE"]][
    #             current_states["SDPSUBARRAYLEAFNODE"]
    #         ]
    #         return (
    #             devices_and_actions["DEVICES"],
    #             devices_and_actions["ACTIONS"],
    #         )
    #     except KeyError as e:
    #         logger.info(
    #             "Key error occured while looking up recovery actions: %s", e
    #         )
    #         return [""], [""]

    def set_devices_defective(self, device_names: list, defective_params: str):
        """Sets the given devices defective with provided parameters."""
        for device_name in device_names:
            device_proxy = DeviceProxy(device_name)
            logger.info(
                "Setting the device: %s defective with params: %s",
                device_name,
                defective_params,
            )
            device_proxy.SetDefective(defective_params)

    def set_devices_availablity(self, device_availability: dict):
        """Sets the availability of  devices  with provided parameters.
        :param
        device_availability: Dictionary
        with device names as keys and their availability as values.
        :dtype: dict"""

        for device_name in device_availability:
            device_proxy = DeviceProxy(device_name)
            logger.info(
                "Setting the device : %s availability: %s",
                device_name,
                device_availability.get(device_name),
            )

            device_proxy.SetSubsystemAvailable(
                device_availability.get(device_name)
            )
            assert (
                device_proxy.isSubsystemAvailable
                is device_availability.get(device_name)
            )

    def reset_defects_for_devices(self, device_names: list):
        """Resets the defect parameters for given devices"""
        for device_name in device_names:
            device_proxy = DeviceProxy(device_name)
            logger.info(
                "Resetting the device: %s defective params",
                device_name,
            )
            device_proxy.SetDefective(RESET_DEFECT)
            self.wait_for_attribute_to_change_to(
                device_name, "defective", RESET_DEFECT
            )

    def wait_for_attribute_to_change_to(
        self, device: str, attribute_name: str, attribute_value: Any
    ) -> None:
        """Wait for the attribute to change to given value.

        :param device: Name of the device
        :param attribute_name: Attribute name as a string
        :param attribute_value: Value of attribute to be asserted
        """
        device_proxy = DeviceProxy(device)
        start_time = time.time()
        elapsed_time = time.time() - start_time
        current_value = device_proxy.read_attribute(attribute_name).value
        while current_value != attribute_value:
            elapsed_time = time.time() - start_time
            if elapsed_time >= TIMEOUT:
                raise AssertionError(
                    "Attribute value is not equal to given value. "
                    + f"Current value: {current_value}, expected value: "
                    + f"{attribute_value}"
                )
            current_value = device_proxy.read_attribute(attribute_name).value
            time.sleep(1)

    def wait_for_subarray_assign_resources(
        self, assign_resources_value: dict, timeout: int = 3
    ):
        """Wait for assign resources value of Subarray
        :param assign_resources_value: expected assign resource value
        :param timeout: timeout in sec for waiting to change assign resource
        """
        count = 0
        while count <= timeout:
            current_assign_resources = self.subarray_node.read_attribute(
                "assignedResources"
            ).value
            if current_assign_resources and json.loads(
                current_assign_resources[0]
            ) == json.loads(assign_resources_value):
                return True
            logger.info(
                "Assign Resource device value %s and expected value %s",
                current_assign_resources,
                assign_resources_value,
            )
            count += 1
            time.sleep(1)
        return False

    def wait_for_subarray_obsstate(
        self,
        obsstate: ObsState,
        timeout: int = 100,
    ):
        """Wait for Subarray Node ObsState to be the
        given obsstate till timeout occurs.

        Raises Exception in case of a timeout."""
        subarray_node_obsstate = self.subarray_node.read_attribute(
            "obsState"
        ).value
        logger.info(
            "Initial Subarray Node ObsState is: %s", subarray_node_obsstate
        )
        start_time = time.time()
        elapsed_time = 0
        while elapsed_time <= timeout and subarray_node_obsstate != obsstate:
            try:
                subarray_node_obsstate = self.subarray_node.read_attribute(
                    "obsState"
                ).value
            except DevFailed as dev_failed:
                logger.exception(
                    "Failed to read the attribute: %s", str(dev_failed.value)
                )
            time.sleep(1)
            elapsed_time = time.time() - start_time
            if int(elapsed_time) % 5 == 0:
                logger.info(
                    "Subarray Node ObsState while waiting is: %s",
                    subarray_node_obsstate,
                )
            if elapsed_time > timeout:
                logger.info(
                    "Subarray Node ObsState at end of wait is: %s",
                    subarray_node_obsstate,
                )
                logger.info(
                    "Current device ObsStates are: %s",
                    self.get_device_obs_states(),
                )
                raise Exception(
                    f"Timeout occured while waiting for Subarray \
                    Node to transition to ObsState: {obsstate}"
                )

    def wait_for_subarray_healthstate(
        self,
        healthstate: HealthState,
        timeout: int = 60,
    ):
        """Wait for Subarray Node healthState to be the given
        Healthstate till timeout occurs.

        Raises Exception in case of a timeout."""
        subarray_node_healthstate = self.subarray_node.read_attribute(
            "healthState"
        ).value
        start_time = time.time()
        elapsed_time = 0
        while (
            elapsed_time <= timeout
            and subarray_node_healthstate != healthstate
        ):
            subarray_node_healthstate = self.subarray_node.read_attribute(
                "healthState"
            ).value
            time.sleep(1)
            elapsed_time = time.time() - start_time
            if int(elapsed_time) % 5 == 0:
                logger.info(
                    "Subarray Node HealthState while waiting is: %s",
                    subarray_node_healthstate,
                )
            if elapsed_time > timeout:
                logger.info(
                    "Subarray Node HealthState at end of wait is: %s",
                    subarray_node_healthstate,
                )
                raise Exception(
                    f"Timeout occured while waiting for Subarray \
                    Node to transition to HealthState: {healthstate}"
                )

    def wait_for_leaf_node_obsstate(
        self,
        device_name: str,
        obsstate: ObsState,
        timeout: int = 100,
    ):
        """Wait for Device ObsState to be the given obsstate till timeout
        occurs.

        Raises Exception in case of a timeout."""
        if "CSP" in device_name:
            attr_name = "cspSubarrayObsState"
        elif "SDP" in device_name:
            attr_name = "sdpSubarrayObsState"
        else:
            attr_name = "obsState"
        device_proxy = DeviceProxy(self.devices[device_name])
        leaf_node_obsstate = device_proxy.read_attribute(attr_name).value
        logger.info(
            "Initial %s ObsState is: %s", device_name, leaf_node_obsstate
        )
        start_time = time.time()
        elapsed_time = 0
        while elapsed_time <= timeout and leaf_node_obsstate != obsstate:
            leaf_node_obsstate = device_proxy.read_attribute(attr_name).value
            time.sleep(1)
            elapsed_time = time.time() - start_time
            if int(elapsed_time) % 5 == 0:
                logger.info(
                    "%s ObsState while waiting is: %s",
                    device_name,
                    leaf_node_obsstate,
                )
            if elapsed_time > timeout:
                logger.info(
                    "%s ObsState at end of wait is: %s",
                    device_name,
                    leaf_node_obsstate,
                )
                logger.info(
                    "Current device ObsStates are: %s",
                    self.get_device_obs_states(),
                )
                raise Exception(
                    f"Timeout occured while waiting for {device_name} \
                    to transition to ObsState: {obsstate}"
                )

    def set_devices_availability(
        self, device_names: list, availability: bool = True
    ):
        """Set the device availability to the given value."""
        for device in device_names:
            device_proxy = DeviceProxy(device)
            device_proxy.SetSubsystemAvailable(availability)

    def clear_command_call_info(self):
        """Clears the command call info attribute."""
        for device in self.devices.values():
            if isinstance(device, list):
                continue
            if device in [MID_SUBARRAY_NODE_NAME, LOW_SUBARRAY_NODE_NAME]:
                continue
            logger.info(
                "Calling the command ClearCommandCallInfo on device: %s",
                device,
            )
            device_proxy = DeviceProxy(device)
            device_proxy.ClearCommandCallInfo()
