"""Tests for ObsStateAggregator"""

import time
from datetime import datetime
from os.path import dirname, join
from typing import Optional

from ska_control_model import ObsState

from ska_tmc_subarraynode.manager.event_data_manager import (
    CommandResultData,
    DishModeData,
    EventDataStorage,
    ObsStateData,
    PointingStateData,
)
from tests.settings import logger


def generate_data(
    command_in_progress,
    csp_obs_state,
    sdp_obs_state,
    pointing_state_1,
    pointing_state_2,
    dish_mode_1,
    dish_mode_2,
    command_result_csp,
    command_result_sdp,
):
    """Generate event data"""
    event_data = EventDataStorage(command_in_progress=command_in_progress)
    current_time = datetime.now()
    event_data.obs_state_data = {
        "csp/1": ObsStateData(
            obs_state=csp_obs_state, event_timestamp=current_time
        ),
        "sdp/2": ObsStateData(
            obs_state=sdp_obs_state, event_timestamp=current_time
        ),
    }
    event_data.command_result_data = {
        "csp/1": CommandResultData(
            result=command_result_csp,
            unique_id="100",
            event_timestamp=current_time,
        ),
        "sdp/1": CommandResultData(
            result=command_result_sdp,
            unique_id="100",
            event_timestamp=current_time,
        ),
    }
    event_data.pointing_state_data = {
        "dish/1": PointingStateData(
            pointing_state=pointing_state_1, event_timestamp=current_time
        ),
        "dish/2": PointingStateData(
            pointing_state=pointing_state_2, event_timestamp=current_time
        ),
    }
    event_data.dish_mode_data = {
        "dish/1": DishModeData(
            dish_mode=dish_mode_1, event_timestamp=current_time
        ),
        "dish/2": DishModeData(
            dish_mode=dish_mode_2, event_timestamp=current_time
        ),
    }
    return event_data


def test_aggregation_process(aggregation_process_mid):
    """Test aggregation process"""
    aggregation_process = aggregation_process_mid
    aggregation_process.start_aggregation_process()

    # The file contains all combinations of ObsStates, PointingStates,
    # DishModes and command results that are possible. The command in progress
    # variable is more of a placeholder except for ReleaseAllResources command
    with open(
        join(
            f"{dirname(__file__)}", "..", "..", "data", "aggregation_table.csv"
        ),
        mode="r",
        encoding="utf-8",
    ) as file:
        data = file.readlines()

    for row in data:
        logger.info("Current line: %s", row)
        (
            command_in_progress,
            csp_obs_state,
            sdp_obs_state,
            pointing_state_1,
            pointing_state_2,
            dish_mode_1,
            dish_mode_2,
            command_result_csp,
            command_result_sdp,
            expected_obs_state,
        ) = row.split(",")
        event_data = generate_data(
            command_in_progress,
            csp_obs_state,
            sdp_obs_state,
            pointing_state_1,
            pointing_state_2,
            dish_mode_1,
            dish_mode_2,
            command_result_csp,
            command_result_sdp,
        )
        expected_obs_state = expected_obs_state[:-1]
        aggregation_process.event_data_queue.put(event_data)
        if expected_obs_state != "None":
            expected_obs_state = ObsState[expected_obs_state]
        else:
            expected_obs_state = None
        wait_for_obs_state_to_be(aggregation_process, expected_obs_state)


def wait_for_obs_state_to_be(
    aggregation_process, expected_obs_state: Optional[ObsState]
):
    """Wait of aggreagation obsState to be given obsState."""
    for _ in range(5):
        if aggregation_process.aggregated_obs_state[0] == expected_obs_state:
            aggregation_process.aggregated_obs_state[0] = ""
            return
        time.sleep(0.05)
    assert aggregation_process.aggregated_obs_state[0] == expected_obs_state
    aggregation_process.aggregated_obs_state[0] = ""
