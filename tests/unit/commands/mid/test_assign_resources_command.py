import json
import logging
import time
from datetime import datetime, timedelta
from os.path import dirname, join

import mock
import pytest
from ska_control_model.faults import StateModelError
from ska_ser_logging import configure_logging
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import (
    AdapterType,
    DevFactory,
    DishMode,
    FaultType,
    HelperAdapterFactory,
)

from ska_tmc_subarraynode.commands.assign_resources_command import (
    AssignResources,
)
from ska_tmc_subarraynode.exceptions import CommandNotAllowed
from ska_tmc_subarraynode.manager.event_data_manager import EventDataManager
from ska_tmc_subarraynode.model.input import InputParameterMid
from src.ska_tmc_subarraynode.model.enum import PointingState
from tests.settings import (
    MID_CSPSUBARRAY_LEAF_NODE,
    SUBARRAY_LEAF_NODES_MID,
    create_mid_cm,
    logger,
    set_sdp_csp_leaf_node_availability_for_aggregation,
    simulate_obs_state_and_result_code_events,
    simulate_obs_state_events,
)

configure_logging(logging.DEBUG)
LOGGER = logging.getLogger(__name__)


def get_assign_input_str(assign_input_file="AssignResources_mid.json"):
    path = join(dirname(__file__), "..", "..", "..", "data", assign_input_file)
    with open(path, "r") as f:
        assign_input_str = f.read()
    return assign_input_str


def test_mid_assign_task_completed(
    tango_context, component_manager_mid, task_callback
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    assign_str = get_assign_input_str()
    component_manager_mid.is_command_allowed("AssignResources")
    component_manager_mid.initial_obs_state = ObsState.EMPTY
    component_manager_mid.assign(argin=assign_str, task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    # Validate assigned resources is empty
    assert not component_manager_mid.assigned_resources
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )

    simulate_obs_state_and_result_code_events(
        component_manager_mid,
        SUBARRAY_LEAF_NODES_MID,
        [ObsState.IDLE],
        "AssignResources",
        ResultCode.OK,
    )
    task_callback.assert_against_call(
        call_kwargs={
            "status": TaskStatus.COMPLETED,
            "result": (ResultCode.OK, "Command Completed"),
        }
    )
    assert component_manager_mid.assigned_resources == [
        "SKA001",
        "SKA036",
        "SKA100",
    ]


def test_assign_resources_timeout(
    tango_context, component_manager_mid, task_callback
):
    component_manager_mid.command_timeout = 2
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    component_manager_mid.is_command_allowed("AssignResources")
    component_manager_mid.initial_obs_state = ObsState.EMPTY
    defect = {
        "enabled": True,
        "fault_type": FaultType.STUCK_IN_INTERMEDIATE_STATE,
        "error_message": "Command stuck in processing",
        "result": ResultCode.FAILED,
        "intermediate_state": ObsState.RESOURCING,
    }
    csp_sln = DevFactory().get_device(MID_CSPSUBARRAY_LEAF_NODE)
    csp_sln.SetDefective(json.dumps(defect))
    assign_str = get_assign_input_str()
    component_manager_mid.assign(assign_str, task_callback)

    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert "Timeout has occurred, command failed" == result["result"][1]
    csp_sln.SetDefective(json.dumps({"enabled": False}))


def test_subarray_assign_resources_error_propagation_command_not_allowed(
    tango_context, component_manager_mid, task_callback
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    component_manager_mid.is_command_allowed("AssignResources")
    component_manager_mid.initial_obs_state = ObsState.EMPTY
    defect = {
        "enabled": True,
        "fault_type": FaultType.COMMAND_NOT_ALLOWED_AFTER_QUEUING,
        "error_message": "Command not allowed on leaf node.",
        "result": ResultCode.FAILED,
    }
    csp_sln = DevFactory().get_device(MID_CSPSUBARRAY_LEAF_NODE)
    csp_sln.SetDefective(json.dumps(defect))
    assign_str = get_assign_input_str()
    component_manager_mid.assign(assign_str, task_callback)

    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    simulate_obs_state_and_result_code_events(
        component_manager_mid,
        [MID_CSPSUBARRAY_LEAF_NODE],
        [],
        "AssignResources",
        ResultCode.NOT_ALLOWED,
    )
    time.sleep(5)
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    csp_sln.SetDefective(json.dumps({"enabled": False}))


@pytest.mark.parametrize("missing_key", ("eb_id", "sdp", "receptor_ids"))
def test_subarray_assign_resources_json_with_missing_key(
    tango_context, component_manager_mid, task_callback, missing_key
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    component_manager_mid.is_command_allowed("AssignResources")
    component_manager_mid.initial_obs_state = ObsState.EMPTY
    assign_str = get_assign_input_str()
    json_argument = json.loads(assign_str)
    if missing_key == "eb_id":
        del json_argument["sdp"]["execution_block"]["eb_id"]
    elif missing_key == "sdp":
        del json_argument["sdp"]
    elif missing_key == "receptor_ids":
        del json_argument["dish"]["receptor_ids"]

    component_manager_mid.assign(json.dumps(json_argument), task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert f"JSON Error: Missing '{missing_key}" in result["result"][1]


def test_subarray_assign_resources_empty_input_json(
    tango_context, component_manager_mid, task_callback
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    component_manager_mid.is_command_allowed("AssignResources")
    component_manager_mid.initial_obs_state = ObsState.EMPTY
    component_manager_mid.assign("", task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert (
        "Error in parsing the JSON string in AssignResources command:"
        in result["result"][1]
    )


def test_subarray_assign_resources_fail_is_allowed(
    tango_context, component_manager_mid
):
    start_time = time.time()
    elapsed_time = time.time() - start_time
    logger.info(
        "checked %s devices in %s",
        len(component_manager_mid.checked_devices),
        elapsed_time,
    )
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, False
    )

    with pytest.raises(CommandNotAllowed):
        callable = component_manager_mid.command_allowed_callable(
            "AssignResources"
        )
        callable()


def test_subarray_assign_resources_fail_csp_subarray_ln(
    tango_context, task_callback, component_manager_mid
):
    start_time = time.time()
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    elapsed_time = time.time() - start_time
    logger.info(
        "checked %s devices in %s",
        len(component_manager_mid.checked_devices),
        elapsed_time,
    )
    logger.info("%s", tango_context)

    component_manager_mid.is_command_allowed("AssignResources")
    component_manager_mid.initial_obs_state = ObsState.EMPTY
    helper_adapter_factory = HelperAdapterFactory()

    # include exception in AssignResources command
    failing_dev = MID_CSPSUBARRAY_LEAF_NODE
    attrs = {"AssignResources.side_effect": Exception}
    CspsubarraylnMock = mock.Mock(**attrs)
    helper_adapter_factory.get_or_create_adapter(
        failing_dev, AdapterType.CSPSUBARRAY, proxy=CspsubarraylnMock
    )
    component_manager_mid.adapter_factory = helper_adapter_factory

    assign_str = get_assign_input_str()
    component_manager_mid.assign(assign_str, task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert "Error while invoking AssignResources on" in result["result"][1]


@pytest.mark.parametrize(
    "not_allowed_obs_state",
    ("ABORTED", "FAULT"),
)
def test_mid_subarray_assign_resources_raises_state_model_exception(
    tango_context, not_allowed_obs_state, component_manager_mid
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    exception_message = "AssignResources command not permitted"
    component_manager_mid.obs_state_model._straight_to_state(
        not_allowed_obs_state
    )
    with pytest.raises(StateModelError, match=exception_message):
        callable = component_manager_mid.command_allowed_callable(
            "AssignResources"
        )
        callable()


@pytest.mark.skip(reason="")
def test_event_manager(tango_context, task_callback):
    print("Test started")
    cm, start_time, _ = create_mid_cm()

    ed = EventDataManager(cm)

    # Assume current_time is already defined as a datetime object
    tv1 = datetime.now()
    tv2 = tv1 + timedelta(milliseconds=3500)
    tv3 = tv1 + timedelta(milliseconds=1500)
    tv4 = tv1 + timedelta(milliseconds=500)

    # Convert current_time to string
    tv11 = tv1.strftime("%d/%m/%Y %H:%M:%S:%f")
    print(f"Original time: {tv11}")

    # Convert the updated time back to string if needed
    tv22 = tv2.strftime("%d/%m/%Y %H:%M:%S:%f")
    print(f"updated time: {tv22}")

    ed.update_event_data(
        device="CSP",
        data=ObsState.EMPTY,
        received_timestamp=tv1,
        data_type="ObsState",
    )

    ed.update_event_data(
        device="SDP",
        data=(999, json.dumps((ResultCode.UNKNOWN, ""))),
        received_timestamp=tv1,
        data_type="CommandResultData",
    )

    ed.update_event_data(
        device="dish01",
        data=DishMode.OPERATE,
        received_timestamp=tv2,
        data_type="DishMode",
    )

    ed.update_event_data(
        device="dish01",
        data=PointingState.TRACK,
        received_timestamp=tv2,
        data_type="PointingState",
    )

    # Fresh event

    ed.update_event_data(
        device="CSP",
        data=ObsState.READY,
        received_timestamp=tv2,
        data_type="ObsState",
    )

    # Stale Event

    ed.update_event_data(
        device="CSP",
        data=ObsState.ABORTED,
        received_timestamp=tv1,
        data_type="ObsState",
    )

    # Updated LRCR

    ed.update_event_data(
        device="SDP",
        data=(999, json.dumps((ResultCode.FAILED, ""))),
        received_timestamp=tv4,
        data_type="CommandResultData",
    )

    ed.update_event_data(
        device="SDP",
        data=(999, json.dumps((ResultCode.OK, ""))),
        received_timestamp=tv3,
        data_type="CommandResultData",
    )

    with ed.component_manager.process_lock:
        size = ed.component_manager.event_data_queue.qsize()
        LOGGER.info(f"qsize = {size}")
        assert size == 9

        event_items = []
        while not ed.component_manager.event_data_queue.empty():
            try:
                item = ed.component_manager.event_data_queue.get(
                    timeout=15
                )  # Timeout after 5 seconds
                event_items.append(item)
            except Exception:
                print(
                    "Queue is empty or timeout occurred while getting an item"
                )
                break

        if event_items:
            LOGGER.info("Items in queue:")
            for item in event_items:
                LOGGER.info(f"item = {item}")
        else:
            LOGGER.info("No event_items were retrieved from the queue")

    LOGGER.info("Finished processing the queue")
