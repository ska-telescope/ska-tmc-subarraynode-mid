import json
import time
from os.path import dirname, join

import mock
import pytest
from ska_control_model import ObsState
from ska_control_model.faults import StateModelError
from ska_tango_base.commands import ResultCode
from ska_tango_base.executor import TaskStatus
from ska_tmc_common.adapters import AdapterType
from ska_tmc_common.op_state_model import TMCOpStateModel
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)

from ska_tmc_subarraynode.commands.scan_command import Scan
from ska_tmc_subarraynode.exceptions import CommandNotAllowed
from ska_tmc_subarraynode.model.input import InputParameterMid
from tests.mock_callable import MockCallable
from tests.settings import (
    SUBARRAY_LEAF_NODES_MID,
    create_dish_mocks,
    create_mid_cm,
    logger,
    set_sdp_csp_leaf_node_availability_for_aggregation,
    setup_mapping_scan_data_for_cm,
    simulate_obs_state_and_result_code_events,
    simulate_obs_state_events,
)
from tests.test_helpers.constants import (
    DISH_LEAF_NODE,
    DISH_LEAF_NODE36,
    DISH_LEAF_NODE100,
    MID_CSPSUBARRAY_LEAF_NODE,
    MID_SDPSUBARRAY_LEAF_NODE,
)

op_state_model = TMCOpStateModel(logger)


def get_scan_input_str(scan_input_file="Scan_mid.json"):
    path = join(dirname(__file__), "..", "..", "..", "data", scan_input_file)
    with open(path, "r") as f:
        scan_input_file = f.read()
    return scan_input_file


def get_scan_command_obj():
    cm, start_time, obs_state_model = create_mid_cm()
    elapsed_time = time.time() - start_time
    logger.info(
        "checked %s devices in %s", len(cm.checked_devices), elapsed_time
    )
    my_adapter_factory = HelperAdapterFactory()
    scan_command = Scan(cm, obs_state_model, my_adapter_factory, logger=logger)
    return scan_command, my_adapter_factory, cm, obs_state_model


@pytest.mark.parametrize(
    "device_name, adapter_type",
    [
        pytest.param(MID_CSPSUBARRAY_LEAF_NODE, AdapterType.SUBARRAY),
        pytest.param(MID_SDPSUBARRAY_LEAF_NODE, AdapterType.SDPSUBARRAY),
        pytest.param(DISH_LEAF_NODE, AdapterType.DISH),
    ],
)
def test_subarray_scan_fail_on_subarray_ln(
    tango_context,
    task_callback,
    device_name,
    adapter_type,
    component_manager_mid,
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )

    helper_adapter_factory = HelperAdapterFactory()
    failing_dev = device_name
    attrs = {"Scan.side_effect": Exception}
    subarrayleafnode_mock = mock.Mock(**attrs)
    helper_adapter_factory.get_or_create_adapter(
        failing_dev, adapter_type, proxy=subarrayleafnode_mock
    )

    simulate_obs_state_events(
        component_manager_mid, SUBARRAY_LEAF_NODES_MID, [ObsState.READY]
    )
    component_manager_mid.obs_state_model._straight_to_state("READY")
    component_manager_mid.is_command_allowed("Scan")
    component_manager_mid.initial_obs_state = ObsState.READY
    component_manager_mid.adapter_factory = helper_adapter_factory
    scan_str = get_scan_input_str()
    component_manager_mid.scan(argin=scan_str, task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert "Error while invoking Scan on " in result["result"][1]


def test_scan_empty_input_json(
    tango_context, task_callback, component_manager_mid
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    component_manager_mid.is_command_allowed("Scan")
    component_manager_mid.adapter_factory = HelperAdapterFactory()
    result_code, _ = component_manager_mid.scan(
        " ", task_callback=task_callback
    )
    assert result_code == TaskStatus.REJECTED


def test_scan_fail_check_allowed(tango_context, component_manager_mid):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid,
        False,
        InputParameterMid(None),
    )
    with pytest.raises(CommandNotAllowed):
        callable = component_manager_mid.command_allowed_callable("Scan")
        callable()


def test_mid_scan_task_completed(
    tango_context, task_callback, component_manager_mid
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    simulate_obs_state_events(
        component_manager_mid, SUBARRAY_LEAF_NODES_MID, [ObsState.READY]
    )

    component_manager_mid.set_assigned_resources([])
    component_manager_mid.set_scan_duration(10)
    component_manager_mid.obs_state_model._straight_to_state("READY")
    component_manager_mid.initial_obs_state = ObsState.READY
    component_manager_mid.is_command_allowed("Scan")
    scan_input = get_scan_input_str()
    component_manager_mid.scan(scan_input, task_callback)

    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )

    simulate_obs_state_and_result_code_events(
        component_manager_mid,
        SUBARRAY_LEAF_NODES_MID,
        [ObsState.SCANNING],
        "Scan",
        ResultCode.OK,
    )
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )


def test_mapping_scan(tango_context, task_callback, component_manager_mid):
    """Test Mapping scan"""
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    simulate_obs_state_events(
        component_manager_mid, SUBARRAY_LEAF_NODES_MID, [ObsState.READY]
    )
    dish_list = [
        DISH_LEAF_NODE,
        DISH_LEAF_NODE36,
        DISH_LEAF_NODE100,
    ]
    helper_adapter_factory = HelperAdapterFactory()
    dish_mock_list = create_dish_mocks(dish_list, helper_adapter_factory)
    component_manager_mid.adapter_factory = helper_adapter_factory
    component_manager_mid.obs_state_model._straight_to_state("READY")
    component_manager_mid.initial_obs_state = ObsState.READY
    setup_mapping_scan_data_for_cm(component_manager_mid)
    component_manager_mid.is_command_allowed("Scan")
    scan_input = get_scan_input_str()
    scan_json = json.loads(scan_input)
    scan_json["scan_ids"] = [1, 2]
    component_manager_mid.scan(json.dumps(scan_json), task_callback)

    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )

    simulate_obs_state_and_result_code_events(
        component_manager_mid,
        SUBARRAY_LEAF_NODES_MID,
        [ObsState.SCANNING],
        "Scan",
        ResultCode.OK,
    )
    assert component_manager_mid.is_mapping_scan

    simulate_obs_state_and_result_code_events(
        component_manager_mid,
        SUBARRAY_LEAF_NODES_MID,
        [ObsState.READY],
        "Scan",
        ResultCode.OK,
    )
    simulate_obs_state_and_result_code_events(
        component_manager_mid,
        SUBARRAY_LEAF_NODES_MID,
        [ObsState.SCANNING],
        "Scan",
        ResultCode.OK,
    )
    # Validate that for pointing dish scan is invoked only once
    # and for trajectory dishes scan in invoked 2 times because
    # two scan ids provided to scan command
    for dish_m in dish_mock_list:
        if dish_m.dev_name == DISH_LEAF_NODE100:
            assert len(dish_m.Scan.call_args_list) == 1
        else:
            assert len(dish_m.Scan.call_args_list) == 2
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )


def test_invalid_mapping_scan(
    tango_context, task_callback, component_manager_mid
):
    """Test that the mapping scan is rejected if the length of the scan IDs
    list does not match the length of the offsets list.
    """
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    simulate_obs_state_events(
        component_manager_mid, SUBARRAY_LEAF_NODES_MID, [ObsState.READY]
    )
    dish_list = [
        DISH_LEAF_NODE,
        DISH_LEAF_NODE36,
        DISH_LEAF_NODE100,
    ]
    helper_adapter_factory = HelperAdapterFactory()
    create_dish_mocks(dish_list, helper_adapter_factory)
    component_manager_mid.adapter_factory = helper_adapter_factory
    component_manager_mid.obs_state_model._straight_to_state("READY")
    component_manager_mid.initial_obs_state = ObsState.READY
    setup_mapping_scan_data_for_cm(component_manager_mid)
    component_manager_mid.is_command_allowed("Scan")
    scan_input = get_scan_input_str()
    scan_json = json.loads(scan_input)
    scan_json["scan_ids"] = [1, 2, 3]
    result_code, _ = component_manager_mid.scan(
        json.dumps(scan_json), task_callback
    )
    assert result_code == TaskStatus.REJECTED


@pytest.mark.parametrize(
    "device_name, adapter_type",
    [
        pytest.param(MID_CSPSUBARRAY_LEAF_NODE, AdapterType.SUBARRAY),
        pytest.param(MID_SDPSUBARRAY_LEAF_NODE, AdapterType.SDPSUBARRAY),
        pytest.param(DISH_LEAF_NODE, AdapterType.DISH),
    ],
)
def test_subarray_mapping_scan_fail_on_subarray_ln(
    tango_context,
    task_callback,
    device_name,
    adapter_type,
    component_manager_mid,
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )

    helper_adapter_factory = HelperAdapterFactory()
    failing_dev = device_name
    attrs = {"Scan.side_effect": Exception}
    subarrayleafnode_mock = mock.Mock(**attrs)
    helper_adapter_factory.get_or_create_adapter(
        failing_dev, adapter_type, proxy=subarrayleafnode_mock
    )
    dish_list = [
        DISH_LEAF_NODE,
        DISH_LEAF_NODE36,
        DISH_LEAF_NODE100,
    ]
    create_dish_mocks(dish_list, helper_adapter_factory)

    simulate_obs_state_events(
        component_manager_mid, SUBARRAY_LEAF_NODES_MID, [ObsState.READY]
    )
    setup_mapping_scan_data_for_cm(component_manager_mid)

    component_manager_mid.obs_state_model._straight_to_state("READY")
    component_manager_mid.is_command_allowed("Scan")
    component_manager_mid.initial_obs_state = ObsState.READY
    component_manager_mid.adapter_factory = helper_adapter_factory
    scan_str = get_scan_input_str()
    component_manager_mid.scan(argin=scan_str, task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert "Error while invoking Scan on " in result["result"][1]
    assert not component_manager_mid.is_mapping_scan


@pytest.mark.parametrize(
    "not_allowed_obs_state",
    ("IDLE", "SCANNING"),
)
def test_mid_subarray_scan_raises_state_model_exception(
    tango_context, not_allowed_obs_state, component_manager_mid
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True
    )
    exception_message = "Scan command not permitted"
    component_manager_mid.obs_state_model._straight_to_state(
        not_allowed_obs_state
    )
    with pytest.raises(StateModelError, match=exception_message):
        callable = component_manager_mid.command_allowed_callable("Scan")
        callable()
