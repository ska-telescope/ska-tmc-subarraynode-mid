import json
import time
from os.path import dirname, join

import mock
import pytest
from ska_control_model.faults import StateModelError
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import AdapterType, DevFactory, FaultType
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)
from tango import DevState

from ska_tmc_subarraynode.commands.release_all_resources_command import (
    ReleaseAllResources,
)
from ska_tmc_subarraynode.exceptions import CommandNotAllowed
from ska_tmc_subarraynode.model.input import InputParameterMid
from tests.settings import (
    SUBARRAY_LEAF_NODES_MID,
    create_mid_cm,
    logger,
    set_sdp_csp_leaf_node_availability_for_aggregation,
    simulate_obs_state_and_result_code_events,
    simulate_obs_state_events,
)
from tests.test_helpers.constants import MID_CSPSUBARRAY_LEAF_NODE


def get_assign_input_str(assign_input_file="AssignResources_mid.json"):
    path = join(dirname(__file__), "..", "..", "..", "data", assign_input_file)
    with open(path, "r") as f:
        assign_input_str = f.read()
    return assign_input_str


def get_release_resources_command_obj():
    cm, start_time, obs_state_model = create_mid_cm()
    elapsed_time = time.time() - start_time
    logger.info(
        "checked %s devices in %s", len(cm.checked_devices), elapsed_time
    )
    helper_adapter_factory = HelperAdapterFactory()
    release_res_command = ReleaseAllResources(
        cm,
        obs_state_model=obs_state_model,
        logger=logger,
        adapter_factory=helper_adapter_factory,
    )
    return release_res_command, helper_adapter_factory, cm, obs_state_model


def test_mid_release_all_resources_task_completed(
    tango_context, task_callback, component_manager_mid
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    simulate_obs_state_events(
        component_manager_mid,
        SUBARRAY_LEAF_NODES_MID,
        [ObsState.IDLE],
    )
    component_manager_mid.obs_state_model._straight_to_state("IDLE")
    component_manager_mid.initial_obs_state = ObsState.IDLE
    component_manager_mid.is_command_allowed("ReleaseAllResources")
    component_manager_mid.set_assigned_resources(["SKA001"])
    component_manager_mid.input_parameter.dish_master_identifier = "elt/master"
    component_manager_mid.release_all(task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    simulate_obs_state_and_result_code_events(
        component_manager_mid,
        SUBARRAY_LEAF_NODES_MID,
        [ObsState.EMPTY],
        "ReleaseAllResources",
        ResultCode.OK,
    )
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )


def test_release_all_resources_timeout(
    tango_context, task_callback, component_manager_mid
):
    component_manager_mid.command_timeout = 2
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    component_manager_mid.obs_state_model._straight_to_state("IDLE")
    component_manager_mid.initial_obs_state = ObsState.IDLE
    component_manager_mid.is_command_allowed("ReleaseAllResources")
    component_manager_mid.set_assigned_resources(["SKA001"])
    defect = {
        "enabled": True,
        "fault_type": FaultType.STUCK_IN_INTERMEDIATE_STATE,
        "error_message": "Command stuck in processing",
        "result": ResultCode.FAILED,
        "intermediate_state": ObsState.RESOURCING,
    }
    csp_sln = DevFactory().get_device(MID_CSPSUBARRAY_LEAF_NODE)
    csp_sln.SetDefective(json.dumps(defect))
    component_manager_mid.release_all(task_callback)

    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert "Timeout has occurred, command failed" in result["result"][1]
    csp_sln.SetDefective(json.dumps({"enabled": False}))


def test_subarray_release_all_resources_error_propagation_command_not_allowed(
    tango_context, task_callback, component_manager_mid
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    component_manager_mid.obs_state_model._straight_to_state("IDLE")
    component_manager_mid.initial_obs_state = ObsState.IDLE
    component_manager_mid.is_command_allowed("ReleaseAllResources")
    component_manager_mid.set_assigned_resources(["SKA001"])
    defect = {
        "enabled": True,
        "fault_type": FaultType.COMMAND_NOT_ALLOWED_BEFORE_QUEUING,
        "error_message": "Command not allowed on leaf node.",
        "result": ResultCode.FAILED,
    }
    csp_sln = DevFactory().get_device(MID_CSPSUBARRAY_LEAF_NODE)
    csp_sln.SetDefective(json.dumps(defect))
    component_manager_mid.release_all(task_callback)

    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert "Command not allowed on leaf node." in result["result"][1]
    csp_sln.SetDefective(json.dumps({"enabled": False}))


def test_release_resources_fail_subarray(
    tango_context, task_callback, component_manager_mid
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )

    helper_adapter_factory = HelperAdapterFactory()
    # include exception in ReleaseAllResources command
    failing_dev = MID_CSPSUBARRAY_LEAF_NODE
    attrs = {"ReleaseAllResources.side_effect": Exception}
    CspsubarraylnMock = mock.Mock(**attrs)
    helper_adapter_factory.get_or_create_adapter(
        failing_dev, AdapterType.SUBARRAY, proxy=CspsubarraylnMock
    )
    component_manager_mid.adapter_factory = helper_adapter_factory
    component_manager_mid.obs_state_model._straight_to_state("IDLE")
    component_manager_mid.initial_obs_state = ObsState.IDLE
    component_manager_mid.is_command_allowed("ReleaseAllResources")

    component_manager_mid.release_all(task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]


def test_subarray_release_resources_fail_is_allowed(
    tango_context, component_manager_mid
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, False, InputParameterMid(None)
    )

    with pytest.raises(CommandNotAllowed):
        callable = component_manager_mid.command_allowed_callable(
            "ReleaseAllResources"
        )
        callable()


def test_subarray_release_resources_fail_not_allowed_with_device_fault(
    tango_context, component_manager_mid
):
    component_manager_mid.op_state_model._op_state = DevState.FAULT
    assert (
        component_manager_mid.is_command_allowed("ReleaseAllResources")
        is False
    )


def test_subarray_release_resources_resources_already_released(
    tango_context, task_callback, component_manager_mid
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    component_manager_mid.obs_state_model._straight_to_state("IDLE")
    component_manager_mid.initial_obs_state = ObsState.IDLE
    helper_adapter_factory = HelperAdapterFactory()
    component_manager_mid.input_parameter.dish_dev_names = []
    component_manager_mid.adapter_factory = helper_adapter_factory

    component_manager_mid.is_command_allowed("ReleaseAllResources")

    component_manager_mid.release_all(task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]


@pytest.mark.parametrize(
    "not_allowed_obs_state",
    ("READY", "FAULT"),
)
def test_mid_subarray_release_all_resources_raises_state_model_exception(
    tango_context, not_allowed_obs_state, component_manager_mid
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    exception_message = "ReleaseAllResources command not permitted"
    component_manager_mid.obs_state_model._straight_to_state(
        not_allowed_obs_state
    )
    with pytest.raises(StateModelError, match=exception_message):
        callable = component_manager_mid.command_allowed_callable(
            "ReleaseAllResources"
        )
        callable()
