import time
from os.path import dirname, join

import mock
import pytest
from ska_control_model.faults import StateModelError
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common.adapters import AdapterType
from ska_tmc_common.op_state_model import TMCOpStateModel
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)

from ska_tmc_subarraynode.commands.scan_command import Scan
from ska_tmc_subarraynode.exceptions import CommandNotAllowed
from ska_tmc_subarraynode.model.input import InputParameterLow
from tests.mock_callable import MockCallable
from tests.settings import (
    SUBARRAY_LEAF_NODES_LOW,
    create_low_cm,
    logger,
    set_sdp_csp_leaf_node_availability_for_aggregation,
    simulate_obs_state_and_result_code_events,
    simulate_obs_state_events,
)
from tests.test_helpers.constants import (
    LOW_CSPSUBARRAY_LEAF_NODE,
    LOW_SDPSUBARRAY_LEAF_NODE,
)


def get_scan_input_str(scan_input_file="Scan_low.json"):
    path = join(dirname(__file__), "..", "..", "..", "data", scan_input_file)
    with open(path, "r") as f:
        scan_input_file = f.read()
    return scan_input_file


def get_scan_command_obj():
    cm, start_time, obs_state_model = create_low_cm(liveliness_probe=False)
    elapsed_time = time.time() - start_time
    logger.info(
        "checked %s devices in %s", len(cm.checked_devices), elapsed_time
    )
    my_adapter_factory = HelperAdapterFactory()
    scan_command = Scan(cm, obs_state_model, my_adapter_factory, logger=logger)
    return scan_command, my_adapter_factory, cm, obs_state_model


@pytest.mark.parametrize(
    "device_name, adapter_type",
    [
        pytest.param(LOW_CSPSUBARRAY_LEAF_NODE, AdapterType.CSPSUBARRAY),
        pytest.param(LOW_SDPSUBARRAY_LEAF_NODE, AdapterType.SDPSUBARRAY),
    ],
)
def test_subarray_scan_fail_on_subarray_ln(
    tango_context,
    task_callback,
    device_name,
    adapter_type,
    component_manager_low,
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )

    helper_adapter_factory = HelperAdapterFactory()
    failing_dev = device_name
    attrs = {"Scan.side_effect": Exception}
    subarrayleafnode_mock = mock.Mock(**attrs)
    helper_adapter_factory.get_or_create_adapter(
        failing_dev, adapter_type, proxy=subarrayleafnode_mock
    )

    simulate_obs_state_events(
        component_manager_low, SUBARRAY_LEAF_NODES_LOW, [ObsState.READY]
    )
    component_manager_low.obs_state_model._straight_to_state("READY")
    component_manager_low.is_command_allowed("Scan")
    component_manager_low.initial_obs_state = ObsState.READY
    component_manager_low.adapter_factory = helper_adapter_factory
    scan_str = get_scan_input_str()
    component_manager_low.scan(argin=scan_str, task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert "Error while invoking Scan on " in result["result"][1]


def test_scan_empty_input_json(
    tango_context, task_callback, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    component_manager_low.is_command_allowed("Scan")
    component_manager_low.adapter_factory = HelperAdapterFactory()
    result_code, _ = component_manager_low.scan(
        " ", task_callback=task_callback
    )
    assert result_code == TaskStatus.REJECTED


def test_scan_fail_check_allowed(tango_context, component_manager_low):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low,
        False,
        InputParameterLow(None),
    )
    with pytest.raises(CommandNotAllowed):
        callable = component_manager_low.command_allowed_callable("Scan")
        callable()


def test_scan_command_with_incorrect_json(task_callback):
    (
        _,
        _,
        cm,
        _,
    ) = get_scan_command_obj()
    set_sdp_csp_leaf_node_availability_for_aggregation(
        cm, True, InputParameterLow(None)
    )
    cm.is_command_allowed("Scan")
    cm.adapter_factory = HelperAdapterFactory()
    scan_str = get_scan_input_str(
        scan_input_file="Scan_low_with_subarray_id_key.json"
    )
    result_code, _ = cm.scan(argin=scan_str, task_callback=task_callback)
    assert result_code == TaskStatus.REJECTED


def test_low_scan_task_completed(
    tango_context, task_callback, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    simulate_obs_state_events(
        component_manager_low, SUBARRAY_LEAF_NODES_LOW, [ObsState.READY]
    )
    component_manager_low.set_scan_duration(10)
    component_manager_low.obs_state_model._straight_to_state("READY")
    component_manager_low.initial_obs_state = ObsState.READY
    component_manager_low.is_command_allowed("Scan")
    scan_input = get_scan_input_str()
    component_manager_low.scan(scan_input, task_callback)

    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )

    simulate_obs_state_and_result_code_events(
        component_manager_low,
        SUBARRAY_LEAF_NODES_LOW,
        [ObsState.SCANNING],
        "Scan",
        ResultCode.OK,
    )
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )


@pytest.mark.parametrize(
    "not_allowed_obs_state",
    ("IDLE", "SCANNING"),
)
def test_low_subarray_scan_raises_state_model_exception(
    tango_context, not_allowed_obs_state, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True
    )
    exception_message = "Scan command not permitted"
    component_manager_low.obs_state_model._straight_to_state(
        not_allowed_obs_state
    )
    with pytest.raises(StateModelError, match=exception_message):
        callable = component_manager_low.command_allowed_callable("Scan")
        callable()
