import json

import mock
import pytest
from ska_control_model.faults import StateModelError
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import AdapterType, DevFactory, FaultType
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)
from tango import DevState

from ska_tmc_subarraynode.exceptions import CommandNotAllowed
from ska_tmc_subarraynode.model.input import InputParameterLow
from tests.settings import (
    SUBARRAY_LEAF_NODES_LOW,
    set_leaf_nodes_obs_state,
    set_sdp_csp_leaf_node_availability_for_aggregation,
    simulate_obs_state_and_result_code_events,
    simulate_obs_state_events,
)
from tests.test_helpers.constants import LOW_CSPSUBARRAY_LEAF_NODE


def test_subarray_low_release_resources(
    tango_context, task_callback, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    simulate_obs_state_events(
        component_manager_low,
        SUBARRAY_LEAF_NODES_LOW,
        [ObsState.IDLE],
    )
    component_manager_low.obs_state_model._straight_to_state("IDLE")
    component_manager_low.initial_obs_state = ObsState.IDLE
    component_manager_low.is_command_allowed("ReleaseAllResources")
    component_manager_low.release_all(task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    simulate_obs_state_and_result_code_events(
        component_manager_low,
        SUBARRAY_LEAF_NODES_LOW,
        [ObsState.EMPTY],
        "ReleaseAllResources",
        ResultCode.OK,
    )
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )


def test_release_resources_fail_subarray(
    tango_context, task_callback, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )

    helper_adapter_factory = HelperAdapterFactory()
    # include exception in ReleaseAllResources command
    failing_dev = LOW_CSPSUBARRAY_LEAF_NODE
    attrs = {"ReleaseAllResources.side_effect": Exception}
    CspsubarraylnMock = mock.Mock(**attrs)
    helper_adapter_factory.get_or_create_adapter(
        failing_dev, AdapterType.SUBARRAY, proxy=CspsubarraylnMock
    )
    component_manager_low.adapter_factory = helper_adapter_factory
    component_manager_low.obs_state_model._straight_to_state("IDLE")
    set_leaf_nodes_obs_state(
        SUBARRAY_LEAF_NODES_LOW,
        ObsState.IDLE,
    )
    component_manager_low.is_command_allowed("ReleaseAllResources")
    component_manager_low.initial_obs_state = ObsState.IDLE
    component_manager_low.release_all(task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]

    assert "Error while invoking ReleaseAllResources" in result["result"][1]


def test_subarray_release_resources_fail_is_allowed(
    tango_context, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low,
        False,
        InputParameterLow(None),
    )

    with pytest.raises(CommandNotAllowed):
        callable = component_manager_low.command_allowed_callable(
            "ReleaseAllResources"
        )
        callable()


def test_subarray_release_resources_fail_not_allowed_with_device_fault(
    tango_context, component_manager_low
):
    component_manager_low.op_state_model._op_state = DevState.FAULT
    assert (
        component_manager_low.is_command_allowed("ReleaseAllResources")
        is False
    )


def test_release_all_resources_timeout(
    tango_context, task_callback, component_manager_low
):
    component_manager_low.command_timeout = 2
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    simulate_obs_state_events(
        component_manager_low,
        SUBARRAY_LEAF_NODES_LOW,
        [ObsState.IDLE],
    )
    component_manager_low.obs_state_model._straight_to_state("IDLE")
    component_manager_low.is_command_allowed("ReleaseAllResources")
    component_manager_low.initial_obs_state = ObsState.IDLE
    defect = {
        "enabled": True,
        "fault_type": FaultType.STUCK_IN_INTERMEDIATE_STATE,
        "error_message": "Command stuck in processing",
        "result": ResultCode.FAILED,
        "intermediate_state": ObsState.RESOURCING,
    }
    csp_sln = DevFactory().get_device(LOW_CSPSUBARRAY_LEAF_NODE)
    csp_sln.SetDefective(json.dumps(defect))
    component_manager_low.release_all(task_callback)

    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert "Timeout has occurred, command failed" in result["result"][1]
    csp_sln.SetDefective(json.dumps({"enabled": False}))


def test_subarray_release_all_resources_error_propagation_command_not_allowed(
    tango_context, task_callback, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    simulate_obs_state_events(
        component_manager_low,
        SUBARRAY_LEAF_NODES_LOW,
        [ObsState.IDLE],
    )
    component_manager_low.obs_state_model._straight_to_state("IDLE")
    component_manager_low.initial_obs_state = ObsState.IDLE
    component_manager_low.is_command_allowed("ReleaseAllResources")
    defect = {
        "enabled": True,
        "fault_type": FaultType.COMMAND_NOT_ALLOWED_BEFORE_QUEUING,
        "error_message": "Command not allowed on leaf node.",
        "result": ResultCode.FAILED,
    }
    csp_sln = DevFactory().get_device(LOW_CSPSUBARRAY_LEAF_NODE)
    csp_sln.SetDefective(json.dumps(defect))
    component_manager_low.release_all(task_callback)

    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert "Command not allowed on leaf node." in result["result"][1]
    csp_sln.SetDefective(json.dumps({"enabled": False}))


@pytest.mark.parametrize(
    "not_allowed_obs_state",
    ("READY", "FAULT"),
)
def test_low_subarray_release_all_resources_raises_state_model_exception(
    tango_context, not_allowed_obs_state, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True
    )
    exception_message = "ReleaseAllResources command not permitted"
    component_manager_low.obs_state_model._straight_to_state(
        not_allowed_obs_state
    )
    with pytest.raises(StateModelError, match=exception_message):
        callable = component_manager_low.command_allowed_callable(
            "ReleaseAllResources"
        )
        callable()
