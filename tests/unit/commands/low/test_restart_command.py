import mock
import pytest
from ska_control_model.faults import StateModelError
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common.adapters import AdapterType
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)
from tango import DevState

from ska_tmc_subarraynode.model.input import InputParameterLow
from tests.settings import (
    SUBARRAY_LEAF_NODES_LOW,
    set_sdp_csp_leaf_node_availability_for_aggregation,
    simulate_obs_state_and_result_code_events,
    simulate_obs_state_events,
)
from tests.test_helpers.constants import (
    LOW_CSPSUBARRAY_LEAF_NODE,
    LOW_SDPSUBARRAY_LEAF_NODE,
)


def test_low_restart_command(
    tango_context, task_callback, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    simulate_obs_state_events(
        component_manager_low, SUBARRAY_LEAF_NODES_LOW, [ObsState.ABORTED]
    )
    component_manager_low.obs_state_model._straight_to_state("ABORTED")
    component_manager_low.initial_obs_state = ObsState.ABORTED
    component_manager_low.restart(task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    simulate_obs_state_and_result_code_events(
        component_manager_low,
        SUBARRAY_LEAF_NODES_LOW,
        [ObsState.EMPTY],
        "Restart",
        ResultCode.OK,
    )
    task_callback.assert_against_call(
        call_kwargs={
            "status": TaskStatus.COMPLETED,
            "result": (ResultCode.OK, "Command Completed"),
        }
    )


@pytest.mark.parametrize(
    "device_name, adapter_type",
    [
        pytest.param(LOW_CSPSUBARRAY_LEAF_NODE, AdapterType.CSPSUBARRAY),
        pytest.param(LOW_SDPSUBARRAY_LEAF_NODE, AdapterType.SDPSUBARRAY),
    ],
)
def test_subarray_restart_fail_on_subarray_ln(
    tango_context,
    task_callback,
    device_name,
    adapter_type,
    component_manager_low,
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    helper_adapter_factory = HelperAdapterFactory()
    failing_dev = device_name
    attrs = {"Restart.side_effect": Exception}
    subarrayleafnode_mock = mock.Mock(**attrs)
    helper_adapter_factory.get_or_create_adapter(
        failing_dev, adapter_type, proxy=subarrayleafnode_mock
    )
    simulate_obs_state_events(
        component_manager_low, SUBARRAY_LEAF_NODES_LOW, [ObsState.ABORTED]
    )
    component_manager_low.obs_state_model._straight_to_state("ABORTED")
    component_manager_low.initial_obs_state = ObsState.ABORTED

    component_manager_low.is_command_allowed("Restart")
    component_manager_low.adapter_factory = helper_adapter_factory
    component_manager_low.restart(task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert "Error while invoking Restart command" in result["result"][1]


def test_low_subarray_restart_fail_is_command_allowed(
    tango_context, component_manager_low
):
    component_manager_low.op_state_model._op_state = DevState.FAULT
    assert component_manager_low.is_command_allowed("Restart") is False


@pytest.mark.parametrize(
    "not_allowed_obs_state",
    ("ABORTING", "EMPTY"),
)
def test_low_subarray_restart_raises_state_model_exception(
    tango_context, not_allowed_obs_state, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True
    )
    exception_message = "Restart command not permitted"
    component_manager_low.obs_state_model._straight_to_state(
        not_allowed_obs_state
    )
    with pytest.raises(StateModelError, match=exception_message):
        callable = component_manager_low.command_allowed_callable("Restart")
        callable()
