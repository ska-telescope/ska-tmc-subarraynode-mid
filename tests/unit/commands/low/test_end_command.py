import mock
import pytest
from ska_control_model.faults import StateModelError
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common.adapters import AdapterType
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)
from tango import DevState

from ska_tmc_subarraynode.exceptions import CommandNotAllowed
from ska_tmc_subarraynode.model.input import InputParameterLow
from tests.settings import (
    SUBARRAY_LEAF_NODES_LOW,
    set_sdp_csp_leaf_node_availability_for_aggregation,
    simulate_obs_state_and_result_code_events,
    simulate_obs_state_events,
)
from tests.test_helpers.constants import (
    LOW_CSPSUBARRAY_LEAF_NODE,
    LOW_SDPSUBARRAY_LEAF_NODE,
)


def test_low_subarray_end_command_completed(
    tango_context, task_callback, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    component_manager_low.is_command_allowed("End")
    simulate_obs_state_events(
        component_manager_low,
        SUBARRAY_LEAF_NODES_LOW,
        [ObsState.READY],
    )
    component_manager_low.obs_state_model._straight_to_state("READY")
    component_manager_low.initial_obs_state = ObsState.READY
    component_manager_low.end(task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    simulate_obs_state_and_result_code_events(
        component_manager_low,
        SUBARRAY_LEAF_NODES_LOW,
        [ObsState.IDLE],
        "End",
        ResultCode.OK,
    )
    task_callback.assert_against_call(
        call_kwargs={
            "status": TaskStatus.COMPLETED,
            "result": (ResultCode.OK, "Command Completed"),
        }
    )


def test_low_subarray_end_fail_low_csp_subarray(
    tango_context, task_callback, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    helper_adapter_factory = HelperAdapterFactory()
    # include exception in End command
    failing_dev = LOW_CSPSUBARRAY_LEAF_NODE
    attrs = {"End.side_effect": Exception}
    CspsubarraylnMock = mock.Mock(**attrs)
    helper_adapter_factory.get_or_create_adapter(
        failing_dev, AdapterType.SUBARRAY, proxy=CspsubarraylnMock
    )

    component_manager_low.is_command_allowed("End")
    simulate_obs_state_events(
        component_manager_low,
        SUBARRAY_LEAF_NODES_LOW,
        [ObsState.READY],
    )
    component_manager_low.obs_state_model._straight_to_state("READY")
    component_manager_low.initial_obs_state = ObsState.READY

    component_manager_low.adapter_factory = helper_adapter_factory
    component_manager_low.end(task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert (
        "Error while invoking End on CSP Subarray Leaf Node: "
        in result["result"][1]
    )


def test_low_subarray_end_fail_low_sdp_subarray(
    tango_context, task_callback, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    helper_adapter_factory = HelperAdapterFactory()
    # include exception in End command
    failing_dev = LOW_SDPSUBARRAY_LEAF_NODE
    attrs = {"End.side_effect": Exception}
    sdpsubarraylnMock = mock.Mock(**attrs)
    helper_adapter_factory.get_or_create_adapter(
        failing_dev, AdapterType.SUBARRAY, proxy=sdpsubarraylnMock
    )

    component_manager_low.is_command_allowed("End")
    simulate_obs_state_events(
        component_manager_low,
        SUBARRAY_LEAF_NODES_LOW,
        [ObsState.READY],
    )
    component_manager_low.obs_state_model._straight_to_state("READY")

    component_manager_low.adapter_factory = helper_adapter_factory
    component_manager_low.end(task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)

    assert ResultCode.FAILED == result["result"][0]
    assert (
        "Error while invoking End on SDP Subarray Leaf Node "
        in result["result"][1]
    )


def test_end_command_fail_check_allowed_with_device_unresponsive(
    tango_context, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low,
        False,
        InputParameterLow(None),
    )

    with pytest.raises(CommandNotAllowed):
        callable = component_manager_low.command_allowed_callable("End")
        callable()


def test_low_subarray_end_fail_not_allowed(
    tango_context, component_manager_low
):
    component_manager_low.op_state_model._op_state = DevState.FAULT
    assert component_manager_low.is_command_allowed("End") is False


@pytest.mark.parametrize(
    "not_allowed_obs_state",
    ("EMPTY", "ABORTED"),
)
def test_low_subarray_end_raises_state_model_exception(
    tango_context, not_allowed_obs_state, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True
    )
    exception_message = "End command not permitted"
    component_manager_low.obs_state_model._straight_to_state(
        not_allowed_obs_state
    )
    with pytest.raises(StateModelError, match=exception_message):
        callable = component_manager_low.command_allowed_callable("End")
        callable()
