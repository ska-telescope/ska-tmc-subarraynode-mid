import json
import time
from os.path import dirname, join
from typing import Any, List

import mock
import pytest
from ska_control_model.faults import StateModelError
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_telmodel.schema import validate
from ska_tmc_common import AdapterType, DevFactory
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)
from tango import DevState, EventType

from ska_tmc_subarraynode.commands.assign_resources_command import (
    AssignResources,
)
from ska_tmc_subarraynode.exceptions import CommandNotAllowed
from ska_tmc_subarraynode.model.input import InputParameterLow
from tests.settings import (
    SUBARRAY_LEAF_NODES_LOW,
    logger,
    set_sdp_csp_leaf_node_availability_for_aggregation,
    simulate_obs_state_and_result_code_events,
)
from tests.test_helpers.constants import (
    LOW_CSPSUBARRAY_LEAF_NODE,
    LOW_SDPSUBARRAY_LEAF_NODE,
)

DEVICE_LIST = [LOW_CSPSUBARRAY_LEAF_NODE, LOW_SDPSUBARRAY_LEAF_NODE]


def get_assign_input_str(
    assign_input_file="AssignResources_low.json",
):
    path = join(dirname(__file__), "..", "..", "..", "data", assign_input_file)
    with open(path, "r") as f:
        assign_input_str = f.read()
    return assign_input_str


def test_subarray_low_assign_resources(
    tango_context, component_manager_low, task_callback
):
    if not wait_for_initialization_events(SUBARRAY_LEAF_NODES_LOW):
        assert False, "Initialization of Devices is not complete"

    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    assign_input = get_assign_input_str()
    component_manager_low.set_subarray_id(1)
    component_manager_low.is_command_allowed("AssignResources")
    component_manager_low.initial_obs_state = ObsState.EMPTY
    component_manager_low.assign(assign_input, task_callback)

    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )

    simulate_obs_state_and_result_code_events(
        component_manager_low,
        SUBARRAY_LEAF_NODES_LOW,
        [ObsState.IDLE],
        "AssignResources",
        ResultCode.OK,
    )
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )


def test_subarray_assign_resources_fail_is_allowed(
    tango_context, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low,
        False,
        InputParameterLow(None),
    )
    with pytest.raises(CommandNotAllowed):
        callable = component_manager_low.command_allowed_callable(
            "AssignResources"
        )
        callable()


def test_subarray_assign_resources_fail_csp_subarray_ln(
    tango_context, task_callback, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )

    component_manager_low.initial_obs_state = ObsState.EMPTY
    component_manager_low.is_command_allowed("AssignResources")
    helper_adapter_factory = HelperAdapterFactory()

    # include exception in AssignResources command
    failing_dev = LOW_CSPSUBARRAY_LEAF_NODE
    attrs = {"AssignResources.side_effect": Exception}
    CspsubarraylnMock = mock.Mock(**attrs)
    helper_adapter_factory.get_or_create_adapter(
        failing_dev, AdapterType.CSPSUBARRAY, proxy=CspsubarraylnMock
    )
    component_manager_low.adapter_factory = helper_adapter_factory

    assign_str = get_assign_input_str()
    component_manager_low.assign(assign_str, task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert "Error while invoking AssignResources" in result["result"][1]


def test_subarray_assign_resources_empty_input_json(
    tango_context, task_callback, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    component_manager_low.initial_obs_state = ObsState.EMPTY
    component_manager_low.is_command_allowed("AssignResources")
    logger.info("is allowed successful")

    component_manager_low.adapter_factory = HelperAdapterFactory()
    component_manager_low.assign(" ", task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert "Error in parsing the JSON string" in result["result"][1]


def test_subarray_assign_resources_command_not_allowed(
    tango_context, component_manager_low
):
    component_manager_low.op_state_model._op_state = DevState.FAULT
    assert component_manager_low.is_command_allowed("AssignResources") is False


@pytest.mark.parametrize("missing_key", ("eb_id", "sdp"))
def test_subarray_assign_resources_json_with_missing_key(
    tango_context, task_callback, missing_key, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )

    component_manager_low.initial_obs_state = ObsState.EMPTY
    component_manager_low.is_command_allowed("AssignResources")
    logger.info("is allowed successful")

    component_manager_low.adapter_factory = HelperAdapterFactory()
    assign_str = get_assign_input_str()
    json_argument = json.loads(assign_str)

    if missing_key == "eb_id":
        del json_argument["sdp"]["execution_block"]["eb_id"]
    elif missing_key == "sdp":
        del json_argument["sdp"]

    component_manager_low.assign(json_argument, task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert "Error in parsing the JSON string" in result["result"][1]


@pytest.mark.parametrize(
    "not_allowed_obs_state",
    ("ABORTED", "FAULT"),
)
def test_low_subarray_assign_resources_raises_state_model_exception(
    tango_context, not_allowed_obs_state, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True
    )
    exception_message = "AssignResources command not permitted"
    component_manager_low.obs_state_model._straight_to_state(
        not_allowed_obs_state
    )
    with pytest.raises(StateModelError, match=exception_message):
        callable = component_manager_low.command_allowed_callable(
            "AssignResources"
        )
        callable()


def test_csp_input_json(tango_context, component_manager_low):
    helper_adapter_factory = HelperAdapterFactory()
    assign_command = AssignResources(
        component_manager_low,
        component_manager_low.obs_state_model,
        logger,
        helper_adapter_factory,
    )
    assign_str = get_assign_input_str()
    component_manager_low.set_subarray_id(1)
    csp_argin = assign_command.get_csp_resources_low(json.loads(assign_str))
    validate(
        version=component_manager_low.csp_assign_interface,
        config=csp_argin,
        strictness=2,
    )


def wait_for_initialization_events(devices: List[str]) -> bool:
    """
    Checks if  ObsState.EMPTY event is received from all devices within a
    specified timeout period.

    Args:
        devices (list): A list of device names to check.

    Returns:
        bool: True if events are received from 3 devices, False otherwise.
    """
    event_count = 0
    devices_with_successful_events = set()
    device_proxys_to_event_ids = {}

    def event_callback(event):
        nonlocal event_count
        if event.err:
            logger.info(f"Error: {event.errors}")
        else:
            device_name = event.device.dev_name()
            if device_name not in devices_with_successful_events:
                devices_with_successful_events.add(device_name)
                event_count += 1
                logger.info(
                    f"Event received from {device_name}: {event.attr_value.value}"
                )
                logger.info(
                    f"Total devices received events from : {event_count}"
                )

    device_factory: DevFactory = DevFactory()

    for device in devices:
        device_proxy = device_factory.get_device(device)

        if "csp" in device:
            attribute_name = "cspSubarrayObsState"
        elif "sdp" in device:
            attribute_name = "sdpSubarrayObsState"
        elif "mccs" in device:
            attribute_name = "obsState"
        else:
            continue

        event_id = device_proxy.subscribe_event(
            attribute_name, EventType.CHANGE_EVENT, event_callback
        )

        device_proxys_to_event_ids[device_proxy] = event_id

    TIMEOUT = 30
    start_time = time.time()

    while True:
        elapsed_time = time.time() - start_time
        time.sleep(0.1)
        if elapsed_time > TIMEOUT:
            # Unsubscribe from all events if timeout occurs
            for device_proxy, event_id in device_proxys_to_event_ids.items():
                device_proxy.unsubscribe_event(event_id)
            return False
        if event_count >= 3:
            logger.info("Event count has reached 3!")
            for device_proxy, event_id in device_proxys_to_event_ids.items():
                device_proxy.unsubscribe_event(event_id)
            return True


def test_subarray_low_assign_resources_without_pss_pst_keys(
    tango_context, component_manager_low, task_callback
):
    if not wait_for_initialization_events(SUBARRAY_LEAF_NODES_LOW):
        assert False, "Initialization of Devices is not complete"

    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    assign_input = get_assign_input_str(
        assign_input_file="AssignResources_without_csp_key.json"
    )

    component_manager_low.set_subarray_id(1)
    component_manager_low.is_command_allowed("AssignResources")
    component_manager_low.initial_obs_state = ObsState.EMPTY
    component_manager_low.assign(assign_input, task_callback)

    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )

    simulate_obs_state_and_result_code_events(
        component_manager_low,
        SUBARRAY_LEAF_NODES_LOW,
        [ObsState.IDLE],
        "AssignResources",
        ResultCode.OK,
    )
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )
