import logging
import time
from functools import partial

import pytest
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState

from tests.settings import set_sdp_csp_leaf_node_availability_for_aggregation


def component_state_change_callback(cm, state_change):
    logging.info(f"{state_change}")
    if "resourced" in state_change.keys():
        logging.info(f"{cm.obs_state_model.obs_state}")
        if state_change.get("resourced", True):
            # If Initial obsState is EMPTY, then only perform action
            # "component_resourced" is needed
            # to transition SubarrayNode obsState from
            # RESOURCING_EMPTY to RESOURCING_IDLE.
            if initial_obs_state == ObsState.EMPTY:
                cm.obs_state_model.perform_action("component_resourced")
        else:
            # If Initial obsState is IDLE, then only perform
            # action "component_unresourced" is needed
            # to transition SubarrayNode obsState from
            # RESOURCING_IDLE to RESOURCING_EMPTY.
            if initial_obs_state in (ObsState.IDLE, ObsState.EMPTY):
                cm.obs_state_model.perform_action("component_unresourced")

    if "configured" in state_change.keys():
        if state_change.get("configured", True):
            # If Initial obsState is IDLE, then only perform
            # action "component_configured" is needed
            # to transition SubarrayNode obsState from
            # RESOURCING_IDLE to RESOURCING_READY.
            if initial_obs_state == ObsState.IDLE:
                cm.obs_state_model.perform_action("component_configured")
        else:
            # If Initial obsState is READY, then only perform
            # action "component_unconfigured" is needed.
            if initial_obs_state == ObsState.READY:
                cm.obs_state_model.perform_action("component_unconfigured")
    if "scanning" in state_change.keys():
        if state_change.get("scanning", True):
            # If Initial obsState is READY, then only perform
            # action "component_configured" is needed
            if initial_obs_state == ObsState.READY:
                cm.obs_state_model.perform_action("component_scanning")
        else:
            # If Initial obsState is READY, then only perform
            # action "component_unconfigured" is needed.
            cm.obs_state_model.perform_action("component_not_scanning")

    if "assign_completed" in state_change.keys():
        cm.obs_state_model.perform_action("assign_completed")
    if "release_completed" in state_change.keys():
        cm.obs_state_model.perform_action("release_completed")
    if "configure_completed" in state_change.keys():
        cm.obs_state_model.perform_action("configure_completed")
    if "abort_completed" in state_change.keys():
        cm.obs_state_model.perform_action("abort_completed")
    if "restart_completed" in state_change.keys():
        cm.obs_state_model.perform_action("restart_completed")


@pytest.mark.parametrize(
    "initial_obsstate,interim_obs_state,obs_state,aggregated_obs_state,\
    command_in_progress,result_code",
    [
        (
            ObsState.EMPTY,
            "RESOURCING_IDLE",
            ObsState.RESOURCING,
            ObsState.EMPTY,
            "",
            ResultCode.OK,
        ),
        (
            ObsState.EMPTY,
            "RESOURCING_IDLE",
            ObsState.RESOURCING,
            ObsState.IDLE,
            "AssignResources",
            ResultCode.OK,
        ),
        (
            ObsState.IDLE,
            "CONFIGURING_IDLE",
            ObsState.CONFIGURING,
            ObsState.READY,
            "Configure",
            ResultCode.OK,
        ),
        (
            ObsState.READY,
            "READY",
            ObsState.READY,
            ObsState.IDLE,
            "End",
            ResultCode.OK,
        ),
        (
            ObsState.IDLE,
            "RESOURCING_EMPTY",
            ObsState.RESOURCING,
            ObsState.EMPTY,
            "ReleaseAllResources",
            ResultCode.OK,
        ),
        (
            ObsState.SCANNING,
            "SCANNING",
            ObsState.SCANNING,
            ObsState.READY,
            "EndScan",
            ResultCode.OK,
        ),
        (
            ObsState.RESTARTING,
            "RESTARTING",
            ObsState.RESTARTING,
            ObsState.EMPTY,
            "Restart",
            ResultCode.OK,
        ),
        (
            ObsState.ABORTING,
            "ABORTING",
            ObsState.ABORTING,
            ObsState.ABORTED,
            "Abort",
            ResultCode.OK,
        ),
    ],
)
def test_check_subarray_availability_attribute_initial_events(
    tango_context,
    component_manager_mid,
    initial_obsstate,
    interim_obs_state,
    obs_state,
    aggregated_obs_state,
    command_in_progress,
    result_code,
):
    global initial_obs_state
    initial_obs_state = initial_obsstate
    logging.info("Setting availablity")
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True
    )
    logging.info("Setting availablity done")
    component_manager_mid.component_state_changed_callback = partial(
        component_state_change_callback, component_manager_mid
    )
    component_manager_mid.obs_state_model._straight_to_state(interim_obs_state)
    assert component_manager_mid.obs_state_model.obs_state == obs_state
    component_manager_mid._set_subarraynode_obsstate(
        aggregated_obs_state, command_in_progress
    )
    assert (
        component_manager_mid.obs_state_model.obs_state == aggregated_obs_state
    )
