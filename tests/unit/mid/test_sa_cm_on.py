import pytest
from ska_tango_base.executor import TaskStatus


def test_cm_on(tango_context, task_callback, component_manager_mid):
    res_code, message = component_manager_mid.on(task_callback)
    assert res_code == TaskStatus.QUEUED
    assert message == "Task queued"
