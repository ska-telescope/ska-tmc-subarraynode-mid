import time

import pytest
from ska_tango_base.control_model import AdminMode, HealthState
from ska_tmc_common.dev_factory import DevFactory

from ska_tmc_subarraynode.exceptions import CommandNotAllowed
from tests.settings import (
    TIMEOUT,
    count_faulty_devices,
    create_cm_no_faulty_devices,
)
from tests.test_helpers.constants import (
    DISH_MASTER_DEVICE,
    LOW_CSPSUBARRAY_LEAF_NODE,
    LOW_SDPSUBARRAY_LEAF_NODE,
    MID_CSPSUBARRAY_LEAF_NODE,
    MID_CSPSUBARRAY_NODE_NAME,
    MID_SDPSUBARRAY_LEAF_NODE,
)


def test_set_health_state_ok_events(tango_context, component_manager_mid):
    num_faulty = count_faulty_devices(component_manager_mid)
    assert num_faulty == 0
    start_time = time.time()
    elapsed_time = 0
    # need to wait for the first event to come just after the subscription
    while (
        component_manager_mid.component.subarray_health_state != HealthState.OK
    ):
        elapsed_time = time.time() - start_time
        time.sleep(0.1)
        if elapsed_time > TIMEOUT:
            pytest.fail("Timeout occurred while executing the test")
    assert (
        component_manager_mid.component.subarray_health_state == HealthState.OK
    )


def set_device_degraded(devFactory, cm, expected_elapsed_time):
    proxy = devFactory.get_device("mid-csp/subarray/01")
    proxy.SetDirectHealthState(HealthState.DEGRADED)
    assert proxy.HealthState == HealthState.DEGRADED
    start_time = time.time()
    elapsed_time = 0
    while cm.component.subarray_health_state != HealthState.DEGRADED:
        elapsed_time = time.time() - start_time
        time.sleep(0.1)
        if elapsed_time > TIMEOUT:
            pytest.fail("Timeout occurred while executing the test")
    assert elapsed_time < expected_elapsed_time


def set_admin_mode_offline(device_name, cm, expected_elapsed_time):
    devFactory = DevFactory()
    proxy = devFactory.get_device(device_name)
    # Set admin mode ONLINE first
    if "csp" in device_name:
        proxy.SetCspSubarrayLeafNodeAdminMode(AdminMode.ONLINE)
        assert proxy.cspSubarrayAdminMode == AdminMode.ONLINE
        proxy.SetCspSubarrayLeafNodeAdminMode(AdminMode.OFFLINE)
    elif "sdp" in device_name:
        proxy.SetSdpSubarrayLeafNodeAdminMode(AdminMode.ONLINE)
        assert proxy.sdpSubarrayAdminMode == AdminMode.ONLINE
        proxy.SetSdpSubarrayLeafNodeAdminMode(AdminMode.OFFLINE)
        assert proxy.sdpSubarrayAdminMode == AdminMode.OFFLINE
    start_time = time.time()
    elapsed_time = 0
    while cm.component.subarray_health_state != HealthState.DEGRADED:
        elapsed_time = time.time() - start_time
        time.sleep(0.1)
        if elapsed_time > TIMEOUT:
            pytest.fail("Timeout occurred while executing the test")
    assert elapsed_time < expected_elapsed_time


def test_set_health_state_degraded_events(
    tango_context, component_manager_mid
):
    devFactory = DevFactory()
    num_faulty = count_faulty_devices(component_manager_mid)
    assert num_faulty == 0
    set_device_degraded(devFactory, component_manager_mid, 100)
    assert (
        component_manager_mid.component.subarray_health_state
        == HealthState.DEGRADED
    )


@pytest.mark.parametrize(
    "device_name",
    [
        MID_CSPSUBARRAY_LEAF_NODE,
        MID_SDPSUBARRAY_LEAF_NODE,
        LOW_CSPSUBARRAY_LEAF_NODE,
        LOW_SDPSUBARRAY_LEAF_NODE,
    ],
)
def test_admin_mode_health_state_events(
    tango_context, component_manager_mid, component_manager_low, device_name
):
    num_faulty = count_faulty_devices(component_manager_mid)
    assert num_faulty == 0
    if "low" in device_name:
        component_manager_low.is_admin_mode_enabled = True
        set_admin_mode_offline(device_name, component_manager_low, 100)
        assert (
            component_manager_low.component.subarray_health_state
            == HealthState.DEGRADED
        )
    else:
        component_manager_mid.is_admin_mode_enabled = True
        set_admin_mode_offline(device_name, component_manager_mid, 100)
        assert (
            component_manager_mid.component.subarray_health_state
            == HealthState.DEGRADED
        )


def test_command_not_allowed_for_degraded_health_state(
    tango_context, component_manager_low
):
    num_faulty = count_faulty_devices(component_manager_low)
    assert num_faulty == 0
    component_manager_low.is_admin_mode_enabled = True
    set_admin_mode_offline(
        LOW_CSPSUBARRAY_LEAF_NODE, component_manager_low, 100
    )
    assert (
        component_manager_low.component.subarray_health_state
        == HealthState.DEGRADED
    )
    with pytest.raises(CommandNotAllowed):
        callable = component_manager_low.is_command_allowed("AssignResources")
        callable()


def set_failed(devFactory, cm, expected_elapsed_time):
    proxy = devFactory.get_device(MID_CSPSUBARRAY_NODE_NAME)
    proxy.SetDirectHealthState(HealthState.DEGRADED)
    assert proxy.HealthState == HealthState.DEGRADED
    proxy.SetDirectHealthState(HealthState.FAILED)
    assert proxy.HealthState == HealthState.FAILED
    start_time = time.time()
    elapsed_time = 0
    while cm.component.subarray_health_state != HealthState.FAILED:
        elapsed_time = time.time() - start_time
        time.sleep(0.1)
        if elapsed_time > TIMEOUT:
            pytest.fail("Timeout occurred while executing the test")
    assert elapsed_time < expected_elapsed_time


def test_set_health_state_failed_events(tango_context, component_manager_mid):
    devFactory = DevFactory()
    num_faulty = count_faulty_devices(component_manager_mid)
    assert num_faulty == 0
    set_failed(devFactory, component_manager_mid, expected_elapsed_time=100)
    assert (
        component_manager_mid.component.subarray_health_state
        == HealthState.FAILED
    )


def test_set_all_dish_failed(tango_context, component_manager_mid):
    devFactory = DevFactory()
    proxy = devFactory.get_device(DISH_MASTER_DEVICE)
    proxy.SetDirectHealthState(HealthState.FAILED)
    assert proxy.HealthState == HealthState.FAILED
    num_faulty = count_faulty_devices(component_manager_mid)
    assert num_faulty == 0
    start_time = time.time()
    elapsed_time = 0
    while (
        component_manager_mid.component.subarray_health_state
        != HealthState.FAILED
    ):
        elapsed_time = time.time() - start_time
        time.sleep(0.1)
        if elapsed_time > TIMEOUT:
            pytest.fail("Timeout occurred while executing the test")
    assert (
        component_manager_mid.component.subarray_health_state
        == HealthState.FAILED
    )


def set_device_unknown(devFactory, cm, expected_elapsed_time):
    proxy = devFactory.get_device(MID_CSPSUBARRAY_NODE_NAME)
    proxy.SetDirectHealthState(HealthState.UNKNOWN)
    assert proxy.HealthState == HealthState.UNKNOWN
    start_time = time.time()
    elapsed_time = 0
    while cm.component.subarray_health_state != HealthState.UNKNOWN:
        elapsed_time = time.time() - start_time
        time.sleep(0.1)
        if elapsed_time > TIMEOUT:
            pytest.fail("Timeout occurred while executing the test")
    assert elapsed_time < expected_elapsed_time


def test_set_health_state_unknown_events(tango_context, component_manager_mid):
    devFactory = DevFactory()
    num_faulty = count_faulty_devices(component_manager_mid)
    assert num_faulty == 0
    set_device_unknown(devFactory, component_manager_mid, 15)
    assert (
        component_manager_mid.component.subarray_health_state
        == HealthState.UNKNOWN
    )
