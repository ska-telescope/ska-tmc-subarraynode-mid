import time

import pytest
from ska_tmc_common import HelperDishDevice, HelperSubarrayLeafDevice

from tests.settings import (
    TIMEOUT,
    count_faulty_devices,
    create_mid_cm,
    logger,
    set_devices_unresponsive,
)
from tests.test_helpers.constants import (
    DISH_LEAF_NODE,
    DISH_MASTER_DEVICE1,
    MID_CSPSUBARRAY_LEAF_NODE,
    MID_CSPSUBARRAY_NODE_NAME,
    MID_SDPSUBARRAY_LEAF_NODE,
    MID_SDPSUBARRAY_NODE_NAME,
)

DEVICE_LIST_MID = [
    MID_CSPSUBARRAY_LEAF_NODE,
    MID_SDPSUBARRAY_LEAF_NODE,
    DISH_LEAF_NODE,
    MID_CSPSUBARRAY_NODE_NAME,
    MID_SDPSUBARRAY_NODE_NAME,
]


@pytest.fixture()
def devices_to_load():
    return (
        {
            "class": HelperSubarrayLeafDevice,
            "devices": [
                {"name": MID_CSPSUBARRAY_LEAF_NODE},
            ],
        },
        {
            "class": HelperDishDevice,
            "devices": [
                {"name": DISH_MASTER_DEVICE1},
            ],
        },
    )


@pytest.mark.skip(
    reason="Liveliness probe implementation removed for unit tests"
)
def test_one_working_other_faulty(tango_context, component_manager_mid):
    logger.info("%s", tango_context)
    for dev in DEVICE_LIST_MID:
        component_manager_mid.get_device(dev)
    set_devices_unresponsive(component_manager_mid, DEVICE_LIST_MID)
    assert len(DEVICE_LIST_MID) - 1 != count_faulty_devices(
        component_manager_mid
    )
