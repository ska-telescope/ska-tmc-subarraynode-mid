import pytest

from ska_tmc_subarraynode.model.input import InputParameterMid

input = InputParameterMid(None)


@pytest.fixture(
    scope="function",
    params=[
        (input.tmc_csp_sln_device_name, "1"),
        (input.tmc_sdp_sln_device_name, "2"),
        (input.csp_subarray_dev_name, "3"),
        (input.sdp_subarray_dev_name, "4"),
        (input.tmc_dish_ln_device_names, ("5", "6")),
        (input.dish_dev_names, "7"),
    ],
)
def property_argument(request):
    device_name, value = request.param
    return device_name, value


def test_properties(property_argument):
    device_name, value = property_argument
    device_name = value
    assert device_name == value
