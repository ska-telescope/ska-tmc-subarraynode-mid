import time

import pytest
from ska_tmc_common.test_helpers.helper_dish_device import HelperDishDevice
from ska_tmc_common.test_helpers.helper_subarray_device import (
    HelperSubArrayDevice,
)
from ska_tmc_common.test_helpers.helper_subarray_leaf_device import (
    HelperSubarrayLeafDevice,
)

from tests.settings import (
    DEVICE_LIST_MID,
    TIMEOUT,
    count_faulty_devices,
    logger,
    set_devices_unresponsive,
)
from tests.test_helpers.constants import (
    DISH_LEAF_NODE,
    DISH_MASTER_DEVICE1,
    MID_CSPSUBARRAY_NODE_NAME,
    MID_SDPSUBARRAY_LEAF_NODE,
)

WORKING_DEVICES = 4


@pytest.fixture()
def devices_to_load():
    return (
        {
            "class": HelperSubarrayLeafDevice,
            "devices": [
                {"name": MID_SDPSUBARRAY_LEAF_NODE},
            ],
        },
        {
            "class": HelperSubArrayDevice,
            "devices": [
                {"name": MID_CSPSUBARRAY_NODE_NAME},
            ],
        },
        {
            "class": HelperDishDevice,
            "devices": [
                {"name": DISH_LEAF_NODE},
                {"name": DISH_MASTER_DEVICE1},
            ],
        },
    )


@pytest.mark.skip(
    reason="Liveliness probe implementation removed for unit tests"
)
def test_some_working_other_faulty(tango_context, component_manager_mid):
    logger.info("%s", tango_context)
    for dev in DEVICE_LIST_MID:
        component_manager_mid.get_device(dev)
    set_devices_unresponsive(component_manager_mid, DEVICE_LIST_MID)
    assert len(DEVICE_LIST_MID) == count_faulty_devices(component_manager_mid)
