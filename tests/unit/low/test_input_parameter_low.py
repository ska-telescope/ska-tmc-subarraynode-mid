import pytest

from ska_tmc_subarraynode.model.input import InputParameterLow
from tests.test_helpers.constants import (
    LOW_CSPSUBARRAY_LEAF_NODE,
    LOW_CSPSUBARRAY_NODE_NAME,
    LOW_SDPSUBARRAY_LEAF_NODE,
    LOW_SDPSUBARRAY_NODE_NAME,
    MCCS_SUBARRAY_LEAF_NODE,
    MCCSSUBARRAY_NODE_NAME,
)

input = InputParameterLow(None)


@pytest.fixture(
    scope="function",
    params=[
        (input.tmc_csp_sln_device_name, LOW_CSPSUBARRAY_LEAF_NODE),
        (input.tmc_sdp_sln_device_name, LOW_SDPSUBARRAY_LEAF_NODE),
        (input.csp_subarray_dev_name, LOW_CSPSUBARRAY_NODE_NAME),
        (input.sdp_subarray_dev_name, LOW_SDPSUBARRAY_NODE_NAME),
        (
            input.tmc_mccs_sln_device_name,
            MCCS_SUBARRAY_LEAF_NODE,
        ),
        (input.mccs_subarray_dev_name, MCCSSUBARRAY_NODE_NAME),
    ],
)
def property_argument(request):
    device_name, value = request.param
    return device_name, value


def test_properties(property_argument):
    device_name, value = property_argument
    device_name = value
    assert device_name == value
