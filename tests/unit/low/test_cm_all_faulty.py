import time

import pytest

from tests.settings import (
    DEVICE_LIST_LOW,
    TIMEOUT,
    count_faulty_devices,
    create_low_cm,
    set_devices_unresponsive,
)


@pytest.mark.skip("Liveliness probe implementation removed for unit tests")
def test_all_devices_faulty(tango_context, component_manager_low):
    print("tango_context %s", tango_context)
    for dev in DEVICE_LIST_LOW:
        component_manager_low.get_device(dev)
    set_devices_unresponsive(component_manager_low, DEVICE_LIST_LOW)
    total_faulty_device = count_faulty_devices(component_manager_low)
    assert len(DEVICE_LIST_LOW) == total_faulty_device
