import time

import pytest
from ska_tango_base.control_model import HealthState
from ska_tmc_common.dev_factory import DevFactory

from ska_tmc_subarraynode.model.input import InputParameterLow
from tests.settings import (
    TIMEOUT,
    count_faulty_devices,
    create_cm_no_faulty_devices,
)


def test_set_health_state_ok_events(tango_context, component_manager_low):
    num_faulty = count_faulty_devices(component_manager_low)
    assert num_faulty == 0
    start_time = time.time()
    elapsed_time = 0
    # need to wait for the first event to come just after the subscription
    while (
        component_manager_low.component.subarray_health_state != HealthState.OK
    ):
        elapsed_time = time.time() - start_time
        time.sleep(0.1)
        if elapsed_time > TIMEOUT:
            pytest.fail("Timeout occurred while executing the test")
    assert (
        component_manager_low.component.subarray_health_state == HealthState.OK
    )


def set_device_degraded(devFactory, cm, expected_elapsed_time):
    proxy = devFactory.get_device("low-sdp/subarray/01")

    proxy.SetDirectHealthState(HealthState.DEGRADED)
    assert proxy.HealthState == HealthState.DEGRADED
    start_time = time.time()
    elapsed_time = 0
    while cm.component.subarray_health_state != HealthState.DEGRADED:
        elapsed_time = time.time() - start_time
        time.sleep(0.1)
        if elapsed_time > TIMEOUT:
            pytest.fail("Timeout occurred while executing the test")
    assert elapsed_time < expected_elapsed_time


def test_set_health_state_degraded_events(
    tango_context, component_manager_low
):
    devFactory = DevFactory()
    num_faulty = count_faulty_devices(component_manager_low)
    assert num_faulty == 0
    set_device_degraded(devFactory, component_manager_low, 100)
    assert (
        component_manager_low.component.subarray_health_state
        == HealthState.DEGRADED
    )


def set_failed(devFactory, cm, expected_elapsed_time):
    proxy = devFactory.get_device("low-sdp/subarray/01")
    proxy.SetDirectHealthState(HealthState.FAILED)
    assert proxy.HealthState == HealthState.FAILED
    start_time = time.time()
    elapsed_time = 0
    while cm.component.subarray_health_state != HealthState.FAILED:
        elapsed_time = time.time() - start_time
        time.sleep(0.1)
        if elapsed_time > TIMEOUT:
            pytest.fail("Timeout occurred while executing the test")
    assert elapsed_time < expected_elapsed_time


def test_set_health_state_failed_events(tango_context, component_manager_low):
    devFactory = DevFactory()
    num_faulty = count_faulty_devices(component_manager_low)
    assert num_faulty == 0
    set_failed(devFactory, component_manager_low, expected_elapsed_time=100)
    assert (
        component_manager_low.component.subarray_health_state
        == HealthState.FAILED
    )


def set_device_unknown(devFactory, cm, expected_elapsed_time):
    proxy = devFactory.get_device("low-sdp/subarray/01")
    proxy.SetDirectHealthState(HealthState.UNKNOWN)
    assert proxy.HealthState == HealthState.UNKNOWN
    start_time = time.time()
    elapsed_time = 0
    while cm.component.subarray_health_state != HealthState.UNKNOWN:
        elapsed_time = time.time() - start_time
        time.sleep(0.1)
        if elapsed_time > TIMEOUT:
            pytest.fail("Timeout occurred while executing the test")
    assert elapsed_time < expected_elapsed_time


def test_set_health_state_unknown_events(tango_context, component_manager_low):
    devFactory = DevFactory()
    num_faulty = count_faulty_devices(component_manager_low)
    assert num_faulty == 0
    set_device_unknown(devFactory, component_manager_low, 15)
    assert (
        component_manager_low.component.subarray_health_state
        == HealthState.UNKNOWN
    )
