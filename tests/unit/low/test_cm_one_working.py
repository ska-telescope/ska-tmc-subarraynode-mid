import time

import pytest
from ska_tmc_common.test_helpers.helper_subarray_leaf_device import (
    HelperSubarrayLeafDevice,
)

from tests.settings import (
    DEVICE_LIST_LOW,
    LOW_SDPSUBARRAY_LEAF_NODE,
    TIMEOUT,
    count_faulty_devices,
    create_low_cm,
    logger,
    set_devices_unresponsive,
)


@pytest.fixture()
def devices_to_load():
    return (
        {
            "class": HelperSubarrayLeafDevice,
            "devices": [
                {"name": LOW_SDPSUBARRAY_LEAF_NODE},
            ],
        },
    )


@pytest.mark.skip(
    reason="Liveliness probe implementation removed for unit tests"
)
def test_one_working_other_faulty(tango_context, component_manager_low):
    logger.info("%s", tango_context)

    for dev in DEVICE_LIST_LOW:
        component_manager_low.get_device(dev)
    set_devices_unresponsive(component_manager_low, DEVICE_LIST_LOW)

    assert len(DEVICE_LIST_LOW) - 1 != count_faulty_devices(
        component_manager_low
    )
