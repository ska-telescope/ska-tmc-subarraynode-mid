import time

import pytest

from tests.settings import count_faulty_devices, create_low_cm, logger


@pytest.mark.skip(
    reason="Liveliness probe implementation removed for unit tests"
)
def test_all_working(tango_context, component_manager_low):
    logger.info("%s", tango_context)
    num_faulty = count_faulty_devices(component_manager_low)
    assert num_faulty == 0
    start_time = time.time()
    elapsed_time = time.time() - start_time
    logger.info("checked %s devices in %s", num_faulty, elapsed_time)
    for dev_info in component_manager_low.devices:
        assert not dev_info.unresponsive
