"""
conftest module for Subarray Node.
"""
import json
import logging
import os
import time
from functools import partial
from multiprocessing import Manager
from os.path import dirname, join

import mock
import pytest
import tango
from ska_control_model import ObsState, ObsStateModel
from ska_tango_base.base.base_device import CommandTracker
from ska_tango_testing.mock import MockCallable
from ska_tango_testing.mock.tango.event_callback import (
    MockTangoEventCallbackGroup,
)
from ska_tmc_common import (
    DevFactory,
    HelperCspSubarrayLeafDevice,
    HelperDishDevice,
    HelperDishLNDevice,
    HelperSdpQueueConnector,
    HelperSdpSubarray,
    HelperSdpSubarrayLeafDevice,
    HelperSubArrayDevice,
    HelperSubarrayLeafDevice,
    TMCOpStateModel,
)
from tango.test_context import MultiDeviceTestContext

from ska_tmc_subarraynode.manager import (
    AggregationProcessLow,
    AggregationProcessMid,
    SubarrayNodeComponentManagerLow,
    SubarrayNodeComponentManagerMid,
)
from ska_tmc_subarraynode.model.input import (
    InputParameterLow,
    InputParameterMid,
)
from tests.settings import DEVICE_LIST_LOW, DEVICE_LIST_MID, logger
from tests.test_helpers.common_utils import SubarrayNode
from tests.test_helpers.constants import (
    DISH_LEAF_NODE,
    DISH_LEAF_NODE2,
    DISH_LEAF_NODE36,
    DISH_LEAF_NODE63,
    DISH_LEAF_NODE100,
    DISH_MASTER_DEVICE1,
    DISH_MASTER_DEVICE2,
    DISH_MASTER_DEVICE36,
    DISH_MASTER_DEVICE63,
    DISH_MASTER_DEVICE100,
    LOW_CSPSUBARRAY_LEAF_NODE,
    LOW_CSPSUBARRAY_NODE_NAME,
    LOW_SDPSUBARRAY_LEAF_NODE,
    LOW_SDPSUBARRAY_NODE_NAME,
    MCCS_SUBARRAY_LEAF_NODE,
    MCCSSUBARRAY_NODE_NAME,
    MID_CSPSUBARRAY_LEAF_NODE,
    MID_CSPSUBARRAY_NODE_NAME,
    MID_SDPSUBARRAY_LEAF_NODE,
    MID_SDPSUBARRAY_NODE_NAME,
    SDPQC_FQDN,
    DishLeafNodePrefix,
)
from tests.test_helpers.event_recorder import EventRecorder

TELESCOPE_ENV = os.getenv("TELESCOPE")


def pytest_sessionstart(session):
    """
    Pytest hook; prints info about tango version.
    :param session: a pytest Session object
    :type session: :py:class:`pytest.Session`
    """
    print(tango.utils.info())


def pytest_addoption(parser):
    """
    Pytest hook; implemented to add the `--true-context` option, used to
    indicate that a true Tango subsystem is available, so there is no
    need for a :py:class:`tango.test_context.MultiDeviceTestContext`.
    :param parser: the command line options parser
    :type parser: :py:class:`argparse.ArgumentParser`
    """
    parser.addoption(
        "--true-context",
        action="store_true",
        default=False,
        help=(
            "Tell pytest that you have a true Tango context and don't "
            "need to spin up a Tango test context"
        ),
    )


@pytest.fixture()
def devices_to_load():
    """This method returns the devices whose context are required"""
    return (
        {
            "class": HelperSubarrayLeafDevice,
            "devices": [{"name": MCCS_SUBARRAY_LEAF_NODE}],
        },
        {
            "class": HelperSubArrayDevice,
            "devices": [
                {"name": MCCSSUBARRAY_NODE_NAME},
                {"name": LOW_CSPSUBARRAY_NODE_NAME},
                {"name": MID_CSPSUBARRAY_NODE_NAME},
            ],
        },
        {
            "class": HelperCspSubarrayLeafDevice,
            "devices": [
                {"name": MID_CSPSUBARRAY_LEAF_NODE},
                {"name": LOW_CSPSUBARRAY_LEAF_NODE},
            ],
        },
        {
            "class": HelperSdpSubarrayLeafDevice,
            "devices": [
                {"name": MID_SDPSUBARRAY_LEAF_NODE},
                {"name": LOW_SDPSUBARRAY_LEAF_NODE},
            ],
        },
        {
            "class": HelperDishDevice,
            "devices": [
                {"name": DISH_MASTER_DEVICE1},
                {"name": DISH_MASTER_DEVICE2},
                {"name": DISH_MASTER_DEVICE36},
                {"name": DISH_MASTER_DEVICE63},
                {"name": DISH_MASTER_DEVICE100},
            ],
        },
        {
            "class": HelperDishLNDevice,
            "devices": [
                {"name": DISH_LEAF_NODE},
                {"name": DISH_LEAF_NODE2},
                {"name": DISH_LEAF_NODE36},
                {"name": DISH_LEAF_NODE63},
                {"name": DISH_LEAF_NODE100},
            ],
        },
        {
            "class": HelperSdpSubarray,
            "devices": [
                {"name": LOW_SDPSUBARRAY_NODE_NAME},
                {"name": MID_SDPSUBARRAY_NODE_NAME},
            ],
        },
        {
            "class": HelperSdpQueueConnector,
            "devices": [{"name": SDPQC_FQDN}],
        },
    )


@pytest.fixture
def tango_context(devices_to_load, request):
    """This Fixture provide the tango context fixture"""
    true_context = request.config.getoption("--true-context")
    logger.info("true context: %s", true_context)
    if not true_context:
        with MultiDeviceTestContext(
            devices_to_load, process=True, timeout=100
        ) as context:
            DevFactory._test_context = context
            logger.info("test context set")
            yield context
    else:
        yield None


@pytest.fixture()
def subarray_node(event_recorder, request):
    """Returns the instance of SubarrayNode class"""

    if "low" in TELESCOPE_ENV:
        deployment = "LOW"
    else:
        deployment = "MID"
    subarray_node = SubarrayNode(deployment)
    yield subarray_node

    subarray_node.updated_tear_down(event_recorder)
    subarray_node.clear_command_call_info()


@pytest.fixture()
def event_recorder():
    """Records events and allows assertions"""
    event_recorder = EventRecorder()
    yield event_recorder

    event_recorder.clear_events()


@pytest.fixture()
def change_event_callbacks() -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of Tango device change event
    callbacks with asynchrony support.

    :return: a collections.defaultdict that returns change event
        callbacks by name.
    """
    return MockTangoEventCallbackGroup(
        "longRunningCommandStatus",
        "longRunningCommandsInQueue",
        "longRunningCommandResult",
        "cspSubarrayObsState",
        "sdpSubarrayObsState",
        "obsState",
        "pointing_offsets",
        timeout=50.0,
    )


def get_input_str(path):
    """
    Returns input json string
    :rtype: String
    """
    with open(path, "r", encoding="utf-8") as f:
        input_arg = json.load(f)
    return json.dumps(input_arg)


@pytest.fixture()
def json_factory():
    """
    Json factory for getting json files
    """

    def _get_json(slug):
        return get_input_str(join(dirname(__file__), "data", f"{slug}.json"))

    return _get_json


@pytest.fixture
def task_callback() -> MockCallable:
    """Creates a mock callable for asynchronous testing

    :rtype: MockCallable
    """
    task_callback = MockCallable(100)
    return task_callback


@pytest.fixture
def set_mid_sdp_csp_leaf_node_availability_for_aggregation():
    """
    Setting mid Csp subarray leaf node and Sdp subarray leaf node availabilty
    attribute isSubsystemAvailable as True for aggregation
    """
    dev_factory = DevFactory()
    proxy_csp_sa_ln = dev_factory.get_device(MID_CSPSUBARRAY_LEAF_NODE)
    proxy_csp_sa_ln.SetSubsystemAvailable(True)

    proxy_sdp_sa_ln = dev_factory.get_device(MID_SDPSUBARRAY_LEAF_NODE)
    proxy_sdp_sa_ln.SetSubsystemAvailable(True)

    proxy_dish_ln = dev_factory.get_device(DISH_LEAF_NODE)
    proxy_dish_ln.SetSubsystemAvailable(True)

    logger.debug(
        "CspSubarrayLeafNode availability is: %s",
        proxy_csp_sa_ln.isSubsystemAvailable,
    )
    logger.debug(
        "SdpSubarrayLeafNode availability is: %s",
        proxy_sdp_sa_ln.isSubsystemAvailable,
    )
    logger.debug(
        "DishLeafNode availability is: %s", proxy_dish_ln.isSubsystemAvailable
    )


@pytest.fixture
def set_low_sdp_csp_leaf_node_availability_for_aggregation():
    """
    Setting low Csp subarray leaf node and Sdp subarray leaf node availabilty
    attribute isSubsystemAvailable as True for aggregation
    """
    dev_factory = DevFactory()
    proxy_csp_sa_ln = dev_factory.get_device(LOW_CSPSUBARRAY_LEAF_NODE)
    proxy_csp_sa_ln.SetSubsystemAvailable(True)

    proxy_sdp_sa_ln = dev_factory.get_device(LOW_SDPSUBARRAY_LEAF_NODE)
    proxy_sdp_sa_ln.SetSubsystemAvailable(True)

    logger.debug(
        "CspSubarrayLeafNode availability is: %s",
        proxy_csp_sa_ln.isSubsystemAvailable,
    )
    logger.debug(
        "SdpSubarrayLeafNode availability is: %s",
        proxy_sdp_sa_ln.isSubsystemAvailable,
    )


def component_state_changed_callback(
    cm,
    state_change,
) -> None:
    """
    Handle change in this device's state.

    This is a callback hook, called whenever the state changes. It
    is responsible for updating the tango side of things i.e. making
    sure the attribute is up to date, and events are pushed.

    :param state_change: A dictionary containing the name of the state
        that changed and its new value.
    :param fqdn: The fqdn of the device.
    """
    if "resourced" in state_change.keys():
        if state_change.get("resourced", True):
            # If Initial obsState is EMPTY, then only perform action
            # "component_resourced" is needed
            # to transition SubarrayNode obsState from RESOURCING_EMPTY
            # to RESOURCING_IDLE.
            if cm.initial_obs_state == ObsState.EMPTY:
                cm.obs_state_model.perform_action("component_resourced")
        else:
            # If Initial obsState is IDLE, then only perform action
            # "component_unresourced" is needed
            # to transition SubarrayNode obsState from RESOURCING_IDLE
            # to RESOURCING_EMPTY.
            if cm.initial_obs_state in (
                ObsState.IDLE,
                ObsState.EMPTY,
            ):
                cm.obs_state_model.perform_action("component_unresourced")

    if "configured" in state_change.keys():
        if state_change.get("configured", True):
            # If Initial obsState is IDLE, then only perform action
            # "component_configured" is needed
            # to transition SubarrayNode obsState from RESOURCING_IDLE
            # to RESOURCING_READY.
            if cm.initial_obs_state == ObsState.IDLE:
                cm.obs_state_model.perform_action("component_configured")
        else:
            # If Initial obsState is READY, then only perform action
            # "component_unconfigured" is needed.
            if cm.initial_obs_state in (
                ObsState.READY,
                ObsState.IDLE,
            ):
                cm.obs_state_model.perform_action("component_unconfigured")
    if "scanning" in state_change.keys():
        if state_change.get("scanning", True):
            # If Initial obsState is READY, then only perform action
            # "component_configured" is needed
            if cm.initial_obs_state == ObsState.READY:
                cm.obs_state_model.perform_action("component_scanning")
        else:
            cm.obs_state_model.perform_action("component_not_scanning")

    if "assign_completed" in state_change.keys():
        cm.obs_state_model.perform_action("assign_completed")
    if "release_completed" in state_change.keys():
        cm.obs_state_model.perform_action("release_completed")
    if "configure_completed" in state_change.keys():
        cm.obs_state_model.perform_action("configure_completed")
    if "abort_completed" in state_change.keys():
        cm.obs_state_model.perform_action("abort_completed")
    if "restart_completed" in state_change.keys():
        cm.obs_state_model.perform_action("restart_completed")
    if "component_obsfault" in state_change.keys():
        cm.obs_state_model.perform_action("component_obsfault")


@pytest.fixture
def component_manager_mid():
    """Create a component manager instance for MID."""
    command_tracker = CommandTracker(
        queue_changed_callback=None,
        status_changed_callback=None,
        progress_changed_callback=None,
        result_callback=None,
    )
    cm = SubarrayNodeComponentManagerMid(
        TMCOpStateModel(logger),
        ObsStateModel(logger=logger),
        command_tracker,
        logger=logger,
        _input_parameter=InputParameterMid(None),
        dev_family="leaf-node",
        _liveliness_probe=False,
        command_timeout=100,
    )

    cm.component_state_changed_callback = partial(
        component_state_changed_callback, cm
    )

    cm.input_parameter.tmc_dish_ln_device_names = [
        DISH_LEAF_NODE,
        DISH_LEAF_NODE100,
        DISH_LEAF_NODE36,
    ]
    cm.input_parameter.dish_ln_prefix = DishLeafNodePrefix
    cm.input_parameter.dish_dev_names = [DISH_MASTER_DEVICE1]
    cm.input_parameter.dish_master_identifier = "elt/master"
    cm.input_parameter.tmc_csp_sln_device_name = MID_CSPSUBARRAY_LEAF_NODE
    cm.input_parameter.tmc_sdp_sln_device_name = MID_SDPSUBARRAY_LEAF_NODE
    cm.input_parameter.csp_subarray_dev_name = MID_CSPSUBARRAY_NODE_NAME
    cm.input_parameter.sdp_subarray_dev_name = MID_SDPSUBARRAY_NODE_NAME
    for device in DEVICE_LIST_MID:
        cm.add_device_to_lp(device)
    yield cm
    cm.__del__()


@pytest.fixture
def component_manager_low():
    """Create a component manager instance for LOW."""
    command_tracker = CommandTracker(
        queue_changed_callback=None,
        status_changed_callback=None,
        progress_changed_callback=None,
        result_callback=None,
    )
    cm = SubarrayNodeComponentManagerLow(
        TMCOpStateModel(logger),
        ObsStateModel(logger=logger),
        command_tracker,
        logger=logger,
        _input_parameter=InputParameterLow(None),
        dev_family="leaf-node",
        _liveliness_probe=False,
    )
    cm.component_state_changed_callback = partial(
        component_state_changed_callback, cm
    )
    cm.input_parameter.tmc_mccs_sln_device_name = MCCS_SUBARRAY_LEAF_NODE
    cm.input_parameter.mccs_subarray_dev_name = MCCSSUBARRAY_NODE_NAME
    cm.input_parameter.tmc_csp_sln_device_name = LOW_CSPSUBARRAY_LEAF_NODE
    cm.input_parameter.tmc_sdp_sln_device_name = LOW_SDPSUBARRAY_LEAF_NODE
    cm.input_parameter.csp_subarray_dev_name = LOW_CSPSUBARRAY_NODE_NAME
    cm.input_parameter.sdp_subarray_dev_name = LOW_SDPSUBARRAY_NODE_NAME
    for device in DEVICE_LIST_LOW:
        cm.add_device_to_lp(device)
    yield cm
    cm.__del__()


@pytest.fixture
def aggregation_process_mid():
    """Create aggregation process mid object"""
    process_manager = Manager()
    event_queue = process_manager.Queue()
    aggregated_obs_state = process_manager.list([""])
    mock_obj = mock.Mock()

    aggregation_process = AggregationProcessMid(
        event_queue, aggregated_obs_state, mock_obj
    )

    yield aggregation_process

    aggregation_process.stop_aggregation_process()
    process_manager.shutdown()


@pytest.fixture
def aggregation_process_low():
    """Create aggregation process low object"""
    process_manager = Manager()
    event_queue = process_manager.Queue()
    aggregated_obs_state = process_manager.list([""])
    mock_obj = mock.Mock()
    aggregation_process = AggregationProcessLow(
        event_queue, aggregated_obs_state, mock_obj
    )

    yield aggregation_process
    aggregation_process.stop_aggregation_process()
    process_manager.shutdown()


def get_scan_input_str(scan_input_file="Scan_mid.json"):
    path = join(dirname(__file__), "data", scan_input_file)
    with open(path, "r") as f:
        scan_input_file = f.read()
    return scan_input_file
