"""Utils methods for subarray
"""
from typing import Tuple

from ska_tmc_common import AdapterFactory, AdapterType


def get_dish_ln_adapters(
    dish_ln_names: list, adapter_factory: AdapterFactory, logger
):
    """Set the Queue Connector FQDN on Dish Leaf Nodes
    :param adapter_factory: Adapter Factory
    :type adapter_factory: Adapter factory
    """
    error_dev_names = []
    dish_adapters = []
    for device_name in dish_ln_names:
        try:
            adapter = adapter_factory.get_or_create_adapter(
                device_name, AdapterType.DISH
            )
            dish_adapters.append(adapter)
        except Exception as e:
            logger.error(
                "Error in creating adapter for %s: %s",
                device_name,
                e,
            )
            error_dev_names.append(device_name)
    return dish_adapters, error_dev_names


def split_interface_version(version: str) -> Tuple[int, int]:
    """Extracts version number from interface URI

    :param version: Version string
    :returns: (major version, minor version) tuple
    """
    version_num = version.rsplit("/", 1)[1]
    (major_version, minor_version) = version_num.split(".")
    return int(major_version), int(minor_version)
