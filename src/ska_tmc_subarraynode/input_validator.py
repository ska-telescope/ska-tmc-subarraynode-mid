"""
This module is for validationg the input strings for the repository
"""

# standard Python imports
import logging
from typing import List, Optional

from ska_ser_logging import configure_logging
from ska_tmc_cdm.exceptions import JsonValidationError, SchemaNotFound
from ska_tmc_cdm.messages.subarray_node.configure import ConfigureRequest
from ska_tmc_cdm.messages.subarray_node.scan import ScanRequest

# SKA specific imports
from ska_tmc_cdm.schemas import CODEC
from ska_tmc_common.exceptions import InvalidJSONError

from ska_tmc_subarraynode.utils.constants import ERROR_MESSAGE, REQUIRED_KEYS

configure_logging()
LOGGER = logging.getLogger(__name__)


class ConfigureValidator:
    """Class to validate the input string of
    Configure command of Subarray Node
    """

    def __init__(self, component_manager, logger: logging.Logger = LOGGER):
        self.component_manager = component_manager
        self.logger = logger

    def loads(self, input_string):
        """
        Validates the input string received as an
        argument of Configure command.
        If the request is correct, returns the Serialized JSON string.
        The ska-tmc-cdm is used to validate the JSON.

        :param input_string: A JSON string
        :type input_string: str
        :param telescope: Mid or Low json validations
        :type telescope: str

        :return: Serialized JSON string if successful.

        :throws:
            InvalidJSONError: When the JSON string is not formatted properly.
        """
        # Check if JSON is correct
        try:
            configure_request = CODEC.loads(ConfigureRequest, input_string)
            configure_json = CODEC.dumps(configure_request)
        except (
            JsonValidationError,
            SchemaNotFound,
            ValueError,
            Exception,
        ) as json_error:
            self.logger.exception(
                "Exception occured while validating the json with cdm: %s",
                str(json_error),
            )
            raise InvalidJSONError(
                "Malformed input string. Please check the JSON format."
                + f"Exception with cdm -> {str(json_error)}"
            ) from json_error
        return configure_json

    def validate_groups_key_data(self, group_list: list) -> None:
        """Validate the trajectory pattern and receptors in the provided
        holography configuration.

        This method checks the trajectory pattern for each group in the
        provided list. Currently, the TMC only supports the 'mosaic'
        pattern. If any other pattern is provided, the command will be rejected

        It also validates receptors based on the number of groups.
        If there are multiple groups, receptors must be provided for each group

        :param group_list: A list containing groups with configuration data,
                        including trajectory and receptors information.
        :type group_list: list
        :returns: True if the validation passes, raises InvalidJSONError if it
        fails.
        :rtype: bool
        """
        assigned_receptors = []
        group_count = len(group_list)

        # Step 1: Check number of groups
        if group_count == 0:
            raise InvalidJSONError("Number of groups cannot be zero.")

        # If there is more than one group, receptors must be validated
        if group_count > 1:
            for group_info in group_list:
                receptors = list(set(group_info.get("receptors")))
                trajectory_info = group_info.get("trajectory")
                if trajectory_info and trajectory_info["name"] != "mosaic":
                    raise InvalidJSONError("TMC support only mosaic pattern")
                if not receptors:
                    raise InvalidJSONError(
                        "Receptor key is mandatory if there are multiple "
                        "groups."
                    )
                for receptor in receptors:
                    if receptor in assigned_receptors:
                        raise InvalidJSONError(
                            f"Receptor {receptor} is already assigned to "
                            "another group."
                        )
                    assigned_receptors.append(receptor)
            self.verify_receptor_in_assigned_resources(assigned_receptors)

        if group_count == 1:
            receptors = group_list[0].get("receptors", [])
            if not receptors:
                receptors = self.component_manager.get_assigned_resources()
            self.verify_receptor_in_assigned_resources(receptors)

    def verify_receptor_in_assigned_resources(self, receptors: List) -> None:
        """Verify if receptors are in assigned_resources.

        Raises:
        InvalidJSONError: If any receptor is not allocated to the subarray.
        """
        subarray_receptors = frozenset(
            self.component_manager.get_assigned_resources()
        )
        requested_receptors = frozenset(receptors)
        receptors_not_in_subarray = requested_receptors.difference(
            subarray_receptors
        )

        if receptors_not_in_subarray:
            raise InvalidJSONError(
                "Request includes receptors not allocated to the subarray:"
                f"{receptors_not_in_subarray}"
            )

    def verify_json_with_keys(
        self, input_json: dict, telescope: str
    ) -> Optional[InvalidJSONError]:
        """Verify the given json to check if it contains all the expected keys
        for a configure json.

        :param input_json: The input json for the Configure command.
        :type input_json: Dict

        :returns: InvalidJSONError if the validation fails.
        """
        if not input_json.get("tmc"):
            raise InvalidJSONError(ERROR_MESSAGE.format(key="tmc"))

        if input_json["tmc"].get("partial_configuration", False):
            return

        required_keys = REQUIRED_KEYS[telescope]
        for key in required_keys:
            if key not in input_json.keys():
                raise InvalidJSONError(ERROR_MESSAGE.format(key=key))

        if telescope == "LOW":
            try:
                int(input_json["tmc"]["scan_duration"])
            except Exception:
                raise InvalidJSONError(
                    "Malformed input string. The key -> `scan_duration`"
                    + " is missing, or not an integer in the json."
                ) from Exception


class ScanValidator:

    """Class to validate the input string of Scan command of Subarray Node"""

    def __init__(self, logger: logging.Logger = LOGGER):
        self.logger = logger

    def loads(self, input_string):
        """
        Validates the input string received as an argument of Scan command.
        If the request is correct, returns the Serialized JSON string.
        The ska-tmc-cdm is used to validate the JSON.

        :param: input_string: A JSON string

        :return: Serialized JSON string if successful.

        :throws:
            InvalidJSONError: When the JSON string is not formatted properly.
        """

        # Check if JSON is correct
        self.logger.info("Checking scan JSON format.")
        try:
            scan_request = CODEC.loads(ScanRequest, input_string)
            scan_json = CODEC.dumps(scan_request)
        except (
            JsonValidationError,
            SchemaNotFound,
            ValueError,
            Exception,
        ) as json_error:
            self.logger.exception("Exception: %s", str(json_error))
            exception_message = (
                "Malformed input string. Please check the JSON format."
                + "Full exception info: "
                + str(json_error)
            )
            raise InvalidJSONError(exception_message) from json_error

        return scan_json

    def validate_scan_ids(self, trajectory_attrs: list, scan_ids: list):
        """Ensure the length of the trajectory offset list matches the length
        of the scan IDs list.
        :param trajectory_attrs: Trajectory attributes provided during
        configuration.
        :type trajectory_attrs: list
        :param scan_ids: List of scan IDs.
        :type scan_ids: list
        """
        if len(trajectory_attrs) != len(scan_ids):
            error_message = (
                "Number of scan ids should be same as number of offsets"
            )
            raise InvalidJSONError(error_message) from Exception
