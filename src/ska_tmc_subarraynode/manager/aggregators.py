"""
This Module is for Aggregating the HealthState and
 ObsStates of Devices
"""
# pylint: disable=inconsistent-return-statements
import logging
from enum import IntEnum
from typing import List, Tuple

from ska_control_model.admin_mode import AdminMode
from ska_ser_logging import configure_logging
from ska_tango_base.control_model import HealthState, ObsState
from ska_tmc_common import Aggregator

configure_logging()
LOGGER = logging.getLogger(__name__)


class HealthStateAggregatorMid(Aggregator):
    """The HealthStateAggregatorMid class is
    an implementation of an aggregator that is responsible
    for aggregating the health states of various
    devices in the mid telescope
    """

    def __init__(self, cm, logger: logging.Logger = LOGGER) -> None:
        self.logger = logger
        super().__init__(cm, logger)

    def aggregate(self) -> IntEnum:
        """
        This method aggregating HealthState in Mid telescope
        """
        # import debugpy; debugpy.debug_this_thread()
        subarray_healthstates = []
        dish_healthstates = []
        csp_subarray = False
        sdp_subarray = False
        is_admin_mode_offline = False
        # get health states of CspSubarray, SdpSubarray and DishMaster devices
        for dev in self._component_manager.checked_devices:
            name = dev.dev_name
            input_param = self._component_manager.input_parameter
            if dev.unresponsive:
                continue
            if name == input_param.csp_subarray_dev_name:
                subarray_healthstates.append(dev.health_state)
                csp_subarray = True
            if name == input_param.sdp_subarray_dev_name:
                subarray_healthstates.append(dev.health_state)
                sdp_subarray = True
            if name in input_param.dish_dev_names:
                dish_healthstates.append(dev.health_state)
            if (
                self._component_manager.is_admin_mode_enabled
                and hasattr(dev, "admin_mode")
                and dev.admin_mode == AdminMode.OFFLINE
            ):
                is_admin_mode_offline = True
                break

        if (
            is_admin_mode_offline
            and self._component_manager.is_admin_mode_enabled
        ):
            return HealthState.DEGRADED

        health_states = subarray_healthstates + dish_healthstates
        unique_health_states = set(health_states)
        unique_dish_healthstates = set(dish_healthstates)
        if self._component_manager.kvalue_validation_failed:
            return HealthState.DEGRADED
        if not sdp_subarray and not csp_subarray:
            return HealthState.UNKNOWN
        if unique_health_states == set([HealthState.OK]):
            return HealthState.OK
        if HealthState.FAILED in unique_health_states:
            # If CSP or SDP Subarray HealthState is FAILED return FAILED
            if HealthState.FAILED in subarray_healthstates:
                return HealthState.FAILED
            # If HealthState is FAILED for all the dishes return FAILED
            if unique_dish_healthstates == set([HealthState.FAILED]):
                return HealthState.FAILED
            # If HealthState is FAILED for not all the dishes return DEGRADED
            return HealthState.DEGRADED
        if HealthState.DEGRADED in unique_health_states:
            return HealthState.DEGRADED

        return HealthState.UNKNOWN


class HealthStateAggregatorLow(Aggregator):
    """The HealthStateAggregatorLow class is an implementation of an
    aggregator that is responsible for aggregating the health states
    of various devices in the low telescope.
    """

    def __init__(self, cm, logger) -> None:
        self.logger = logger
        super().__init__(cm, logger)

    def aggregate(self) -> IntEnum:
        """
        This method aggregates HealthState for Low Subarray
        """
        subarray_healthstates = []
        csp_subarray = False
        sdp_subarray = False
        mccs_subarray = False
        is_admin_mode_offline = False
        # get health states of CspSubarray, SdpSubarray and MccsSubarray
        for dev in self._component_manager.checked_devices:
            device_name = dev.dev_name.lower()
            input_param = self._component_manager.input_parameter
            if dev.unresponsive:
                continue
            if device_name == input_param.csp_subarray_dev_name:
                subarray_healthstates.append(dev.health_state)
                csp_subarray = True
            if device_name == input_param.sdp_subarray_dev_name:
                subarray_healthstates.append(dev.health_state)
                sdp_subarray = True
            if device_name == input_param.mccs_subarray_dev_name:
                subarray_healthstates.append(dev.health_state)
                mccs_subarray = True
            if (
                self._component_manager.is_admin_mode_enabled
                and hasattr(dev, "admin_mode")
                and dev.admin_mode == AdminMode.OFFLINE
            ):
                is_admin_mode_offline = True
                break

        if (
            is_admin_mode_offline
            and self._component_manager.is_admin_mode_enabled
        ):
            return HealthState.DEGRADED

        health_states = subarray_healthstates
        unique_health_states = set(health_states)

        if not sdp_subarray and not csp_subarray and not mccs_subarray:
            return HealthState.UNKNOWN
        if unique_health_states == set([HealthState.OK]):
            return HealthState.OK
        if HealthState.FAILED in unique_health_states:
            if HealthState.FAILED in subarray_healthstates:
                return HealthState.FAILED

        if HealthState.DEGRADED in unique_health_states:
            return HealthState.DEGRADED

        return HealthState.UNKNOWN


class SubarrayAvailabilityAggregatorMid(Aggregator):
    """
    class to aggregate tmc mid subarray device
    availability depending on tmc mid leaf nodes
    """

    def __init__(self, component_manager, logger) -> None:
        super().__init__(component_manager, logger)
        self.unavailable_devices: list = []
        self.devices_availability_dict: dict = {}

    def aggregate(self) -> Tuple[bool, List]:
        """Aggregates the subarray availability"""

        csp_subarray_ln = False
        sdp_subarray_ln = False
        subarray_availability_status = []
        dish_availability_status = []
        self.unavailable_devices = []
        dish_ln = False

        for device in self._component_manager.checked_devices:
            name = device.dev_name
            input_param = self._component_manager.input_parameter
            if device.unresponsive:
                continue
            if name == input_param.tmc_csp_sln_device_name:
                subarray_availability_status.append(device.device_availability)

                if device.device_availability is False:
                    self.unavailable_devices.append(
                        input_param.csp_subarray_dev_name
                    )
                csp_subarray_ln = True
                self.devices_availability_dict[name] = [
                    device.device_availability,
                    csp_subarray_ln,
                ]
                subarray_availability_status.append(csp_subarray_ln)
            elif name == input_param.tmc_sdp_sln_device_name:
                subarray_availability_status.append(device.device_availability)
                if device.device_availability is False:
                    self.unavailable_devices.append(
                        input_param.sdp_subarray_dev_name
                    )
                sdp_subarray_ln = True
                self.devices_availability_dict[name] = [
                    device.device_availability,
                    sdp_subarray_ln,
                ]
                subarray_availability_status.append(sdp_subarray_ln)
            elif (
                self._component_manager.input_parameter.dish_ln_prefix.lower()
                in name.lower()
            ):
                dish_availability_status.append(device.device_availability)
                self.devices_availability_dict[
                    name
                ] = device.device_availability
                if device.device_availability is False:
                    # extract last 3 digit
                    ID = name.lower()[-3:]
                    for dish in input_param.dish_dev_names:
                        if ID.lower() in dish.lower():
                            self.unavailable_devices.append(dish)
                dish_ln = True
                self.devices_availability_dict[name] = [
                    device.device_availability,
                    dish_ln,
                ]
                dish_availability_status.append(dish_ln)

        if not csp_subarray_ln:
            self._logger.info(
                f"CSP Subarray Leaf Node is unavailable: {csp_subarray_ln}"
            )
            return False, self.unavailable_devices
        if not sdp_subarray_ln:
            self._logger.info(
                f"SDP Subarray Leaf Node is unavailable: {sdp_subarray_ln}"
            )
            return False, self.unavailable_devices

        if (self._component_manager.subarray_obsstate in [ObsState.IDLE]) and (
            not dish_ln
        ):
            self._logger.info(
                "Dish Master devices are assigned however the DishLeafNodes "
                + f"associated with Dish Masters are unavailable: {dish_ln}"
            )
            self._logger.info(
                f"Unavailable devices: {self.unavailable_devices}"
            )
            return False, self.unavailable_devices

        if not all(subarray_availability_status):
            self._logger.debug(
                f"Unavailable devices: {self.unavailable_devices}"
            )
            return False, self.unavailable_devices
        if all(subarray_availability_status):
            # Check if all the dishes are available or not
            if self._component_manager.subarray_obsstate in [ObsState.IDLE]:
                if all(dish_availability_status):
                    return True, []

                self._logger.debug(
                    f"Unavailable dishes:{self.unavailable_devices}"
                )
                return False, self.unavailable_devices

        return True, []


class SubarrayAvailabilityAggregatorLow(Aggregator):
    """
    Class to aggregate tmc low subarray device availability
    depending on tmc low leaf nodes
    """

    def __init__(self, component_manager, logger) -> None:
        super().__init__(component_manager, logger)
        self.unavailable_devices: list = []
        self.devices_availability_dict: dict = {}

    def aggregate(self):
        """Aggregates the subarray availability"""

        csp_subarray_ln = False
        sdp_subarray_ln = False
        subarray_availability_status = []
        self.unavailable_devices = []
        for device in self._component_manager.checked_devices:
            name = device.dev_name.lower()
            input_param = self._component_manager.input_parameter
            if device.unresponsive:
                continue
            if name == input_param.tmc_csp_sln_device_name:
                subarray_availability_status.append(device.device_availability)
                if device.device_availability is False:
                    self.unavailable_devices.append(device.dev_name)
                csp_subarray_ln = True
                subarray_availability_status.append(csp_subarray_ln)
                self.devices_availability_dict[name] = [
                    device.device_availability,
                    csp_subarray_ln,
                ]
            if name == input_param.tmc_sdp_sln_device_name:
                subarray_availability_status.append(device.device_availability)
                if device.device_availability is False:
                    self.unavailable_devices.append(device.dev_name)
                sdp_subarray_ln = True
                subarray_availability_status.append(sdp_subarray_ln)
                self.devices_availability_dict[name] = [
                    device.device_availability,
                    sdp_subarray_ln,
                ]

        if not csp_subarray_ln:
            self._logger.debug(
                f"CspSubarrayLeafNode is unavailable: {csp_subarray_ln}"
            )
            return False, self.unavailable_devices
        if not sdp_subarray_ln:
            self._logger.debug(
                f"SdpSubarrayLeafNode is unavailable: {sdp_subarray_ln}"
            )
            return False, self.unavailable_devices
        if not all(subarray_availability_status):
            self._logger.debug(
                f"Unavailable devices: {self.unavailable_devices}"
            )
            return False, self.unavailable_devices
        if all(subarray_availability_status):
            return True, []


class ObsStateAggregator:
    """New Aggregator class for Mid"""

    def __init__(self, obs_state_rules: dict):
        """
        :param obs_state_rules: Rules to use for aggregation
        :type obs_state_rules: dict
        """
        self.obs_state_rules = obs_state_rules

    def aggregate(self, event_data: dict) -> ObsState:
        """Aggregate ObsState based on event data
        :param event_data: event data dict which contain data required
        for aggregation
        :type event_data: dict
        """
        LOGGER.debug("Got event data for aggregation %s", event_data)
        for (
            obs_state,
            obs_state_rules,
        ) in self.obs_state_rules.items():
            LOGGER.debug("Checking rules for obsState %s", obs_state)
            if any(
                obs_state_rule.matches(event_data)
                for obs_state_rule in obs_state_rules
            ):
                return (
                    ObsState[obs_state]
                    if hasattr(ObsState, obs_state)
                    else obs_state
                )
