"""This Lists all rules required for ObsState aggregation
"""

from rule_engine import Rule

OBS_STATE_RULES = {
    "EMPTY": [
        Rule(
            'command_in_progress == "ReleaseAllResources" ? '
            '["EMPTY"] == all_unique_obs_states '
            'and ["OK"] == all_unique_command_results'
            ' : ["EMPTY"] == all_unique_obs_states'
        )
    ],
    "ABORTING": [Rule('"ABORTING" in all_unique_obs_states')],
    "RESOURCING": [
        Rule(
            '"RESOURCING" in all_unique_obs_states '
            "or "
            '["EMPTY", "IDLE"].to_set == all_unique_obs_states.to_set'
        ),
        Rule(
            '["IDLE"] == all_unique_obs_states '
            "and "
            "$any([command_result.is_empty or command_result in"
            ' ["FAILED", "UNKNOWN"] '
            "for command_result in all_unique_command_results])"
            " and "
            'command_in_progress == "AssignResources"'
        ),
    ],
    "IDLE": [
        Rule(
            '["IDLE"] == all_unique_obs_states '
            "and "
            "command_in_progress.is_empty ? true :"
            ' $all([["OK"] == all_unique_command_results, '
            'command_in_progress.as_lower in ["assignresources", "end"]])'
        )
    ],
    "CONFIGURING": [
        Rule(
            '"CONFIGURING" in all_unique_obs_states '
            "or "
            '["IDLE", "READY"].to_set == all_unique_obs_states.to_set'
        ),
        Rule(
            '["READY"] == all_unique_obs_states '
            "and "
            "$any([command_result.is_empty or command_result in "
            '["FAILED", "UNKNOWN"] '
            "for command_result in all_unique_command_results])"
            " and "
            'command_in_progress == "Configure"'
        ),
    ],
    "SCANNING": [Rule('"SCANNING" in all_unique_obs_states')],
    "ABORTED": [
        Rule(
            '["ABORTED"] == all_unique_obs_states '
            "or "
            '["ABORTED", "EMPTY"].to_set == all_unique_obs_states.to_set '
        )
    ],
    "RESTARTING": [Rule('"RESTARTING" in all_unique_obs_states')],
}

OBS_STATE_RULES_MID = OBS_STATE_RULES | {
    "MAPPING_SCAN": [  # This is internal Obs state of TMC.
        Rule(
            '["READY"] == all_unique_obs_states '
            "and "
            '"is_mapping_scan" in event_data ? '
            "event_data.is_mapping_scan == true : false"
        )
    ],
    "CONFIGURING": [
        Rule(
            '"CONFIGURING" in all_unique_obs_states '
            "or "
            '["IDLE", "READY"].to_set == all_unique_obs_states.to_set'
        ),
        Rule(
            '$any([pointing_state.is_empty or pointing_state=="READY" '
            "for pointing_state in all_unique_pointing_states]) "
            "and "
            '["READY"] == all_unique_obs_states'
        ),
        Rule(
            '["READY"] == all_unique_obs_states '
            "and "
            "$any([command_result.is_empty or command_result in "
            '["FAILED", "UNKNOWN"] '
            "for command_result in all_unique_command_results])"
            " and "
            'command_in_progress == "Configure"'
        ),
    ],
    "READY": [
        Rule(
            '["READY"] == all_unique_obs_states '
            "and "
            "$all([pointing_state in "
            '["TRACK", "SLEW"] '
            "for pointing_state in all_unique_pointing_states])"
            " and "
            '["OPERATE"] == all_unique_dish_modes'
            " and "
            "command_in_progress.is_empty ? true : "
            '$all([["OK"] == all_unique_command_results, '
            'command_in_progress.as_lower in ["configure", "endscan"]]) '
            "and "
            '"is_mapping_scan" in event_data ? '
            "event_data.is_mapping_scan == false : true"
        ),
        Rule(
            "is_partial_configuration == true "
            "and "
            '["OK"] == all_unique_command_results'
        ),
    ],
    "IDLE": [
        Rule(
            '["IDLE"] == all_unique_obs_states '
            "and "
            "command_in_progress.is_empty ? true :"
            ' $all([["OK"] == all_unique_command_results, '
            'command_in_progress.as_lower == "assignresources"])'
        ),
        Rule(
            '["IDLE"] == all_unique_obs_states '
            "and "
            "command_in_progress.is_empty ? true :"
            ' $all([["OK"] == all_unique_command_results, '
            '["READY"] == all_unique_pointing_states, '
            'command_in_progress.as_lower == "end"])'
        ),
    ],
    "ABORTED": [
        Rule(
            '$any([["ABORTED"] == all_unique_obs_states, '
            '["ABORTED", "EMPTY"].to_set == all_unique_obs_states.to_set]) '
            "and "
            '$any([["READY"] == all_unique_pointing_states, '
            '["READY", "NONE"].to_set == all_unique_pointing_states.to_set, '
            '["NONE"] == all_unique_pointing_states])'
        )
    ],
}

OBS_STATE_RULES_LOW = OBS_STATE_RULES | {
    "READY": [
        Rule(
            '["READY"] == all_unique_obs_states '
            "and "
            "command_in_progress.is_empty ? true : "
            '$all([["OK"] == all_unique_command_results, '
            'command_in_progress.as_lower in ["configure", "endscan"]])'
        )
    ]
}
