"""
This module provided a reference implementation
of a SubarrayComponentManager inheritted from BaseComponentManager.
"""
import json
import threading
import time
from datetime import datetime
from logging import Logger
from multiprocessing import Event
from multiprocessing import Lock as ProcessLock
from multiprocessing import Manager
from queue import Empty, Queue
from time import sleep
from typing import Any, Callable, Dict, List, Tuple, Union

import pandas as pd
import tango
from ska_control_model import ObsStateModel
from ska_control_model.faults import StateModelError
from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import AdminMode, HealthState, ObsState
from ska_tango_base.executor import TaskStatus
from ska_tango_base.subarray import SubarrayComponentManager
from ska_telmodel.schema import validate
from ska_tmc_common import (
    AdapterFactory,
    DeviceInfo,
    DishMode,
    LRCRCallback,
    PointingState,
    SdpSubarrayDeviceInfo,
    SubArrayDeviceInfo,
)
from ska_tmc_common.device_info import DishDeviceInfo
from ska_tmc_common.v1.liveliness_probe import MultiDeviceLivelinessProbe
from ska_tmc_common.v1.tmc_component_manager import BaseTmcComponentManager
from tango import DevState

from ska_tmc_subarraynode.commands.abort_command import Abort
from ska_tmc_subarraynode.commands.assign_resources_command import (
    AssignResources,
)
from ska_tmc_subarraynode.commands.configure_command import Configure
from ska_tmc_subarraynode.commands.end_command import End
from ska_tmc_subarraynode.commands.end_scan_command import EndScan
from ska_tmc_subarraynode.commands.off_command import Off
from ska_tmc_subarraynode.commands.on_command import On
from ska_tmc_subarraynode.commands.release_all_resources_command import (
    ReleaseAllResources,
)
from ska_tmc_subarraynode.commands.restart_command import Restart
from ska_tmc_subarraynode.commands.scan_command import Scan
from ska_tmc_subarraynode.commands.set_admin_mode_command import SetAdminMode
from ska_tmc_subarraynode.exceptions import CommandNotAllowed
from ska_tmc_subarraynode.input_validator import (
    ConfigureValidator,
    ScanValidator,
)
from ska_tmc_subarraynode.manager.event_data_manager import EventDataManager
from ska_tmc_subarraynode.manager.event_receiver import (
    SubarrayNodeEventReceiver,
)
from ska_tmc_subarraynode.model.component import SubarrayComponent
from ska_tmc_subarraynode.model.input import (
    InputParameterLow,
    InputParameterMid,
)
from ska_tmc_subarraynode.utils.constants import (
    NUMBER_OF_DEVICE_EVENTS_COUNT_MID,
    OSO_TMC_LOW_CONFIGURE_INTERFACE,
    OSO_TMC_LOW_SCAN_INTERFACE,
)
from ska_tmc_subarraynode.utils.subarray_node_utils import (
    split_interface_version,
)

SLEEP_TIME = 0.2
TIMEOUT = 80


# pylint: disable=too-many-instance-attributes
class SubarrayNodeComponentManager(
    BaseTmcComponentManager, SubarrayComponentManager
):
    """
    A component manager for The Subarray Node component.

    It supports:

    * Monitoring its component, e.g. detect that it has been turned off
      or on

    * Fetching the latest SCM indicator values and receiving change events
      of the components periodically and trigger the subarray health state and
      observation state aggregation
    """

    def __init__(
        self,
        op_state_model,
        obs_state_model: ObsStateModel,
        _command_tracker,
        _input_parameter,
        dev_family,
        logger: Logger,
        _component=None,
        _update_device_callback=None,
        _update_subarray_health_state_callback=None,
        _update_assigned_resources_callback=None,
        _update_subarray_availability_status_callback=None,
        _liveliness_probe=True,
        _event_receiver=True,
        communication_state_changed_callback=None,
        component_state_changed_callback=None,
        command_timeout: int = 30,
        abort_command_timeout: int = 40,
        proxy_timeout: int = 500,
        event_subscription_check_period=1,
        liveliness_check_period=1,
        csp_assign_interface: str = "",
        csp_scan_interface: str = "",
        sdp_scan_interface: str = "",
        jones_uri: str = "",
    ):
        """
        Initialise a new ComponentManager instance.

        :param op_state_model: the op state model used by this component
            manager
        :param logger: a logger for this component manager
        :param _component: allows setting of the component to be
            managed; for testing purposes only
        """
        super().__init__(
            logger=logger,
            communication_state_callback=communication_state_changed_callback,
            component_state_callback=component_state_changed_callback,
        )
        self.dev_family = dev_family
        self._unavailable_devices: List = []
        self.adapter_factory = AdapterFactory()
        self.op_state_model = op_state_model
        self.logger = logger
        self.obs_state_model = obs_state_model
        self.command_tracker = _command_tracker
        self.lock = threading._RLock()
        self.health_aggregation_lock = threading._RLock()
        self.availability_aggregation_lock = threading._RLock()
        self.attribute_lock = threading._RLock()
        self.input_parameter_lock = threading._RLock()
        self.component = _component or SubarrayComponent(logger)
        self._event_receiver_flag = _event_receiver
        self._liveliness_probe_flag = _liveliness_probe
        self.proxy_timeout = proxy_timeout
        self.event_subscription_check_period = event_subscription_check_period
        self.liveliness_check_period = liveliness_check_period
        self.command_timeout = command_timeout
        self.abort_command_timeout = abort_command_timeout
        self.scan_timer = None
        self.component_state_changed_callback = (
            component_state_changed_callback
        )
        self.dish_unique_ids: dict[str, str] = {}
        self.error_dict: dict = {}
        self.device_events: dict = {}
        self.abort_command = None
        self.is_operation_aborted_lock = threading._RLock()
        self._stop_thread: bool = False

        self.event_queues: Dict[str, Queue] = {
            "obsState": Queue(),
            "longRunningCommandResult": Queue(),
            "dishMode": Queue(),
            "pointingState": Queue(),
            "adminMode": Queue(),
        }
        self.event_processing_methods: Dict[
            str, Callable[[str, Any], None]
        ] = {
            "obsState": self.update_device_obs_state,
            "longRunningCommandResult": (
                self.update_long_running_command_result
            ),
            "dishMode": self.update_device_dish_mode,
            "pointingState": self.update_device_pointing_state,
            "adminMode": self.update_device_admin_mode,
        }
        self.liveliness_probe = None
        self.__start_event_processing_threads()
        self._stop = False
        self._subarray_availability_aggregator = None
        self.subarray_availability_thread = threading.Thread(
            target=self._subarray_availability_check
        )
        self._start_all_threads()
        self.component.set_op_callbacks(
            _update_device_callback,
            _update_subarray_health_state_callback,
            _update_subarray_availability_status_callback,
        )
        self.component.set_obs_callbacks(
            _update_assigned_resources_callback,
        )

        self._input_parameter = _input_parameter
        self._obs_state_aggregator = None
        self._subarray_health_state_aggregator = None

        self._command_in_progress = ""
        self.command_in_progress_id = None
        self.command_result_code = None
        self.last_executed_command = ""
        self._subarray_obsstate = ObsState.EMPTY
        self.long_running_result_callback = LRCRCallback(self.logger)
        self.supported_commands = (
            "AssignResources",
            "Configure",
            "ReleaseAllResources",
            "End",
            "EndScan",
            "TrackStop",
            "Scan",
        )
        self._is_partial_configuration: bool = False
        self.csp_scan_interface: str = csp_scan_interface
        self.sdp_scan_interface: str = sdp_scan_interface
        self.csp_assign_interface: str = csp_assign_interface
        self.event_data_manager = EventDataManager(self)
        self.process_lock = ProcessLock()
        self.command_timestamp = None
        self.jones_uri: str = jones_uri
        self.oso_tmc_interface: str = ""
        self.tmc_major: int = 0
        self.tmc_minor: int = 0
        self._is_mapping_scan: bool = False
        self.pattern_scan_data: dict = {}
        self.scan_obj = None
        # Aggregation Process
        self.aggregate_process_manager = Manager()
        self.event_data_queue = self.aggregate_process_manager.Queue()
        self.aggregated_obs_state = self.aggregate_process_manager.list([""])
        self.tmc_internal_obs_state = None
        self.aggregate_value_update_event = Event()
        self.aggregate_process_monitor_thread = threading.Thread(
            target=self.aggregate_process_monitor
        )
        self.aggregate_process_monitor_thread.start()

        self.all_lrcr_ok = False
        self.mapping_scan_completed = False
        self.is_admin_mode_enabled = False
        self.abort_event = threading.Event()

    @property
    def is_mapping_scan(self) -> bool:
        """Return if current scan is mapping scan"""
        return self._is_mapping_scan

    @is_mapping_scan.setter
    def is_mapping_scan(self, value: bool):
        self._is_mapping_scan = value

    def aggregate_process_monitor(self):
        """This method keep tracking aggregate obs state changed
        from aggregation process
        """
        while not self._stop_thread:
            if self.aggregate_value_update_event.is_set():
                self.aggregate_value_update_event.clear()
                current_obs_state = self.aggregated_obs_state[0]
                self.logger.debug(
                    "Aggregate obs state called %s and "
                    "command in progress %s, subarray current obs state: %s",
                    current_obs_state,
                    self.command_in_progress,
                    self.obs_state_model.obs_state,
                )
                if current_obs_state == "MAPPING_SCAN":
                    self.logger.debug("Invoking Next Mapping Scan")
                    self.handle_mapping_scan_callback()
                elif current_obs_state is not None and (
                    current_obs_state != self.obs_state_model.obs_state
                ):
                    self._set_subarraynode_obsstate(
                        current_obs_state, self.command_in_progress
                    )

            time.sleep(0.1)
        self.logger.debug("aggregation process monitor thread stopped")

    def stop(self):
        """Stopping the liveliness probe"""
        self.stop_liveliness_probe()
        self.stop_event_receiver()
        self._stop_thread = True

    def _start_all_threads(self):
        """This method start
        1. Liveliness probe
        2. Event receiver
        3. subarray availability thread
        """
        try:
            if not self.subarray_availability_thread.is_alive():
                self.subarray_availability_thread.start()
        except Exception as exception:
            self.logger.debug(
                "Exception occurred while starting the subarray"
                + f"availability check thread: {exception}"
            )

        if self._liveliness_probe_flag:
            self.liveliness_probe = MultiDeviceLivelinessProbe(
                self,
                logger=self.logger,
                proxy_timeout=self.proxy_timeout,
                liveliness_check_period=self.liveliness_check_period,
            )
            try:
                self.liveliness_probe.start()
            except Exception as exception:
                self.logger.debug(
                    "Exception occurred while starting the "
                    + f"liveliness probe thread: {exception}"
                )

        if self._event_receiver_flag:
            evt_sub_check_period = self.event_subscription_check_period
            self.event_receiver = SubarrayNodeEventReceiver(
                self,
                self.logger,
                max_workers=1,
                proxy_timeout=self.proxy_timeout,
                event_subscription_check_period=evt_sub_check_period,
            )
            try:
                self.event_receiver.start()
            except RuntimeError as exception:
                self.logger.debug(
                    "Exception occurred while starting the "
                    + f"event receiver thread: {exception}"
                )
            except Exception as exception:
                self.logger.debug(
                    "Exception occurred while starting the "
                    + f"event receiver thread: {exception}"
                )

    @property
    def command_in_progress(self) -> str:
        """Returns the command_in_progress variable
        :return: command in progress
        :rtype: str
        """
        return self._command_in_progress

    @command_in_progress.setter
    def command_in_progress(self, value: str):
        """
        Sets the command_in_progress variable
        :param value: value set by setter
        :type value: str
        """
        self._command_in_progress = value

    @property
    def subarray_obsstate(self):
        """Keeps track of aggregated subarray ObsState"""
        return self._subarray_obsstate

    @subarray_obsstate.setter
    def subarray_obsstate(self, obsstate: ObsState):
        """Set the aggregated subarray ObsState"""
        self._subarray_obsstate = obsstate

    @property
    def is_operation_aborted(self):
        """Aborted Flag"""
        with self.is_operation_aborted_lock:
            return self.abort_command.is_operation_aborted

    @is_operation_aborted.setter
    def is_operation_aborted(self, is_operation_aborted: bool):
        """Set the is operation aborted flag"""
        with self.is_operation_aborted_lock:
            self.abort_command.is_operation_aborted = is_operation_aborted

    def get_subarray_obsstate(self) -> ObsState:
        """Returns aggregated subarray ObsState"""
        return self.obs_state_model.obs_state

    @property
    def assigned_resources(self) -> List[str]:
        """
        Return the resources assigned to the component.

        :return: the resources assigned to the component
        :rtype: list of str
        """
        return self.component.assigned_resources

    @property
    def devices(self) -> List:
        """
        Return the list of the monitored devices

        :return: list of the monitored devices
        """
        return self.component.devices

    @property
    def input_parameter(self) -> Union[InputParameterLow, InputParameterMid]:
        """
        Return the input parameter

        :return: input parameter
        :rtype: InputParameter
        """
        return self._input_parameter

    @property
    def checked_devices(self):
        """
        Return the list of the checked monitored devices

        :return: list of the checked monitored devices
        """
        return self.component.devices

    @property
    def is_partial_configuration(self) -> bool:
        """Returns True if the configuration is partial."""
        return self._is_partial_configuration

    @is_partial_configuration.setter
    def is_partial_configuration(self, value: bool) -> None:
        """Sets the partial configuration flag to the given value.

        :param value: (bool) Boolean value

        :return: None
        """
        self._is_partial_configuration = value

    @property
    def unavailable_devices(self) -> List:
        """Returns List of the unavailable_devices."""
        return self._unavailable_devices

    @unavailable_devices.setter
    def unavailable_devices(self, value: List) -> None:
        """Sets the _unavailable_device list.

        :param value: (list) List

        :return: None
        """
        self._unavailable_devices = value

    def add_multiple_devices(self, device_list: List) -> List:
        """
        Add multiple devices to the liveliness probe

        :param device_list: list of device names
        :type list: list[str]
        """
        result = []
        for device_name in device_list:
            self.add_device_to_lp(device_name)
            result.append(device_name)
        return result

    def add_device_to_lp(self, device_name: str) -> None:
        """
        Add device to the liveliness probe

        :param device_name: device name
        :type device_name: str
        """
        raise NotImplementedError("abstract add_device_to_lp method")

    def remove_devices_from_lp(self, device_names: List[str]) -> None:
        """Remove the given devices from liveliness probe.

        :param device_names: Device names for devices to be removed from
            Liveliness Probe
        :type device_names: `List[str]`
        """
        if self.liveliness_probe:
            self.liveliness_probe.remove_devices(device_names)

    def add_similar_low_mid_device(self, device_name: str) -> None:
        """
        Add Similar Low and Mid device to the liveliness probe

        :param device_name: device name
        :type device_name: str
        """
        dev_info = None
        if device_name is None:
            return
        if ("subarray" in device_name.lower()) and (
            "leaf" not in device_name.lower()
        ):
            if "sdp" in device_name.lower():
                dev_info = SdpSubarrayDeviceInfo(device_name, False)
            if "csp" in device_name.lower():
                dev_info = SubArrayDeviceInfo(device_name, False)

        if device_name.lower() in (
            self.input_parameter.tmc_csp_sln_device_name.lower(),
            self.input_parameter.tmc_sdp_sln_device_name.lower(),
        ):
            dev_info = SubArrayDeviceInfo(device_name, False)

        if dev_info:
            self.component.update_device(dev_info)
            if self.liveliness_probe:
                self.liveliness_probe.add_device(device_name)

    def get_device(self, device_name: str) -> DeviceInfo:
        """
        Return the device info with device name device_name

        :param device_name: name of the device
        :type device_name: str
        :return: a device info
        :rtype: DeviceInfo
        """
        return self.component.get_device(device_name)

    def update_input_parameter(self) -> None:
        """Updates the input parameter of a class instance with
        the current state of the instance.
        """
        with self.input_parameter_lock:
            self.logger.debug("Updating input parameter")
            self.input_parameter.update(self)

    def get_mccs_subarray_dev_name(self) -> str:
        """
        Returns Mccs Subarray device name
        """
        return self.input_parameter.mccs_subarray_dev_name

    def get_sdp_subarray_dev_name(self) -> str:
        """
        Return Sdp Subarray device name
        """
        return self.input_parameter.sdp_subarray_dev_name

    def get_csp_subarray_dev_name(self) -> str:
        """
        Return Csp Subarray device name
        """
        return self.input_parameter.csp_subarray_dev_name

    def get_tmc_csp_sln_device_name(self) -> str:
        """
        Return Csp Subarray Leaf Node device name
        """
        return self.input_parameter.tmc_csp_sln_device_name

    def get_tmc_sdp_sln_device_name(self) -> str:
        """
        Return Sdp Subarray Leaf Node device name
        """
        return self.input_parameter.tmc_sdp_sln_device_name

    def get_tmc_mccs_sln_device_name(self) -> str:
        """
        Return Mccs Subarray Leaf Node device name
        """
        return self.input_parameter.tmc_mccs_sln_device_name

    def _subarray_availability_check(self) -> None:
        """
        A method to submit aggregation call for
        subarray availability attribute
        """
        with tango.EnsureOmniThread():
            while not self._stop:
                try:
                    self._aggregate_subarray_device_availability()
                except Exception as exception:
                    self.logger.exception(
                        "Exception occurred while performing availability "
                        + "check: %s",
                        exception,
                    )
                sleep(self.liveliness_check_period)

    def update_subarray_availability_status(
        self, device_name: str, availability_status: bool
    ) -> None:
        """
        Update a monitored device availability status
        aggregate the subarray availability

        :param device_name: name of the device
        :type device_name: str
        :param availability_status: availability status of the device
        :type availability_status: isSubarrayAvailable
        """
        with tango.EnsureOmniThread():
            dev_info = self.get_device(device_name)
            if dev_info:
                if dev_info.device_availability != availability_status:
                    self.logger.debug(
                        "Updating the availability status for %s to %s",
                        device_name,
                        availability_status,
                    )
                dev_info.device_availability = availability_status
                dev_info.last_event_arrived = time.time()
                dev_info.update_unresponsive(False)
                self.component.invoke_device_callback(dev_info)
                self._aggregate_subarray_device_availability()

    def update_device_health_state(
        self, device_name: str, health_state: HealthState
    ) -> None:
        """
        Update a monitored device health state
        aggregate the health states available

        :param device_name: name of the device
        :type device_name: str
        :param health_state: health state of the device
        :type health_state: HealthState
        """
        self.logger.debug(
            f"healthState event for {device_name}: "
            + f"{HealthState(health_state).name}"
        )
        dev_info = self.get_device(device_name)
        if dev_info is not None:
            dev_info.health_state = health_state
            self.logger.debug(
                "Updated healthState of %s: %s",
                dev_info.dev_name,
                HealthState(dev_info.health_state).name,
            )
            dev_info.last_event_arrived = time.time()
            dev_info.update_unresponsive(False)
            self.component.invoke_device_callback(dev_info)
        self._aggregate_health_state()

    def update_device_dish_mode(
        self, device_name: str, dish_mode: DishMode, timestamp: datetime
    ):
        """
        Update the dish mode for given device.

        :param device_name: name of the device
        :type device_name: str
        :param dish_mode: dish mode of the device
        :type dish_mode: DishMode
        """
        self.logger.debug(
            f"Received dishMode event from {device_name}: "
            + f"{DishMode(dish_mode).name}"
        )
        device_info = self.get_device(device_name)
        if device_info is not None:
            device_info.last_event_arrived = time.time()
            device_info.update_unresponsive(False)
            if device_info.dish_mode != dish_mode:
                device_info.dish_mode = dish_mode
                self.logger.debug(
                    "Command in progress is: %s with ID: %s",
                    self.command_in_progress,
                    self.command_in_progress_id,
                )

                self.event_data_manager.update_event_data(
                    device=device_name,
                    data=dish_mode,
                    received_timestamp=timestamp,
                    data_type="DishMode",
                )
            self.component.invoke_device_callback(device_info)

    def update_device_pointing_state(
        self,
        device_name: str,
        pointing_state: PointingState,
        timestamp: datetime,
    ):
        """
        Update a monitored device pointing state
        aggregate the Subarray obs states and Dish pointing states

        :param device_name: name of the device
        :type device_name: str
        :param pointing_state: pointing state of the device
        :type pointing_state: PointingState
        """
        self.logger.debug(
            f"pointingState event for {device_name}: "
            + f"{PointingState(pointing_state).name}"
        )

        self.event_data_manager.update_event_data(
            device=device_name,
            data=pointing_state,
            received_timestamp=timestamp,
            data_type="PointingState",
        )
        device_info = self.get_device(device_name)
        if device_info is not None:
            device_info.last_event_arrived = time.time()
            device_info.update_unresponsive(False)
            if device_info.pointing_state != pointing_state:
                device_info.pointing_state = pointing_state
                self.logger.debug(
                    "Command in progress is: %s with ID: %s",
                    self.command_in_progress,
                    self.command_in_progress_id,
                )
            self.component.invoke_device_callback(device_info)

    def update_device_admin_mode(
        self,
        device_name: str,
        admin_mode: AdminMode,
        timestamp: datetime,
    ):
        """
        Update a monitored device admin mode

        :param device_name: name of the device
        :type device_name: str
        :param admin_mode: admin mode of the device
        :type admin_mode: AdminMode
        """
        self.logger.debug(
            "admin mode event for %s: %s", device_name, admin_mode
        )
        device_info = self.get_device(device_name)
        if device_info is not None:
            device_info.last_event_arrived = time.time()
            device_info.update_unresponsive(False)
            if device_info.admin_mode != admin_mode:
                device_info.admin_mode = admin_mode
                self.logger.debug(
                    "Command in progress is: %s with id: %s",
                    self.command_in_progress,
                    self.command_in_progress_id,
                )
                self._aggregate_health_state()

    def update_receive_addresses(
        self, device_name: str, receive_addresses: str
    ) -> None:
        """
        Update receiveAddresses for a monitored device

        :param device_name: name of the device
        :type device_name: str
        :param receive_addresses: receiveAddresses
        :type receive_addresses: str
        """
        sdp_subarray_dev_name = self.get_sdp_subarray_dev_name()
        if device_name in sdp_subarray_dev_name:
            device_name = sdp_subarray_dev_name
        dev_info = self.get_device(device_name)
        if dev_info is not None:
            dev_info.receive_addresses = receive_addresses
            dev_info.last_event_arrived = time.time()
            dev_info.update_unresponsive(False)
            self.component.invoke_device_callback(dev_info)

    def update_assigned_resources(
        self, device_name: str, assigned_resources: str
    ):
        """
        Update assignedResources for a monitored device

        :param device_name: name of the device
        :type device_name: str
        :param assigned_resources: assignedResources
        :type assigned_resources: str
        """
        dev_info = self.get_device(device_name)
        if isinstance(self.input_parameter, InputParameterLow):
            # MCCS Subarray assignedResources is of string type.
            # Set the SubarrayNode Low assigned_resources
            # with MCCS Subarray resources
            # from MCCS Subarray deviceInfo
            dev_info.resources = [assigned_resources]
            self.component.assigned_resources = dev_info.resources
        else:
            dev_info.resources = assigned_resources

        dev_info.last_event_arrived = time.time()
        dev_info.update_unresponsive(False)
        self.component.invoke_device_callback(dev_info)

    def check_event_error(self, event: tango.EventData, callback: str):
        """Method for checking event error."""
        if event.err:
            error = event.errors[0]
            self.logger.error(
                "Error occurred on %s for device: %s - %s, %s",
                callback,
                event.device.dev_name(),
                error.reason,
                error.desc,
            )
            self.update_event_failure(event.device.dev_name())
            return True
        return False

    def update_device_obs_state(
        self, device_name: str, obs_state: ObsState, timestamp: datetime
    ):
        """
        Update a monitored device obs state,
        and call the relative callbacks if available

        :param device_name: name of the device
        :type device_name: str
        :param obs_state: obs state of the device
        :type obs_state: ObsState
        """
        self.logger.debug(
            f"obsState event for {device_name}: {ObsState(obs_state).name}"
        )
        device_info = self.get_device(device_name)
        if device_info is not None:
            device_info.last_event_arrived = time.time()
            device_info.update_unresponsive(False)

            self.event_data_manager.update_event_data(
                device=device_name,
                data=obs_state,
                received_timestamp=timestamp,
                data_type="ObsState",
            )
            if device_info.obs_state != obs_state:
                device_info.obs_state = obs_state
                self.logger.debug(
                    "Command in progress is: %s with ID: %s",
                    self.command_in_progress,
                    self.command_in_progress_id,
                )

            self.component.invoke_device_callback(device_info)

    def handle_mapping_scan_callback(self, **kwargs):
        """This method is specific to mid"""
        return

    def _aggregate_health_state(self) -> None:
        """
        Aggregates all health states and call the relative callback if
            available
        """

        with self.health_aggregation_lock:
            self.component.subarray_health_state = (
                self._subarray_health_state_aggregator.aggregate()
            )
            self.logger.debug(
                "SubarrayNode aggregated healthState: "
                + f"{HealthState(self.component.subarray_health_state).name}"
            )

    def _aggregate_subarray_device_availability(self) -> None:
        """
        Aggregates subarrays availability
        and call the relative callback if available
        """
        with self.availability_aggregation_lock:
            (
                aggregated_subarray_availability,
                self.unavailable_devices,
            ) = self._subarray_availability_aggregator.aggregate()
            if (
                self.component.subarray_availability
                != aggregated_subarray_availability
            ):
                self.component.subarray_availability = (
                    aggregated_subarray_availability
                )
                self.logger.debug(
                    "Aggregated availability status "
                    + f"on subarray is {self.component.subarray_availability} "
                    + f"and unavailable devices {self.unavailable_devices}"
                )

    def get_subarray_healthstate(self) -> HealthState:
        """
        Returns value of subarray's health state.
        """
        return self.component.subarray_health_state

    def get_subarray_availability(self) -> bool:
        """
        Returns subarray availability status
        """
        return self.component.subarray_availability

    def check_for_incremental_assign_and_revise_obsstate(
        self,
        command_in_progress_id: str,
        device_events: dict,
        error_dict: dict,
    ) -> ObsState:
        """
        This method checks for incremental assign resources and evaluates final
        obsstate
        :param command_in_progress_id: command in progress id
        :type command_in_progress_id: str
        :param device_events: events for each device
        :type device_events: dict
        :param error_dict: error dict
        :type error_dict: dict
        :return: ObsState
        :rtype: ObsState
        """
        # Check if aggregation is triggred by command execution and both
        # csp and sdp longRunningCommandResult events received with
        # ResultCode.OK
        # TODO :: We need to revisit aggragation logic in near future
        # TMC currently uses aggragation logic to decide final Obsstate .
        # But then based on exceptional cases , TMC is not considering it
        # as final state and performing revisions.
        # This might needs to be revisited

        if (
            command_in_progress_id
            and (
                len(device_events.get(command_in_progress_id, []))
                == NUMBER_OF_DEVICE_EVENTS_COUNT_MID
            )
            and not error_dict.get(command_in_progress_id)
        ):
            self.logger.info(
                "Returning to obsState IDLE from Incremental Assign logic"
            )
            return ObsState.IDLE

        return ObsState.RESOURCING

    def check_successive_configure_revise_obsstate(
        self,
        command_in_progress_id: str,
        device_events: dict,
        error_dict: dict,
    ) -> ObsState:
        """
        This method checks for successive configure resources and
        evaluates final obsstate
        :param command_in_progress_id: command in progress id
        :type command_in_progress_id: str
        :param device_events: events for each device
        :type device_events: dict
        :param error_dict: error dict
        :type error_dict: dict
        :return: ObsState
        :rtype: ObsState
        """
        # Check if aggregation is triggred by command execution and both
        # csp and sdp longRunningCommandResult events received with
        # ResultCode.OK
        # TODO :: We need to revisit aggragation logic in near future
        # TMC currently uses aggragation logic to decide final Obsstate .
        # But then based on exceptional cases , TMC is not considering it
        # as final state and performing revisions.
        # This might needs to be revisited

        if (
            command_in_progress_id
            and (
                len(device_events.get(command_in_progress_id, []))
                == NUMBER_OF_DEVICE_EVENTS_COUNT_MID
            )
            and not error_dict.get(command_in_progress_id)
        ):
            self.logger.info(
                "Returning to obsState READY from successive configure logic"
            )
            return ObsState.READY

        return ObsState.CONFIGURING

    def _set_subarraynode_obsstate(
        self,
        aggregated_obsstate: ObsState,
        command_in_progress: str,
        abort_timeout: bool = False,
    ) -> None:
        """
        This method sets subarraynode obsstate

        :param aggregated_obsstate: obsstate aggregated
            from subsystem obsstate values.
        :type: ObsState
        :param command_in_progress: name of command in progress
        :type: str or None
        :param command_result_code: contains resultcode
            of the command in progress
        :type: ResultCode or None
        """
        # If command_in_progress fails with ResultCode.FAILED,
        # how should the aggregated obsState behave?
        set_obstate = {
            ObsState.EMPTY: {
                "ReleaseAllResources": [
                    (
                        "ReleaseAllResources COMPLETED",
                        {"release_completed": None},
                    )
                ],
                "Restart": [
                    ("Restart COMPLETED", {"restart_completed": None})
                ],
                "": {
                    "RESOURCING_IDLE": {"resourced": False},
                    "RESOURCING_EMPTY": {"release_completed": None},
                    "RESTARTING": {"restart_completed": None},
                },
            },
            ObsState.IDLE: {
                "AssignResources": [
                    ("AssignResources COMPLETED", {"assign_completed": None})
                ],
                "End": [("End COMPLETED", {"configured": False})],
                "ObsReset": [
                    ("ObsReset COMPLETED", {"obsreset_completed": None})
                ],
                "": {
                    "CONFIGURING_IDLE": {"configure_completed": None},
                },
            },
            ObsState.READY: {
                "Configure": [
                    ("component configured", {"configured": True}),
                    ("CONFIGURE COMPLETED", {"configure_completed": None}),
                ],
                "EndScan": [("SCAN COMPLETED", {"scanning": False})],
                "Scan": [("SCAN COMPLETED", {"scanning": False})],
            },
            ObsState.ABORTED: {
                "Abort": [("Abort COMPLETED", {"abort_completed": None})],
                "": {
                    "ABORTING": {"abort_completed": None},
                },
            },
            ObsState.ABORTING: {
                "Abort": [("Abort TIMEOUT", {"component_obsfault": None})]
            },
        }
        transitional_obsstate = [
            ObsState.RESOURCING,
            ObsState.RESTARTING,
            ObsState.ABORTING,
            ObsState.CONFIGURING,
            ObsState.RESETTING,
            ObsState.SCANNING,
        ]
        final_obsstates = [
            ObsState.EMPTY,
            ObsState.IDLE,
            ObsState.READY,
            ObsState.ABORTED,
        ]
        # if command_result_code not in [ResultCode.FAILED]:
        if command_in_progress:
            if (
                self.obs_state_model.obs_state in transitional_obsstate
                and aggregated_obsstate not in final_obsstates
            ):
                # If a command is in progress, and the aggregated ObsState
                # is not a final ObsState, we use the ObsState from the
                # ObsStateModel as it is the actual Subarray Node ObsState.
                self.logger.debug(
                    "Updating the Subarray Node ObsState from the "
                    + "ObsStateModel as %s",
                    self.obs_state_model.obs_state,
                )
                self.subarray_obsstate = self.obs_state_model.obs_state
                if (
                    aggregated_obsstate == ObsState.ABORTING
                    and abort_timeout is True
                ):
                    self.logger.info("Abort command TIMEOUT occurred")
                    callback_action = set_obstate.get(aggregated_obsstate).get(
                        command_in_progress
                    )
                    self.logger.info(
                        "Abort TIMEOUT: Invoking callback_action: %s",
                        callback_action,
                    )
                    if callback_action:
                        self.component_state_changed_callback_and_logging(
                            callback_action,
                            command_in_progress,
                            aggregated_obsstate,
                        )
            else:
                self.logger.info(
                    "Updating the SubarrayNode's obsState from aggregation"
                    + " logic as %s",
                    aggregated_obsstate,
                )
                self.subarray_obsstate = aggregated_obsstate

        if aggregated_obsstate in transitional_obsstate:
            return

        if set_obstate.get(aggregated_obsstate):
            # Check if it is one of the initial events of AssignResources
            # command
            if aggregated_obsstate == ObsState.EMPTY and (
                command_in_progress == "AssignResources"
            ):
                self.logger.debug("AssignResources is not yet complete")
            else:
                callback_action = set_obstate.get(aggregated_obsstate).get(
                    command_in_progress
                )
                self.logger.info(
                    "Invoking callback_action :: %s", callback_action
                )

                if callback_action:
                    self.component_state_changed_callback_and_logging(
                        callback_action,
                        command_in_progress,
                        aggregated_obsstate,
                    )

    def component_state_changed_callback_and_logging(
        self,
        callback_action: list | Dict,
        command_in_progress: str | None,
        aggregated_obsstate: ObsState,
    ) -> None:
        """
        Method calls component_state_changed_callback to complete
        aggregate obsState change and logs completion message.

        :param callback_action: contains details required by component
         state changed callback and log statement.
        :type: dict or list
        :param command_in_progress: name of command in progress
        :type: str or None
        :param aggregated_obsstate: obsstate aggregated from
            subsystem obsstate values.
        :type: ObsState
        """
        # clearing the attributes once the Tmc SubarrayNode ObsState is updated
        self.logger.info(
            "ObsState model is %s before performing action: %s",
            self.obs_state_model.obs_state,
            list(callback_action),
        )
        if command_in_progress:
            if command_in_progress.lower() in (
                "end",
                "restart",
                "scan",
                "endscan",
            ):
                with self.event_data_manager.eventlock:
                    self.event_data_manager.clear_lrcr()

            if command_in_progress.lower() in ("restart",):
                self.event_data_manager.clear_dish_data()
            self.command_in_progress = ""
            self.command_in_progress_id = None
            self.command_result_code = None
            for action in callback_action:
                self.component_state_changed_callback(action[1])
                self.logger.info(
                    "Action performed: %s, obsState is: %s",
                    action,
                    self.obs_state_model.obs_state,
                )

            self.logger.info(
                "Aggregation process complete for: %s", command_in_progress
            )
        else:
            intermitent_state = self.obs_state_model._obs_state_machine.state
            self.logger.debug(
                "SubarrayNode's current obsState in obs state machine is: "
                + "%s",
                intermitent_state,
            )
            if intermitent_state == "FAULT":
                return

            if callback_action.get(intermitent_state):
                # Update Dish Leaf Node and Dish Master
                # devices from Input Parameter
                self.subarray_obsstate = aggregated_obsstate
                if intermitent_state == "RESOURCING_IDLE":
                    self.component_state_changed_callback(
                        callback_action.get(intermitent_state)
                    )
                    intermitent_state = (
                        self.obs_state_model._obs_state_machine.state
                    )
                self.component_state_changed_callback(
                    callback_action.get(intermitent_state)
                )
                aggregate_obsstate = (
                    self.obs_state_model._obs_state_machine.state
                )
                self.logger.info(
                    "Aggregation process complete, aggregated obsState is: %s",
                    aggregate_obsstate,
                )
                if aggregate_obsstate == "EMPTY":
                    self.clear_assigned_resources()
        self.observable.notify_observers(attribute_value_change=True)

    def set_sb_id(self, sb_id):
        """
        Sets sb_id value.

        :param sb_id: value to set
        :type: float
        """
        with self.attribute_lock:
            self.component.sb_id = sb_id

    def reset_sb_id(self) -> None:
        """
        Resets sb_id value.
        """
        with self.attribute_lock:
            self.component.sb_id = ""

    def get_sb_id(self) -> float:
        """
        Returns sb_id value.
        """
        return self.component.sb_id

    def set_subarray_id(self, subarray_id: int) -> None:
        """
        Sets subarray_id value.

        :param sb_id: value to set
        :type: int
        """
        with self.attribute_lock:
            self.component.subarray_id = subarray_id

    def reset_subarray_id(self) -> None:
        """
        Resets subarray_id value.
        """
        with self.attribute_lock:
            self.component.subarray_id = None

    def get_subarray_id(self) -> int:
        """
        Returns subarray_id value.
        """
        return self.component.subarray_id

    def set_scan_id(self, scan_id: float) -> None:
        """
        Sets scan_id value.

        :param scan_id: value to set
        :type: float
        """
        with self.attribute_lock:
            self.component.scan_id = scan_id

    def reset_scan_id(self) -> None:
        """
        Resets scan_id value.
        """
        self.logger.info(f"The scan_id to reset is: {self.component.scan_id}")
        with self.attribute_lock:
            self.component.scan_id = ""

    def get_scan_id(self):
        """
        Returns scan_id value.
        """
        return self.component.scan_id

    def set_scan_duration(self, scan_duration):
        """
        Sets scan_duration value.

        :param scan_duration: value to set
        :type: float
        """
        with self.attribute_lock:
            self.component.scan_duration = scan_duration

    def reset_scan_duration(self):
        """
        Reset scan_duration value.
        """
        with self.attribute_lock:
            self.component.scan_duration = 0.0

    def get_scan_duration(self):
        """
        Returns scan_duration value.
        """
        return self.component.scan_duration

    def stop_scan_timer(self):
        """
        Stops scan_timer thread.
        """
        if self.scan_timer:
            self.scan_timer.cancel()

    # pylint: disable=inconsistent-return-statements
    def is_scan_timer_running(self):
        """
        Checks if the scan_timer thread is alive.
        """
        if self.scan_timer:
            return self.scan_timer.is_alive()

    # pylint: enable=inconsistent-return-statements

    def set_assigned_resources(self, resources):
        """
        For SubarrayNode Mid, set assigned_resources
        with the list of dishes in argin.

        :param resources: name of the dish devices
        :type list: list[str]

        """
        self.logger.debug("Setting the resources to be assigned")
        with self.attribute_lock:
            if isinstance(self.input_parameter, InputParameterMid):
                # Set the SubarrayNode Mid assigned_resources
                # with input dishes list
                if len(resources) > 0:
                    resources_to_be_allocated = []
                    resources_allocated = self.get_assigned_resources()
                    # Check if resources are already allocated
                    for receptor in resources:
                        if receptor not in resources_allocated:
                            resources_to_be_allocated.append(receptor)
                        else:
                            continue

                    # assigned_resources variable is
                    # introduced because component
                    # setter method for assigned_resources does
                    # not get called when this component
                    # property is updated using extend method.
                    assigned_resources = self.component.assigned_resources
                    assigned_resources.extend(resources_to_be_allocated)
                    self.component.assigned_resources = assigned_resources
                else:
                    self.component.assigned_resources = []
                    self.logger.info(
                        f"self.assigned_resources: \
                        {self.component.assigned_resources}"
                    )
            else:
                self.component.assigned_resources = resources
        self.logger.info(
            f"Subarray assigned resources: {self.component.assigned_resources}"
        )

    def unsubscribe_dish_events(self):
        """
        Unsubscribes the events for Dishleafnode and Dishmaster
        """
        self.logger.info("Unsubscribing dish events")
        # Unsubscribe state, healthState, PointingState and dishMode
        # events of all dishes assigned to Subarray
        self.event_receiver.unsubscribe_dish_events()
        # Unsubscribe state and healthState events of
        # all Dish Leaf Nodes assigned to Subarray
        self.logger.info("Unsubscribing dish leaf node events")
        self.event_receiver.unsubscribe_dish_leaf_events()
        self.logger.info("Unsubscribed dish device events")

    def get_assigned_resources(self):
        """
        :return: assigned_resources
        :rtype: list[str]
        """
        return self.assigned_resources

    def generate_command_result(self, result_code: int, message: str) -> tuple:
        """
        Method for generating command result
        :param result_code : send result_code
        :type result_code: int
        :param message: message of invoking command
        """
        if result_code == ResultCode.FAILED:
            self.logger.error(message)
        self.logger.info(message)
        return result_code, message

    def reset(self):
        """Method for resetting Not implemented"""

    def update_exception_for_unresponsiveness(
        self, device_info: DeviceInfo, exception: str
    ) -> None:
        """
        Set a device to failed and call the relative callback if available

        :param device_info: a device info
        :type device_info: DeviceInfo
        :param exception: an exception
        :type: Exception
        """
        message = f"Failed to ping device {device_info.dev_name}: {exception}"
        self.logger.error(message)
        self.component.update_device_exception(device_info, exception)

    def update_event_failure(self, device_name: str) -> None:
        """
        Update the event failures if any for the given Device.

        :param device_name: name of the device
        :type device_name: str
        """
        dev_info = self.get_device(device_name)
        if dev_info:
            dev_info.last_event_arrived = time.time()
            dev_info.update_unresponsive(False)
            self.component.invoke_device_callback(dev_info)

    def update_device_state(self, device_name: str, state: DevState) -> None:
        """
        Update a monitored device state,
        aggregate the states available
        and call the relative callbacks if available

        :param device_name: name of the device
        :type device_name: str
        :param state: state of the device
        :type state: DevState
        """
        self.logger.debug(f"State event for {device_name}: {state}")
        dev_info = self.get_device(device_name)
        if dev_info is not None:
            dev_info.state = state
            self.logger.debug(
                f"Updated State of {dev_info.dev_name}: {dev_info.state}"
            )
            dev_info.last_event_arrived = time.time()
            dev_info.update_unresponsive(False)
            self.component.invoke_device_callback(dev_info)

    def check_command_not_allowed_exception(
        self, op_state, states_not_allowed, cmd_name: str
    ):
        """
        This method checks command not allowed exception
        """
        if op_state in states_not_allowed:
            raise CommandNotAllowed(
                f"{cmd_name} is not allowed in current state {op_state}"
            )

    def check_subarray_device_availability(self) -> None:
        """
        Checks if the subarray device availability is True if not,
        raises exception CommandNotAllowed.
        """
        is_subarray_available = self.get_subarray_availability()
        self.logger.info(
            "Subarray availability attribute "
            + f"value is: {is_subarray_available}"
        )
        if self.unavailable_devices and not is_subarray_available:
            raise CommandNotAllowed(
                "Command not allowed due to unavailabity of "
                + f"following devices {self.unavailable_devices}"
            )

    def check_if_sdp_sln_is_available(self) -> bool:
        """
        Check if the sdp subarray device availability is True.
        """
        device_info = self.get_device(self.get_tmc_sdp_sln_device_name())
        if not (
            device_info.device_availability is True
            and device_info.unresponsive is False
        ):
            self.logger.info(
                "%s is not available with it's device availability: %s and "
                + "unresponsive value: %s",
                self.get_tmc_sdp_sln_device_name(),
                device_info.device_availability,
                device_info.unresponsive,
            )
            return False
        return True

    def stop_liveliness_probe(self) -> None:
        """Stops the liveliness probe"""
        self.liveliness_probe.stop()

    def stop_event_receiver(self) -> None:
        """Stops the event receiver"""
        self.event_receiver.stop()

    def update_responsiveness_info(self, device_name: str) -> None:
        """
        Update a device with correct ping information.

        :param device_name: name of the device
        :type device_name: str
        :param ping: device response time
        :type ping: int
        """
        dev_info = self.get_device(device_name)
        dev_info.update_unresponsive(False, "")

    def is_command_allowed(self, command_name: str) -> bool:
        """
        Checks whether this command is allowed
        It checks that the device is in a state
        to perform this command and that all the
        component needed for the operation are not unresponsive

        :param command_name: name of the command
        :type command_name: str
        :return: True if this command is allowed, False otherwise
        :rtype: boolean
        """
        # if health state is degraded then reject the command
        if (
            self.is_admin_mode_enabled
            and self.component.subarray_health_state == HealthState.DEGRADED
        ):
            self.logger.info(
                "Command is not allowed when HealthState is Degraded"
            )
            raise CommandNotAllowed(
                "Command is not allowed in DEGRADED Health State"
            )

        if command_name in [
            "On",
            "Off",
            "Standby",
            "Abort",
            "Restart",
        ]:
            if self.op_state_model.op_state in [
                DevState.FAULT,
                DevState.UNKNOWN,
                DevState.DISABLE,
            ]:
                return False
        elif command_name in [
            "AssignResources",
            "ReleaseAllResources",
            "Configure",
            "End",
            "Scan",
            "EndScan",
            "Abort",
            "Restart",
            "SetAdminMode",
        ]:
            if self.op_state_model.op_state in [
                DevState.FAULT,
                DevState.UNKNOWN,
                DevState.DISABLE,
            ]:
                return False
        else:
            self.logger.info(
                f"Command '{command_name}' is not supported "
                + f"in {self.op_state_model.op_state} for SubarrayNode"
            )
        return True

    def command_allowed_callable(self, command_name: str):
        """
        This method provides callable for is_cmd_allowed parameter
        set while submitting the task
        Args:
            command_name (str): Tango Device Command Name
        """

        def check_obs_state():
            """Return whether the command may be called in the current state.

            Raises:
                CommandNotAllowed if the monitored devices are unavailable.
                StateModelError if the tango device (SubarrayNode) is in
                incorrect/not permitted observation state.
            """
            self.check_subarray_device_availability()
            allowed_obs_states = {
                "AssignResources": [ObsState.EMPTY, ObsState.IDLE],
                "ReleaseAllResources": [ObsState.EMPTY, ObsState.IDLE],
                "Configure": [ObsState.IDLE, ObsState.READY],
                "End": [ObsState.IDLE, ObsState.READY],
                "Scan": [ObsState.READY],
                "EndScan": [ObsState.SCANNING],
                "Restart": [ObsState.ABORTED, ObsState.FAULT],
                "SetAdminMode": [ObsState.EMPTY],
            }

            if command_name in allowed_obs_states:
                if (
                    self.obs_state_model.obs_state
                    not in allowed_obs_states[command_name]
                ):
                    raise StateModelError(
                        f"{command_name} command not permitted "
                        + "in observation state "
                        + f"{self.obs_state_model.obs_state}"
                    )
            return True

        return check_obs_state

    def on(self, task_callback: TaskCallbackType) -> Tuple:
        """
        Submits On command as a separate task.

        :param task_callback: Update task state, defaults to None
        :type task_callback: TaskCallbackType, optional

        :return: a result code and message
        """
        on_command = On(
            self, adapter_factory=self.adapter_factory, logger=self.logger
        )
        task_status, response = self.submit_task(
            on_command.on_leaf_nodes,
            args=[self.logger],
            task_callback=task_callback,
        )
        self.logger.info(
            "On command's status: "
            + f"{task_status.name} and response: {response}"
        )
        return task_status, response

    def off(self, task_callback: TaskCallbackType) -> Tuple:
        """
        Submits Off command as a separate task.

        :return: a result code and message
        """
        off_command = Off(
            self, adapter_factory=self.adapter_factory, logger=self.logger
        )
        task_status, response = self.submit_task(
            off_command.subarray_off,
            args=[self.logger],
            task_callback=task_callback,
        )
        self.logger.info(
            "Off command's status: "
            + f"{task_status.name}, and response: {response}"
        )
        return task_status, response

    def release_all(self, task_callback: TaskCallbackType) -> Tuple:
        """
        Submits ReleaseAllResources command as a separate task.

        :return: a result code and message
        """
        release_resources_command = ReleaseAllResources(
            self,
            obs_state_model=self.obs_state_model,
            logger=self.logger,
            adapter_factory=self.adapter_factory,
        )

        task_status, response = self.submit_task(
            release_resources_command.invoke_release_resources,
            is_cmd_allowed=self.command_allowed_callable(
                "ReleaseAllResources"
            ),
            task_callback=task_callback,
        )
        self.logger.info(
            "ReleaseAllResources command's status: "
            + f"{task_status.name}, response: {response}"
        )
        return task_status, response

    def assign(self, argin, task_callback: TaskCallbackType) -> Tuple:
        """
        Submits AssignResources command as a separate task.

        :return: a result code and message
        """
        assign_command = AssignResources(
            self,
            obs_state_model=self.obs_state_model,
            logger=self.logger,
            adapter_factory=self.adapter_factory,
        )
        task_status, response = self.submit_task(
            assign_command.invoke_assign_resources,
            kwargs={"argin": argin},
            is_cmd_allowed=self.command_allowed_callable("AssignResources"),
            task_callback=task_callback,
        )
        self.logger.info(
            "AssignResources command's status: "
            + f"{task_status.name}, and response: {response}"
        )
        return task_status, response

    def update_long_running_command_result(
        self, dev_name: str, value: Tuple[str, str], timestamp: datetime
    ) -> None:
        """Abstract method for processing LongRunningCommandResult events."""
        raise NotImplementedError("This method is abstract")

    def _should_ignore_lrcr_event(self, value: Tuple[str, str]) -> bool:
        """
        Check if the long-running command result (LRC) event should be ignored
        based on the current command in progress.

        :param value: A tuple containing the unique ID and result
        message of the event.
        :type value: Tuple[str, str]
        :return: True if the event should be ignored, False otherwise.
        :rtype: bool
        """
        should_ignore = (
            False  # Flag to track whether the event should be ignored
        )

        # Early check for invalid input or no command in progress
        if not value or not self.command_in_progress:
            should_ignore = True
        else:
            # Validate the structure of the event value
            try:
                unique_id, result_message = value
                if not unique_id or not result_message:
                    self.logger.debug(
                        "Ignoring event with empty or invalid data."
                    )
                    should_ignore = True
            except (IndexError, ValueError) as error:
                self.logger.exception(
                    "Exception occurred while processing "
                    "longRunningCommandResult event: %s",
                    str(error),
                )
                should_ignore = True

        return should_ignore

    def configure(self, argin: str, task_callback: TaskCallbackType) -> Tuple:
        """
        Configure to  allocated Subarray device.

        :return: a result code and message
        """
        configure_command = Configure(
            self,
            obs_state_model=self.obs_state_model,
            logger=self.logger,
            adapter_factory=self.adapter_factory,
        )

        try:
            json_argument = json.loads(argin)
        except (json.JSONDecodeError, TypeError):
            return configure_command.reject_command("Malformed input JSON")

        configure_validator = ConfigureValidator(self, self.logger)
        exception_msg: str = ""
        if isinstance(self.input_parameter, InputParameterMid):
            try:
                try:
                    self.oso_tmc_interface = json_argument["interface"]
                    (self.tmc_major, self.tmc_minor) = split_interface_version(
                        self.oso_tmc_interface
                    )
                except KeyError as key_error:
                    self.logger.exception(key_error)
                    return configure_command.reject_command(
                        "JSON Error: Missing 'interface'"
                        "key in the Configure json"
                    )
                if "groups" in json_argument["pointing"]:
                    configure_validator.validate_groups_key_data(
                        json_argument["pointing"]["groups"]
                    )
                    self.set_pattern_scan_data(
                        json_argument["pointing"]["groups"]
                    )
                # Validate with mandatory keys
                configure_validator.verify_json_with_keys(json_argument, "MID")
                # Validate with CDM/Telmodel
                argin = configure_validator.loads(argin)

            except (json.JSONDecodeError, ValueError) as exception:
                # Catch other unexpected exceptions
                self.logger.exception(
                    "Exception occurred while validating Configure"
                    + "configuration : %s ",
                    exception,
                )
                exception_msg = str(exception)
            except Exception as exception:
                # Catch other unexpected exceptions
                self.logger.exception(
                    "Exception occurred while validating Configure"
                    + "configuration : %s ",
                    exception,
                )
                exception_msg = str(exception)

            if exception_msg:
                return configure_command.reject_command(exception_msg)
        else:
            try:
                # TODO: Enable CDM validations once CDM
                # is available, plan for PI23 is with Tel Model validations
                # # Validate with CDM
                # argin = configure_validator.loads(argin, telescope="LOW")
                # Validate with Telmodel
                validate(
                    version=OSO_TMC_LOW_CONFIGURE_INTERFACE,
                    config=json.loads(argin),
                    strictness=2,
                )
            except (json.JSONDecodeError, ValueError) as exception:
                # Catch other unexpected exceptions
                self.logger.exception(
                    "Exception occurred while validating Configure"
                    + "configuration : %s ",
                    exception,
                )
                exception_msg = str(exception)
            except Exception as exception:
                # Catch other unexpected exceptions
                self.logger.exception(
                    "Exception occurred while validating Configure"
                    + "configuration : %s ",
                    exception,
                )
                exception_msg = str(exception)

            if exception_msg:
                return configure_command.reject_command(exception_msg)

        task_status, response = self.submit_task(
            configure_command.invoke_configure,
            kwargs={"argin": argin},
            is_cmd_allowed=self.command_allowed_callable("Configure"),
            task_callback=task_callback,
        )
        self.logger.info(
            "Configure command's status: "
            + f"{task_status.name}, and response: {response}"
        )
        return task_status, response

    def set_pattern_scan_data(self, group_list):
        """This method set pattern scan data.
        Override this method to set pattern scan data
        provided in configure
        """

    def end(self, task_callback: TaskCallbackType) -> Tuple:
        """
        End the configuration of Subarray.

        :return: a result code and message
        """
        end_command = End(
            self,
            obs_state_model=self.obs_state_model,
            adapter_factory=self.adapter_factory,
            logger=self.logger,
        )
        task_status, response = self.submit_task(
            end_command.invoke_end,
            args=[self.logger],
            is_cmd_allowed=self.command_allowed_callable("End"),
            task_callback=task_callback,
        )
        self.logger.info(
            "End command's status: "
            + f"{task_status.name}, and response: {response}"
        )
        return task_status, response

    def set_admin_mode(
        self, argin: int, task_callback: TaskCallbackType
    ) -> Tuple:
        """
        set the admin mode of subarray devices
        :return: a result code and message
        """
        set_admin_mode_command = SetAdminMode(
            self, adapter_factory=self.adapter_factory, logger=self.logger
        )
        task_status, response = self.submit_task(
            set_admin_mode_command.invoke_setadminmode,
            kwargs={"argin": argin},
            is_cmd_allowed=self.command_allowed_callable("SetAdminMode"),
            task_callback=task_callback,
        )
        self.logger.info(
            "SetAdminMode command's status: "
            + f"{task_status.name}, and response: {response}"
        )
        return task_status, response

    def scan(self, argin, task_callback: TaskCallbackType) -> Tuple:
        """
        Scanning the devices.

        :return: a result code and message
        """

        scan_command = Scan(
            self,
            obs_state_model=self.obs_state_model,
            adapter_factory=self.adapter_factory,
            logger=self.logger,
        )
        scan_ids = []
        self.logger.info(f"Scan json is {argin}")
        try:
            json_dict = json.loads(argin)
            if "scan_ids" in json_dict:
                scan_ids = json_dict["scan_ids"]
        except (json.JSONDecodeError, TypeError):
            return scan_command.reject_command("Malformed input json")

        if isinstance(self.input_parameter, InputParameterMid):
            # Utilize CDM to validate json schema
            try:
                scan_validator = ScanValidator(self.logger)
                argin = scan_validator.loads(argin)
                # Remove this logic once telmodel is updated to
                # accept scan ids
                if scan_ids and self.pattern_scan_data:
                    offset_list = self.pattern_scan_data["attrs"]["x_offsets"]
                    scan_validator.validate_scan_ids(offset_list, scan_ids)
                    argin_json = json.loads(argin)
                    argin_json["scan_ids"] = scan_ids
                    argin = json.dumps(argin_json)
            except Exception as excetpion:
                return scan_command.reject_command(str(excetpion))
        else:
            exception_msg: str = ""
            try:
                # Validate with Telmodel
                validate(
                    version=OSO_TMC_LOW_SCAN_INTERFACE,
                    config=json.loads(argin),
                    strictness=2,
                )
            except (json.JSONDecodeError, ValueError) as exception:
                # Catch other unexpected exceptions
                self.logger.exception(
                    "Exception occurred while Scan " + "configuration : %s ",
                    exception,
                )
                exception_msg = str(exception)
            except Exception as exception:
                # Catch other unexpected exceptions
                self.logger.exception(
                    "Exception occurred while Scan " + "configuration : %s ",
                    exception,
                )
                exception_msg = str(exception)

            if exception_msg:
                return scan_command.reject_command(exception_msg)

        task_status, response = self.submit_task(
            scan_command.invoke_scan,
            kwargs={"argin": argin},
            is_cmd_allowed=self.command_allowed_callable("Scan"),
            task_callback=task_callback,
        )

        self.logger.info(
            "Scan command's status: "
            + f"{task_status.name}, and response: {response}"
        )
        return task_status, response

    def end_scan(self, task_callback: TaskCallbackType) -> Tuple:
        """
        End the configuration of Subarray.

        :return: a result code and message
        """
        end_scan_command = EndScan(
            self,
            obs_state_model=self.obs_state_model,
            adapter_factory=self.adapter_factory,
            logger=self.logger,
        )
        task_status, response = self.submit_task(
            end_scan_command.invoke_end_scan,
            args=[self.logger],
            is_cmd_allowed=self.command_allowed_callable("EndScan"),
            task_callback=task_callback,
        )
        self.logger.info(
            "EndScan command's status: "
            + f"{task_status.name}, and response: {response}"
        )
        return task_status, response

    def abort(self, task_callback: TaskCallbackType) -> Tuple:
        """
        Aborting the subarray.

        :return: a result code and message
        """

        self.abort_command = Abort(
            self,
            obs_state_model=self.obs_state_model,
            adapter_factory=self.adapter_factory,
            logger=self.logger,
        )

        # base classes set and clear immediately, so we set to
        # clear ongoing observers and timers.
        self.abort_event.set()
        self.observable.notify_observers(attribute_value_change=True)

        self.abort_event.clear()

        def _invoke_abort_callback(
            status=None,
            progress=None,
            result=None,
            exception=None,
        ):
            """
            Method for invoking abort callback
            """
            if progress is not None:
                task_callback(progress=50 + progress / 2)

            if status == TaskStatus.FAILED:
                task_callback(
                    status=status, exception=exception, result=result
                )
            elif status == TaskStatus.COMPLETED:
                task_callback(status=status, progress=100, result=result)

        def _abort_commands_callback(
            status=None,
            progress=None,
            result=None,
            exception=None,
        ):
            """
            method for abort command callback
            """
            if progress is not None:
                task_callback(progress=progress / 2)
            if status == TaskStatus.FAILED:
                task_callback(
                    status=status, exception="Failed to abort commands"
                )
            elif status == TaskStatus.COMPLETED:
                task_callback(
                    progress=50,
                )

                self.abort_command.invoke_abort(
                    self.logger, task_callback=_invoke_abort_callback
                )

        self.command_in_progress = "Abort"
        return self.abort_commands(task_callback=_abort_commands_callback)

    def restart(self, task_callback: TaskCallbackType) -> Tuple:
        """
        Restarting the subarray.

        :return: a result code and message
        """
        restart_command = Restart(
            self,
            obs_state_model=self.obs_state_model,
            adapter_factory=self.adapter_factory,
            logger=self.logger,
        )

        task_status, response = self.submit_task(
            restart_command.invoke_restart,
            args=[self.logger],
            is_cmd_allowed=self.command_allowed_callable("Restart"),
            task_callback=task_callback,
        )
        self.logger.info(
            "Restart command's status: "
            + f"{task_status.name}, and response: {response}"
        )
        return task_status, response

    def stop_aggregation_process(self):
        """Override this method in mid and low"""
        raise NotImplementedError

    def stop_all_process(self):
        """This stop aggregation process"""
        with self.process_lock:
            self.stop_aggregation_process()
            del self.event_data_queue
            del self.aggregated_obs_state
            self.aggregate_process_manager.shutdown()
            self.logger.debug("aggregation process stopped")

    def __del__(self):
        """shutdown aggregation process"""
        self.logger.debug("component destructor called")
        self.stop_all_process()

    def clear_assigned_resources(self):
        """Clears assigned resources after the aggregation of obsState EMPTY
        This method is overridden by the child classes.
        For Mid it clears dish dev names.
        For low currently no resources are cleared.
        """

    def clear_mapping_scan_data(self) -> None:
        """Clears mapping scan data.
        This method is overridden in MID
        to clear MID-specific mapping scan data.
        """

    def __start_event_processing_threads(self) -> None:
        """Start all the event processing threads."""
        for attribute in self.event_queues:
            thread = threading.Thread(
                target=self.process_event, args=[attribute], name=attribute
            )
            thread.start()

    def process_event(self, attribute_name: str) -> None:
        """Process the given attribute's event using the data from the
        event_queues and invoke corresponding process method.

        :param attribute_name: Name of the attribute for which event is to be
            processed
        :type attribute_name: str

        :returns: None
        """
        while not self._stop_thread:
            try:
                event_data = self.event_queues[attribute_name].get(
                    block=True, timeout=0.1
                )
                if not self.check_event_error(
                    event_data, f"{attribute_name}_Callback"
                ):
                    self.event_processing_methods[attribute_name](
                        event_data.device.dev_name(),
                        event_data.attr_value.value,
                        event_data.attr_value.time.todatetime(),
                    )
            except Empty:
                # If an empty exception is raised by the Queue, we can
                # safely ignore it.
                pass
            except Exception as exception:
                self.logger.error(exception)
        self.logger.debug("Process event thread stopped")

    def is_scan_completed(self):
        """
        A function to check whether Scan Command is completed succesfully."""

        if (
            self.obs_state_model.obs_state == ObsState.SCANNING
            and self.all_lrcr_ok is True
            and self.is_mapping_scan is False
        ) or (self.mapping_scan_completed is True):
            self.logger.info("All events received for Scan")

            return True

        return False

    def log_state(self, msg: str = "Device States") -> None:
        """
        Logs the state of devices and dishes.

        This method logs the observation states of generic devices and

        the pointing states of dish devices.

        Args:
            msg (str): A message to be included in the log.
        """
        device_names = []
        dish_names = []
        obs_state = []
        pointing_state = []
        pd.set_option("display.colheader_justify", "left")
        pd.set_option("display.expand_frame_repr", False)

        for device in self.devices:
            if (
                isinstance(device, DishDeviceInfo)
                and self.dev_family in device.dev_name
            ):
                dish_names.append(device.dev_name)
                pointing_state.append(
                    PointingState(device.pointing_state).name
                )
            elif self.dev_family in device.dev_name:
                device_names.append(device.dev_name)
                obs_state.append(ObsState(device.obs_state).name)

        device_states = pd.DataFrame(
            {
                "Devices": device_names,
                "obsState": obs_state,
            }
        )

        dish_states = pd.DataFrame(
            {
                "Devices": dish_names,
                "pointingState": pointing_state,
            }
        )
        self.logger.info("Observation states are as follows ")
        self.logger.info(
            f"\n{msg}\n\n"
            + f"{device_states.to_string(index=False, justify='left')}\n"
        )

        if not dish_states.empty:
            self.logger.info("Dish pointing states are as follows")
            self.logger.info(
                f"\n{msg}\n\n"
                + f"{dish_states.to_string(index=False, justify='left')}\n"
            )

    def end_singular_scan(self, task_callback: TaskCallbackType) -> Tuple:
        """
        End the configuration of Subarray.

        :return: a result code and message
        """
        end_scan_command = EndScan(
            self,
            obs_state_model=self.obs_state_model,
            adapter_factory=self.adapter_factory,
            logger=self.logger,
            is_only_on_trajectory_dishes=False,
        )
        task_status, response = self.submit_task(
            end_scan_command.end_scan,
            args=[self.logger],
            is_cmd_allowed=self.command_allowed_callable("EndScan"),
            task_callback=task_callback,
        )
        self.logger.info(
            "New EndScan command's status: "
            + f"{task_status.name}, and response: {response}"
        )
        return task_status, response
