"""
On Command for SubarrayNode
"""
import threading
from typing import Optional, Tuple

from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tango_base.executor import TaskStatus
from ska_tmc_common.adapters import AdapterFactory

from ska_tmc_subarraynode.commands.subarray_node_command import (
    SubarrayNodeCommand,
)
from ska_tmc_subarraynode.utils.constants import COMMAND_COMPLETION_MESSAGE


class On(SubarrayNodeCommand):
    """
    A class for the SubarrayNode's On() command.
    """

    def __init__(
        self,
        component_manager,
        adapter_factory=AdapterFactory(),
        logger=None,
    ):
        super().__init__(
            component_manager,
            adapter_factory,
            logger=logger,
        )

    def on_leaf_nodes(
        self,
        logger,
        task_callback: TaskCallbackType,
        task_abort_event: Optional[threading.Event] = None,
    ):
        """This is a long running method for On command, it executes do hook,
        invokes On command on CspSubarrayleafnode and SdpSubarrayleafnode.

        :param logger: logger
        :type logger: logging.Logger
        :param task_callback: Update task state, defaults to None
        :type task_callback: TaskCallbackType, optional
        :param task_abort_event: Check for abort, defaults to None
        :type task_abort_event: Event, optional
        """
        # Indicate that the task has started
        task_callback(status=TaskStatus.IN_PROGRESS)
        self.component_manager.command_in_progress = "On"
        ret_code, message = self.do(argin=None)
        self.logger.info(message)
        if ret_code == ResultCode.FAILED:
            task_callback(
                status=TaskStatus.COMPLETED,
                result=(ResultCode.FAILED, message),
                exception=message,
            )
        else:
            task_callback(
                status=TaskStatus.COMPLETED, result=(ResultCode.OK, message)
            )

    def do_mid(self, argin: str = None) -> Tuple[ResultCode, str]:
        """
        Method to invoke On command on Mid CspSubarrayLeafNode
        and SdpSubarrayLeafNode.

        :param argin: Input json for Command, defaults to None
        :type: None

        :return: A tuple containing a return code and a string message
            indicating execution status of command.
        :rtype: Tuple(ResultCode, str)

        :raises: DevFailed if the command execution is not successful
        """
        command_name = "On"
        self.update_command_in_progress_id(command_name)

        ret_code, message = self.init_adapters()
        if ret_code == ResultCode.FAILED:
            return ret_code, message

        tmc_sdp_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_sdp_sln_device_name()
        )
        try:
            if self.component_manager.check_if_sdp_sln_is_available():
                tmc_sdp_sln_adapter.On()
                self.logger.info(
                    f"On command is invoked on {tmc_sdp_sln_adapter.dev_name}"
                )
            else:
                return (
                    ResultCode.FAILED,
                    f"{tmc_sdp_sln_adapter.dev_name} is "
                    + "not available to receive command",
                )
        except Exception as e:
            log_msg = (
                "Execution of On command is failed."
                + "Reason: Error while invoking On in TMC SDP "
                + f"Subarray Leaf Node{tmc_sdp_sln_adapter.dev_name}: {e}"
                + "The command is not executed successfully."
                + "The device will continue with normal operation"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error while invoking On "
                + f"command on {tmc_sdp_sln_adapter.dev_name}",
            )

        self.logger.info("On command is invoked on SubarrayNode.")

        return (ResultCode.OK, COMMAND_COMPLETION_MESSAGE)

    def do_low(self, argin=None) -> Tuple[ResultCode, str]:
        """
        Method to invoke On command on MccsSubarrayLeafNode,
        Low CspSubarrayLeafNode and Low SdpSubarrayLeafNode.

        :param argin: Input json for Command, defaults to None
        :type: None

        :return: A tuple containing a return code and a string
            message indicating execution status of command.
        :rtype: Tuple(ResultCode, str)

        :raises: Exception if the command execution is not successful
        """

        self.update_command_in_progress_id("On")

        ret_code, message = self.init_adapters()
        if ret_code == ResultCode.FAILED:
            return ret_code, message

        tmc_sdp_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_sdp_sln_device_name()
        )
        try:
            if self.component_manager.check_if_sdp_sln_is_available():
                tmc_sdp_sln_adapter.On()
                self.logger.info(
                    f"On command is invoked on {tmc_sdp_sln_adapter.dev_name}"
                )
            else:
                return (
                    ResultCode.FAILED,
                    f"{tmc_sdp_sln_adapter.dev_name} is "
                    + "not available to receive command",
                )
        except Exception as e:
            log_msg = (
                "Execution of On command is failed."
                + "Reason: Error while invoking On in TMC SDP "
                + f"Subarray Leaf Node{tmc_sdp_sln_adapter.dev_name}: {e}"
                + "The command is not executed successfully."
                + "The device will continue with normal operation"
            )

            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error while invoking On "
                + f"command on {tmc_sdp_sln_adapter.dev_name}",
            )

        self.logger.info("On command is invoked on SubarrayNode.")

        return (ResultCode.OK, COMMAND_COMPLETION_MESSAGE)
