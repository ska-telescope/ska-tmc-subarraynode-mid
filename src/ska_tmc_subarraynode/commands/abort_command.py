"""
Abort Command for SubarrayNode.
"""
import threading
import time
from typing import Optional, Tuple

from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common.adapters import AdapterFactory

from ska_tmc_subarraynode.commands.subarray_node_command import (
    SubarrayNodeCommand,
)
from ska_tmc_subarraynode.utils.constants import COMMAND_STARTED_MESSAGE


class Abort(SubarrayNodeCommand):
    """
    A class for SubarrayNode Abort() command.
    It aborts the ongoing operation on subarray node.
    """

    def __init__(
        self,
        component_manager,
        obs_state_model,
        adapter_factory=AdapterFactory(),
        logger=None,
    ):
        super().__init__(
            component_manager=component_manager,
            adapter_factory=adapter_factory,
            logger=logger,
        )
        self.obs_state_model = obs_state_model
        self.command_retries: int = 3
        self.aborted_csp: bool = False
        self.aborted_sdp: bool = False
        self.aborted_mccs: bool = False
        self.aborted_dishes_list: list = []
        self.dish_leaf_dict: dict = {}
        self.abort_completed_thread = threading.Thread(
            target=self.check_abort_timeout_and_set_ObsState
        )
        self.is_operation_aborted = False

    def invoke_abort(
        self,
        logger,
        task_callback: TaskCallbackType,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """Method for invoking Abort command"""
        # Indicate that the task has started
        task_callback(status=TaskStatus.IN_PROGRESS)
        with self.component_manager.event_data_manager.eventlock:
            self.component_manager.event_data_manager.clear_lrcr()
        result_code, message = self.do(argin=None)
        self.logger.info(message)
        if result_code == ResultCode.FAILED:
            task_callback(
                status=TaskStatus.COMPLETED,
                result=(result_code, message),
                exception=message,
            )
        else:
            task_callback(
                status=TaskStatus.COMPLETED, result=(result_code, message)
            )

    def do_mid(self, argin: str = None) -> Tuple[ResultCode, str]:
        """
        This method invokes Abort command on CSP Subarray Leaf Node and SDP
        Subarray Leaf Node, and stops tracking of all the assigned dishes.

        return:
            A tuple containing a return code and a
            string message indicating execution status of command.


        rtype:
            (ResultCode, str)

        raises:
            Exception if an error occurs while invoking a
            command on any of the devices like CSPSubarrayLeafNode,
            SDPSubarrayLeafNode or DishLeafNode
        """
        self.update_command_in_progress_id("Abort")
        result_code, message = self.init_adapters()
        if result_code == ResultCode.FAILED:
            return result_code, message

        self.obs_state_model.perform_action("abort_invoked")

        return_code, message = self.abort_timer()
        if return_code == ResultCode.FAILED:
            return self.component_manager.generate_command_result(
                return_code,
                message,
            )
        # Stop the current ongoing mapping scan
        if self.component_manager.scan_obj:
            self.logger.debug("Stopping Mapping Scan")
            self.component_manager.scan_obj.clear_data()

        self.aborted_csp = False
        self.aborted_sdp = False
        self.aborted_dishes_list = []

        # Create a dictionary mapping the dish with the respective dish leaf
        # node. Here key is dish leaf node device whereas the dish device is
        # value.
        for dish_leaf in self.component_manager.get_tmc_dish_ln_device_names():
            # extract the dish id
            dish_id = "".join(
                [dish_num for dish_num in dish_leaf if dish_num.isdigit()]
            )
            for dish in self.component_manager.get_dish_dev_names():
                if dish_id[1:] in dish:
                    self.dish_leaf_dict[dish_leaf] = dish
        self.logger.debug(
            "Dish leaf node to dish device mapping: %s", self.dish_leaf_dict
        )

        for retry in range(0, self.command_retries):
            if self.aborted_csp is False:
                return_code, message = self.abort_csp()
                if return_code == ResultCode.FAILED:
                    return self.component_manager.generate_command_result(
                        return_code,
                        message,
                    )
            if self.aborted_sdp is False:
                return_code, message = self.abort_sdp()
                if return_code == ResultCode.FAILED:
                    return self.component_manager.generate_command_result(
                        return_code,
                        message,
                    )

            if len(self.aborted_dishes_list) != len(
                self.component_manager.get_tmc_dish_ln_device_names()
            ):
                return_code, message = self.abort_dishes()
                if return_code == ResultCode.FAILED:
                    return self.component_manager.generate_command_result(
                        return_code,
                        message,
                    )
        self.logger.info("Invoked Abort Command.")
        try:
            if not self.abort_completed_thread.is_alive():
                self.abort_completed_thread.start()
        except Exception as exception:
            self.logger.debug(
                "Exception occurred while starting the Abort"
                + f"completion check thread: {exception}"
            )

        return (ResultCode.STARTED, COMMAND_STARTED_MESSAGE)

    def abort_timer(self) -> Tuple[ResultCode, str]:
        """Method for managing abort timer"""
        self.logger.info("Abort scan timer")
        try:
            if self.component_manager.is_scan_timer_running():
                self.component_manager.stop_scan_timer()
        except Exception as exception:
            self.logger.exception(
                "Execution of Abort command is failed."
                + "Reason: Failed to abort the scan timer: %s"
                + "The command is not executed successfully."
                + "The device will continue with normal operation",
                exception,
            )
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                f"Error in stopping scan timer :: {exception}",
            )
        return ResultCode.OK, ""

    def abort_sdp(self) -> Tuple[ResultCode, str]:
        """
        Send abort command to SDP devices
        """
        try:
            devices_availability_dict = (
                self.component_manager.get_devices_availability_dict()
            )
            sdp_subarray_availability = devices_availability_dict[
                self.component_manager.get_tmc_sdp_sln_device_name()
            ]
            if set(sdp_subarray_availability) == set([True]):
                sdp_subarray_leaf_node_obs_state = self._get_device_obstate(
                    self.component_manager.get_tmc_sdp_sln_device_name()
                )
                if sdp_subarray_leaf_node_obs_state in [
                    ObsState.EMPTY,
                    ObsState.ABORTING,
                    ObsState.ABORTED,
                    ObsState.FAULT,
                ]:
                    self.logger.info(
                        "Abort Command is not invoked on the SDP Subarray "
                        + "Lead Node as the SDP Subarray observation state is"
                        f"{sdp_subarray_leaf_node_obs_state}"
                    )
                    self.aborted_sdp = True

                else:
                    sdp_sln_adapter = self.get_adapter_by_device_name(
                        self.component_manager.get_tmc_sdp_sln_device_name()
                    )
                    result_code, message = sdp_sln_adapter.Abort()

                    self.aborted_sdp = True
                    if result_code == ResultCode.FAILED:
                        error_message = (
                            f"Abort command failed with result code: "
                            f"{result_code}, message: {message}"
                        )
                        self.logger.error(error_message)

                    return result_code, message

            else:
                self.logger.info(
                    "Abort command can not be invoked on the "
                    + f"{self.component_manager.get_tmc_sdp_sln_device_name()}"
                )
        except Exception as e:
            self.logger.exception(
                "Execution of Abort command is failed."
                + "Reason: Failed to execute Abort command"
                + "on SDP Subarray leaf Node: %s"
                + "The command is not executed successfully."
                + "The device will continue with normal operation",
                e,
            )
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error while invoking Abort command on"
                + f"SDP Subarray Leaf Node {sdp_sln_adapter.dev_name} : {e}",
            )
        return ResultCode.OK, ""

    def abort_csp(self) -> Tuple[ResultCode, str]:
        """
        Send abort command to CSP devices
        """
        try:
            devices_availability_dict = (
                self.component_manager.get_devices_availability_dict()
            )
            csp_subarray_availability = devices_availability_dict[
                self.component_manager.get_tmc_csp_sln_device_name()
            ]

            if set(csp_subarray_availability) == set([True]):
                csp_subarray_leaf_node_obs_state = self._get_device_obstate(
                    self.component_manager.get_tmc_csp_sln_device_name()
                )
                if csp_subarray_leaf_node_obs_state in [
                    ObsState.EMPTY,
                    ObsState.ABORTING,
                    ObsState.ABORTED,
                    ObsState.FAULT,
                ]:
                    self.logger.info(
                        "Abort Command is not invoked on the CSP Subarray "
                        + "Leaf Node as the CSP Subarray observation state is "
                        f"{csp_subarray_leaf_node_obs_state}"
                    )
                    self.aborted_csp = True

                else:
                    csp_sln_adapter = self.get_adapter_by_device_name(
                        self.component_manager.get_tmc_csp_sln_device_name()
                    )
                    result_code, message = csp_sln_adapter.Abort()

                    self.aborted_csp = True
                    if result_code == ResultCode.FAILED:
                        error_message = (
                            "Abort command failed with result code: "
                            f"{result_code}, message: {message}"
                        )
                        self.logger.error(error_message)
                    return result_code, message

            else:
                self.logger.info(
                    "Abort command can not be invoked on the "
                    + f"{self.component_manager.get_tmc_csp_sln_device_name()}"
                )
        except Exception as e:
            self.logger.exception(
                "Execution of Abort command is failed."
                + "Reason: Failed to execute Abort command on "
                + "Csp Subarray Leaf Node: %s "
                + "The command is not executed successfully."
                + "The device will continue with normal operation",
                e,
            )
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error while invoking Abort command on "
                + f"CSP Subarray Leaf Node {csp_sln_adapter.dev_name} : {e}",
            )
        return ResultCode.OK, ""

    def abort_dishes(self) -> Tuple[ResultCode, str]:
        """
        Abort command on Dish Leaf Node Group
        """

        tmc_leaf_dish_adapters = self.get_dish_adapter_by_device_name(
            self.component_manager.get_tmc_dish_ln_device_names()
        )
        result = {"result_code": [], "message": [], "dish": []}
        for dish_leaf_adapter in tmc_leaf_dish_adapters:
            try:
                if dish_leaf_adapter.dev_name in self.aborted_dishes_list:
                    self.logger.info(
                        "Abort command already invoked on "
                        + f"{dish_leaf_adapter.dev_name}"
                    )
                else:
                    devices_availability_dict = (
                        self.component_manager.get_devices_availability_dict()
                    )
                    dish_availability = devices_availability_dict[
                        dish_leaf_adapter.dev_name
                    ]

                    self.logger.debug(
                        "Dish: %s, Availability: %s",
                        dish_leaf_adapter.dev_name,
                        dish_availability,
                    )
                    if set(dish_availability) == set([True]):
                        (
                            result_code,
                            message,
                        ) = dish_leaf_adapter.AbortCommands()

                        if result_code[0] == ResultCode.FAILED:
                            result["result_code"].append(result_code[0])
                            result["message"].append(message[0])
                            result["dish"].append(dish_leaf_adapter.dev_name)

                        self.aborted_dishes_list.append(
                            dish_leaf_adapter.dev_name
                        )
                    else:
                        self.logger.info(
                            "Abort command can not be invoked on the "
                            + f"{dish_leaf_adapter.dev_name}"
                        )
            except Exception as e:
                log_msg = (
                    "Execution of Abort command is failed."
                    + "Reason: Failed to execute Abort"
                    + "command on Dish Leaf Node: %s"
                    + " The device will continue with its "
                    + "current state/operation",
                    e,
                )
                self.logger.exception(log_msg)
                return self.component_manager.generate_command_result(
                    ResultCode.FAILED,
                    "Error in calling Abort in TMC "
                    + f"Dish Leaf Node: {dish_leaf_adapter.dev_name} : {e}",
                )
        if ResultCode.FAILED in result["result_code"]:
            self.logger.debug(
                f"Abort command failed on dishes: {result['dish']} "
                f"with result code {result['result_code']} "
                f"and message {result['message']}"
            )
            return (
                ResultCode.FAILED,
                f"Abort command failed on dishes: {result['dish']}",
            )

        return ResultCode.OK, ""

    def do_low(self, argin: str = None) -> Tuple[ResultCode, str]:
        """
        Method to invoke Abort command on Csp SubarrayLeaf Node and
        Sdp SubarrayLeaf Node.

        return:
            A tuple containing a return code and
            a string message indicating execution status of command.

        rtype:
            (ResultCode, str)

        """
        self.update_command_in_progress_id("Abort")
        result_code, message = self.init_adapters()
        if result_code == ResultCode.FAILED:
            return result_code, message

        self.obs_state_model.perform_action("abort_invoked")

        return_code, message = self.abort_timer()
        if return_code == ResultCode.FAILED:
            return self.component_manager.generate_command_result(
                return_code,
                message,
            )

        self.aborted_csp = False
        self.aborted_sdp = False
        self.aborted_mccs = False

        for retry in range(0, self.command_retries):
            if self.aborted_csp is False:
                self.logger.info("Invoking Abort Command on CSP")
                return_code, message = self.abort_csp()
                if return_code == ResultCode.FAILED:
                    return self.component_manager.generate_command_result(
                        return_code,
                        message,
                    )
            if self.aborted_sdp is False:
                self.logger.info("Invoking Abort Command on SDP")
                return_code, message = self.abort_sdp()
                if return_code == ResultCode.FAILED:
                    return self.component_manager.generate_command_result(
                        return_code,
                        message,
                    )
            if self.aborted_mccs is False:
                self.logger.info("Invoking Abort Command on MCCS")
                return_code, message = self.abort_mccs()
                if return_code == ResultCode.FAILED:
                    return self.component_manager.generate_command_result(
                        return_code,
                        message,
                    )

        try:
            if not self.abort_completed_thread.is_alive():
                self.abort_completed_thread.start()
        except Exception as exception:
            self.logger.debug(
                "Exception occurred while starting the Abort"
                + f"completion check thread: {exception}"
            )

        self.logger.info("Execution of Abort command is started.")
        return (ResultCode.STARTED, COMMAND_STARTED_MESSAGE)

    def abort_mccs(self) -> Tuple[ResultCode, str]:
        """
        Executes the 'Abort' command on the TMC MCCS Subarray Leaf Node.

        Returns:
        tuple: A tuple containing a ResultCode indicating the status of the
        command execution and a string describing any error that occurred
        during execution. For successful execution, the ResultCode is OK,
        and the error string is empty.
        """
        try:
            mccs_subarray_leaf_node_obs_state = self._get_device_obstate(
                self.component_manager.get_tmc_mccs_sln_device_name()
            )
            if mccs_subarray_leaf_node_obs_state in [
                ObsState.EMPTY,
                ObsState.ABORTING,
                ObsState.ABORTED,
                ObsState.FAULT,
            ]:
                self.logger.info(
                    "Abort Command is not invoked on the MCCS Subarray "
                    + "Leaf Node as the MCCS Subarray observation state is"
                    f"{mccs_subarray_leaf_node_obs_state}"
                )
                self.aborted_mccs = True
            else:
                tmc_mccs_sln_adapter = self.get_adapter_by_device_name(
                    self.component_manager.get_tmc_mccs_sln_device_name()
                )
                result_code, message = tmc_mccs_sln_adapter.Abort()
                self.aborted_mccs = True
                # Log the result code and message
                self.logger.info(
                    f"Abort command result code: {result_code}, "
                    f"message: {message}"
                )

                if result_code == ResultCode.FAILED:
                    error_message = (
                        "Abort command failed with result code: "
                        f"{result_code}, message: {message}"
                    )
                    self.logger.error(error_message)
                return result_code, message

        except Exception as exception:
            log_msg = (
                "Execution of Abort command failed on MCCS"
                "Subarray Leaf Node: %s",
                exception,
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                (
                    "Error in calling Abort in TMC MCCS Subarray Leaf Node %s",
                    tmc_mccs_sln_adapter.dev_name,
                ),
            )
        return ResultCode.OK, ""

    def check_abort_timeout_and_set_ObsState(self):
        """
        A method to wait for the SubarrayNode obsState ABORTED
        for the given AbortCommandTimeOut. Once the AbortCommandTimeOut exceeds
        put SubarrayNode to obsState FAULT.

        """
        self.logger.debug("Thread for ObsState ABORTED timeout started")
        start_time = time.time()
        time_spent = 0

        while self.is_operation_aborted is False:
            if time_spent >= self.component_manager.abort_command_timeout:
                self.logger.info(
                    "Timeout occurred while waiting for ObsState.ABORTED"
                )
                command_in_progress = (
                    self.component_manager.command_in_progress
                )
                self.component_manager._set_subarraynode_obsstate(
                    self.obs_state_model.obs_state,
                    command_in_progress,
                    abort_timeout=True,
                )
                break
            iteration_time = time.time()
            time_spent = iteration_time - start_time
        else:
            self.logger.info(
                "SubarrayNode transitioned to obsState %s",
                self.obs_state_model.obs_state,
            )
