"""
Restart Command for SubarrayNode.
"""
import threading
from typing import Optional, Tuple

from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common.adapters import AdapterFactory

from ska_tmc_subarraynode.commands.subarray_node_command import (
    SubarrayNodeCommand,
)
from ska_tmc_subarraynode.utils.constants import COMMAND_COMPLETION_MESSAGE


class Restart(SubarrayNodeCommand):
    """
    A class representing the Restart command for SubarrayNode.

    This command restarts various leaf nodes of the Subarray, including
    CSP Subarray Leaf Node, SDP Subarray Leaf Node, and Mccs Subarray Leaf Node
    to reset ongoing activities and configurations.

    :param component_manager: The component manager instance.
    :param obs_state_model: The observational state model of the Subarray.
    :param adapter_factory: An optional adapter factory instance.
    :param logger: An optional logger instance for logging. Default is None.
    """

    def __init__(
        self,
        component_manager,
        obs_state_model,
        adapter_factory=AdapterFactory(),
        logger=None,
    ):
        super().__init__(
            component_manager=component_manager,
            adapter_factory=adapter_factory,
            logger=logger,
        )
        self.obs_state_model = obs_state_model

    def invoke_restart(
        self,
        logger,
        task_callback: TaskCallbackType,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Invokes the Restart command on various leaf nodes of the Subarray.

        :param logger: The logger instance for logging.
        :type logger: logging.Logger
        :param task_callback: An optional callback function for task status
            updates. Default is None.
        :type task_callback: TaskCallbackType, optional
        :param task_abort_event: An optional threading event for checking task
            abort status. Default is None.
        :type task_abort_event: threading.Event, optional
        """

        self.obs_state_model.perform_action("restart_invoked")
        # Indicate that the task has started
        self.component_manager.command_in_progress = "Restart"
        task_callback(status=TaskStatus.IN_PROGRESS)
        with self.component_manager.event_data_manager.eventlock:
            self.component_manager.event_data_manager.clear_lrcr()
        result_code, message = self.do(argin=None)
        result_code = ResultCode(result_code)
        self.logger.info(message)
        if result_code == ResultCode.FAILED:
            task_callback(
                status=TaskStatus.COMPLETED,
                result=(result_code, message),
                exception=Exception(message),
            )
        else:
            task_callback(
                status=TaskStatus.COMPLETED, result=(result_code, message)
            )

    def do_mid(self, argin: str = None) -> Tuple[ResultCode, str]:
        """
        Invokes the Restart command on CSP Subarray Leaf Node, SDP Subarray
        Leaf Node,
        and Dish Leaf Nodes.

        :param argin: Optional argument. Default is None.
        :type argin: str, optional
        :return: A tuple containing the result code and a message indicating
            the execution status.
        :rtype: Tuple[ResultCode, str]
        """
        self.update_command_in_progress_id("Restart")
        self.component_manager.unsubscribe_dish_events()

        result_code, message = self.init_adapters()
        if result_code == ResultCode.FAILED:
            return result_code, message

        return_code, message = self.clean_up_configuration()
        if return_code == ResultCode.FAILED:
            return self.component_manager.generate_command_result(
                return_code,
                message,
            )
        return_code, message = self.clean_up_dishes()
        if return_code == ResultCode.FAILED:
            return self.component_manager.generate_command_result(
                return_code,
                message,
            )

        # Get CspSubarrayLeafNode ObsState
        csp_subarray_leaf_node_obs_state = self._get_device_obstate(
            self.component_manager.get_tmc_csp_sln_device_name()
        )
        # Get SdpSubarrayLeafNode ObsState
        sdp_subarray_leaf_node_obs_state = self._get_device_obstate(
            self.component_manager.get_tmc_sdp_sln_device_name()
        )
        self.logger.info(
            f"CspSubarrayLeafNode obsState: {csp_subarray_leaf_node_obs_state}"
        )

        self.logger.info(
            f"SdpSubarrayLeafNode obsState: {sdp_subarray_leaf_node_obs_state}"
        )

        if self.component_manager.component_state_changed_callback:
            self.component_manager.component_state_changed_callback(
                {"restart_invoked": None}
            )
        # check CspSubarrayLeafNode obsState =
        # Aborted/Fault before sending Restart command
        if csp_subarray_leaf_node_obs_state in [
            ObsState.ABORTED,
            ObsState.FAULT,
        ]:
            return_code, message = self.restart_csp()
            if return_code == ResultCode.FAILED:
                return self.component_manager.generate_command_result(
                    return_code,
                    message,
                )
        else:
            self.logger.info(
                "Restart is not invoked on Csp Subarray. "
                + "Csp Subarray Leaf Node obsstate "
                + f"is: {csp_subarray_leaf_node_obs_state}"
            )

        # check SdpSubarrayLeafNode obsState =
        # Aborted/Fault before sending Restart command
        if sdp_subarray_leaf_node_obs_state in [
            ObsState.ABORTED,
            ObsState.FAULT,
        ]:
            return_code, message = self.restart_sdp()
            if return_code == ResultCode.FAILED:
                return self.component_manager.generate_command_result(
                    return_code,
                    message,
                )
        else:
            self.logger.info(
                "Restart is not invoked on Sdp Subarray."
                + "Sdp Subarray Leaf Node obsstate "
                + f"is:{sdp_subarray_leaf_node_obs_state}"
            )

        self.logger.info("Restart command is invoked.")
        return ResultCode.OK, COMMAND_COMPLETION_MESSAGE

    def restart_sdp(self) -> Tuple[ResultCode, str]:
        """
        Restarts the SDP devices by invoking the Restart command on the
        SDP Subarray Leaf Node.

        :return: A tuple containing the result code and an empty string.
        :rtype: Tuple[ResultCode, str]
        """
        sdp_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_sdp_sln_device_name()
        )
        # Invoke Restart command on SDP Subarray Leaf Node.
        try:
            result_code, message_or_unique_id = sdp_sln_adapter.Restart()
            if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                return result_code[0], message_or_unique_id[0]

            self.update_event_data_storage(
                self.component_manager.get_tmc_sdp_sln_device_name(),
                message_or_unique_id,
            )
        except Exception as exception:
            self.logger.exception(
                "Execution of Restart command is failed. "
                + "Reason: Restart command call failed on SDP "
                + f"Subarray Leaf Node: {exception}"
            )
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error while invoking Restart command on "
                + "SDP Subarray Leaf Node "
                + f"{sdp_sln_adapter.dev_name}: {exception}",
            )
        return ResultCode.OK, ""

    def restart_csp(self) -> Tuple[ResultCode, str]:
        """
        Restarts the CSP devices by invoking the Restart command on the
        CSP Subarray Leaf Node.

        :return: A tuple containing the result code and an empty string.
        :rtype: Tuple[ResultCode, str]
        """
        csp_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_csp_sln_device_name()
        )
        # Invoke Restart command on CSP Subarray Leaf Node.
        try:
            result_code, message_or_unique_id = csp_sln_adapter.Restart()
            if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                return result_code[0], message_or_unique_id[0]

            self.update_event_data_storage(
                self.component_manager.get_tmc_csp_sln_device_name(),
                message_or_unique_id,
            )
        except Exception as exception:
            self.logger.exception(
                "Execution of Restart command is failed."
                + "Reason: Restart command call failed on "
                + f"CSP Subarray Leaf Node: {exception}"
            )
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error while invoking Restart command on "
                + "CSP Subarray Leaf Node "
                + f"{csp_sln_adapter.dev_name}: {exception}",
            )
        return ResultCode.OK, ""

    def restart_dishes(self) -> Tuple[ResultCode, str]:
        """
        Restarts all Dish Leaf Nodes in the Subarray.

        :return: A tuple containing the result code and an empty string.
        :rtype: Tuple[ResultCode, str]
        """
        for adapter in self.component_manager.tmc_leaf_dish_adapters:
            try:
                result_code, message_or_unique_id = adapter.Restart()
                if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                    return result_code[0], message_or_unique_id[0]
            except Exception as exception:
                log_msg = (
                    "Execution of Restart command is failed. "
                    + "Reason: Restart command call failed on "
                    + f"Dish Leaf Node: {exception}"
                )
                self.logger.exception(log_msg)
                return self.component_manager.generate_command_result(
                    ResultCode.FAILED,
                    "Error while invoking Restart command on "
                    + f"Dish Leaf Node: {adapter.dev_name}: {exception}",
                )
        return ResultCode.OK, ""

    def clean_up_dishes(self) -> Tuple[ResultCode, str]:
        """
        Removes the dish devices from input parameter class. Also unsubscribes
        events for dish and dish leaf node attributes.

        :return: A tuple containing the result code and an empty string.
        :rtype: Tuple[ResultCode, str]
        """
        try:
            self.component_manager.clear_assigned_resources()
            self.logger.info("Clean Up complete.")

        except Exception as exception:
            self.logger.exception(
                "Execution of Restart command is failed. Reason: "
                + f"Failed to clear Dishes from Subarray Node: {exception}"
            )
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in clearing the Dishes from the Subarray Node: "
                + f"{exception}",
            )
        return ResultCode.OK, ""

    def clean_up_configuration(self) -> Tuple[ResultCode, str]:
        """
        Cleans up the configuration and resources of the Subarray.

        :return: A tuple containing the result code and an empty string.
        :rtype: Tuple[ResultCode, str]
        """
        try:
            # Update SubarrayNode assigned_resources attribute
            self.component_manager.set_assigned_resources([])
            self.logger.info("Clean Up complete.")

        except Exception as exception:
            self.logger.exception(
                "Execution of Restart command is failed. Reason: "
                + f"Failed to clear resources from Subarray Node: {exception}"
            )
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in clearing the Resources from the Subarray Node",
            )

        if self.component_manager.component_state_changed_callback is not None:
            self.component_manager.component_state_changed_callback(
                {"resourced": False}
            )

        try:
            self.component_manager.reset_sb_id()
        except Exception as exception:
            self.logger.exception(
                "Execution of Restart command is failed. Reason: "
                + f"Failed to reset sb_id on Subarray Node: {exception}"
            )
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in clearing the sb id in Restart command",
            )

        try:
            self.component_manager.reset_scan_duration()
        except Exception as exception:
            self.logger.exception(
                "Execution of Restart command is failed. "
                + "Reason: Failed to reset scan duration "
                + f"on Subarray Node: {exception}"
            )
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in clearing the scan duration in Restart command",
            )

        if self.component_manager.component_state_changed_callback is not None:
            self.component_manager.component_state_changed_callback(
                {"configured": False}
            )

        return ResultCode.OK, ""

    def do_low(self, argin: str = None) -> Tuple[ResultCode, str]:
        """
        Invokes the Restart command on CSP Subarray Leaf Node, SDP
        Subarray Leaf Node, and Mccs Subarray Leaf Node to restarts the
        respective devices.

        :param argin: Optional argument. Default is None.
        :type argin: str, optional
        :return: A tuple containing the result code and a message
            indicating the execution status.
        :rtype: Tuple[ResultCode, str]
        """
        self.update_command_in_progress_id("Restart")
        result_code, message = self.init_adapters()
        if result_code == ResultCode.FAILED:
            return result_code, message

        result_code, message = self.clean_up_configuration()

        if result_code == ResultCode.FAILED:
            return self.component_manager.generate_command_result(
                result_code,
                message,
            )

        # Get CspSubarrayLeafNode ObsState
        csp_subarray_leaf_node_obs_state = self._get_device_obstate(
            self.component_manager.get_tmc_csp_sln_device_name()
        )
        self.logger.info(
            f"CspSubarrayLeafNode obsState: {csp_subarray_leaf_node_obs_state}"
        )
        # Get SdpSubarrayLeafNode ObsState
        sdp_subarray_leaf_node_obs_state = self._get_device_obstate(
            self.component_manager.get_tmc_sdp_sln_device_name()
        )

        # Get MccsSubarrayLeafNode ObsState
        mccs_subarray_leaf_node_obs_state = self._get_device_obstate(
            self.component_manager.get_tmc_mccs_sln_device_name()
        )

        self.logger.info(
            f"MccsSubarrayLeafNode obsState: "
            f"{mccs_subarray_leaf_node_obs_state}"
        )

        if self.component_manager.component_state_changed_callback:
            self.component_manager.component_state_changed_callback(
                {"restart_invoked": None}
            )
        # check CspSubarrayLeafNode obsState = Aborted/Fault
        # before sending Restart
        # command
        if csp_subarray_leaf_node_obs_state in [
            ObsState.ABORTED,
            ObsState.FAULT,
        ]:
            result_code, message = self.restart_csp()
            if result_code == ResultCode.FAILED:
                return self.component_manager.generate_command_result(
                    result_code,
                    message,
                )
        else:
            self.logger.info(
                "Restart command is not invoked on Csp Subarray. "
                + "Csp Subarray Leaf Node obsstate"
                + f"is: {csp_subarray_leaf_node_obs_state}"
            )

        # check SDP subarray obsState = Aborted/Fault
        # before sending Restart command
        if sdp_subarray_leaf_node_obs_state in [
            ObsState.ABORTED,
            ObsState.FAULT,
        ]:
            result_code, message = self.restart_sdp()
            if result_code == ResultCode.FAILED:
                return self.component_manager.generate_command_result(
                    result_code,
                    message,
                )
        else:
            self.logger.info(
                "Restart command is not invoked on Sdp Subarray. "
                + "Sdp Subarray Leaf Node obsstate "
                + f"is: {sdp_subarray_leaf_node_obs_state}"
            )

        # check Mccs subarray obsState = Aborted/Fault
        # before sending Restart command
        if mccs_subarray_leaf_node_obs_state in [
            ObsState.ABORTED,
            ObsState.FAULT,
        ]:
            result_code, message = self.restart_mccs()
            if result_code == ResultCode.FAILED:
                return self.component_manager.generate_command_result(
                    result_code,
                    message,
                )
        else:
            self.logger.info(
                "Restart command is not invoked on Mccs Subarray. "
                + "Mccs Subarray Leaf Node obsstate "
                + f"is: {mccs_subarray_leaf_node_obs_state}"
            )

        return ResultCode.OK, COMMAND_COMPLETION_MESSAGE

    def restart_mccs(self) -> Tuple[ResultCode, str]:
        """
        Restarts the MCCS devices by invoking the Restart command on the
        MccsSubarrayLeafNode.

        Returns:
            A tuple containing the result code and an empty string.

            - ResultCode.OK: If the Restart command is executed successfully.
            - ResultCode.FAILED: If there is an error while invoking the
              Restart command.

        Raises:
            Exception: If the execution of the Restart command fails on the
            MccsSubarrayLeafNode.
        """
        mccs_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_mccs_sln_device_name()
        )
        # Invoke Restart command on MCCS Subarray Leaf Node.
        try:
            result_code, message_or_unique_id = mccs_sln_adapter.Restart()
            if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                return result_code[0], message_or_unique_id[0]

            self.update_event_data_storage(
                self.component_manager.get_tmc_mccs_sln_device_name(),
                message_or_unique_id,
            )
        except Exception as exception:
            error_message = (
                f"Failed to execute Restart command on "
                f"Mccs Subarray Leaf Node: {exception}. "
            )
            self.logger.exception(error_message)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED, error_message
            )

        return ResultCode.OK, ""
