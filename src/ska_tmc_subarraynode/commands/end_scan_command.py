"""
A class for TMC SubarrayNode's EndScan() command.
"""


import time
from typing import Callable, Optional, Tuple

from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import TimeKeeper, TimeoutCallback, TimeoutState
from ska_tmc_common.v1.error_propagation_tracker import (
    error_propagation_tracker,
)
from ska_tmc_common.v1.timeout_tracker import timeout_tracker

from ska_tmc_subarraynode.commands.subarray_node_command import (
    SubarrayNodeCommand,
)
from ska_tmc_subarraynode.model.input import InputParameterMid
from ska_tmc_subarraynode.utils.constants import COMMAND_COMPLETION_MESSAGE


class EndScan(SubarrayNodeCommand):
    """
    A class for SubarrayNode's EndScan() command.

    Ends the scan. It is invoked on subarray after
    completion of the scan duration. It can
    also be invoked by an external client while a scan is in progress,
    Which stops the scan
    immediately irrespective of the provided scan duration.

    """

    def __init__(
        self,
        component_manager,
        obs_state_model,
        adapter_factory,
        logger,
        **kwargs,
    ):
        super().__init__(
            component_manager=component_manager,
            adapter_factory=adapter_factory,
            logger=logger,
        )
        self.obs_state_model = obs_state_model
        self.__error_message_list: list = []
        self.is_only_on_trajectory_dishes = kwargs.get(
            "is_only_on_trajectory_dishes", False
        )

        self.timeout_id = f"{time.time()}_{__class__.__name__}"
        self.timekeeper = TimeKeeper(
            self.component_manager.command_timeout, logger
        )
        self.timeout_callback: Callable[
            [str, TimeoutState], Optional[ValueError]
        ] = TimeoutCallback(self.timeout_id, self.logger)

    @timeout_tracker
    @error_propagation_tracker(
        "get_subarray_obsstate", [ObsState.SCANNING, ObsState.READY]
    )
    def invoke_end_scan(
        self,
    ):
        """This is a long running method for Scan command,
        it executes do hook, invokes EndScan command on CspSubarrayleafnode
        and SdpSubarrayleafnode.

        :param logger: logger
        :type logger: logging.Logger
        :type task_callback: TaskCallbackType, optional
        :param task_abort_event: Check for abort, defaults to None
        :type task_abort_event: Event, optional
        """

        self.obs_state_model.perform_action("end_scan_invoked")

        with self.component_manager.event_data_manager.eventlock:
            self.component_manager.event_data_manager.clear_lrcr()

        return self.do()

    def do_mid(self, argin: str = None) -> Tuple[ResultCode, str]:
        """
        Method to invoke Endscan command.

        return: None

        raises: The command execution is not successful.
        """
        self.update_command_in_progress_id("EndScan")
        self.logger.info("command_id %s", self.component_manager.command_id)

        result_code, message = self.init_adapters()
        if result_code == ResultCode.FAILED:
            return result_code, message

        self.logger.info("Ending scan timer")
        if self.component_manager.is_scan_timer_running():
            self.component_manager.stop_scan_timer()

        # Stop the current ongoing mapping scan
        if self.component_manager.scan_obj:
            self.component_manager.scan_obj.clear_data()

        result_code, message = self.end_scan_mid()
        if result_code == ResultCode.FAILED:
            return self.component_manager.generate_command_result(
                result_code,
                message,
            )

        self.logger.info("EndScan command is invoked.")
        return (ResultCode.OK, COMMAND_COMPLETION_MESSAGE)

    def end_mapping_scan(self) -> None:
        """
        This method will be invoked to end the scan during mapping Scan.
        """
        with self.component_manager.event_data_manager.eventlock:
            self.component_manager.event_data_manager.clear_lrcr()
        self.component_manager.command_in_progress = "EndScan"
        if isinstance(
            self.component_manager.input_parameter, InputParameterMid
        ):
            self.end_scan_mid()
        else:
            self.end_scan_low()

    @timeout_tracker
    @error_propagation_tracker("get_subarray_obsstate", [ObsState.READY])
    def end_scan(
        self,
    ) -> None:
        """
        This method will be invoked to end the scan during regular Scan.
        """

        self.update_command_in_progress_id("EndScan")

        self.logger.info("command_id %s", self.component_manager.command_id)
        with self.component_manager.event_data_manager.eventlock:
            self.component_manager.event_data_manager.clear_lrcr()
        self.component_manager.command_in_progress = "EndScan"

        if isinstance(
            self.component_manager.input_parameter, InputParameterMid
        ):
            return self.end_scan_mid()

        return self.end_scan_low()

    def end_scan_mid(self) -> Tuple[ResultCode, str]:
        """
        Method to invoke EndScan command on TMC-mid subsystems

        :return: (ResultCode, message)
        """
        self.component_manager.reset_scan_id()
        for method in [
            self.endscan_dishes,
            self.endscan_sdp,
            self.endscan_csp,
        ]:
            method()
            self.logger.debug(f"Execution of {method.__name__} is completed.")

        if len(self.__error_message_list) != 0:
            error_message = (
                "Error while invoking EndScan on the following devices: "
                + ", ".join(self.__error_message_list)
            )
            return ResultCode.FAILED, error_message
        return ResultCode.OK, ""

    def endscan_sdp(self) -> Tuple[ResultCode, str]:
        """
        Method to invoke EndScan command on SDP Subarray Leaf Node.

        :return: (ResultCode, message)
        """

        tmc_sdp_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_sdp_sln_device_name()
        )
        try:
            result_code, message_or_unique_id = tmc_sdp_sln_adapter.EndScan()
            if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                self.__error_message_list.append(
                    f"{tmc_sdp_sln_adapter.dev_name}: "
                    + f"[{result_code[0]}, {message_or_unique_id[0]}]"
                )
                return result_code[0], message_or_unique_id[0]

            self.update_event_data_storage(
                self.component_manager.get_tmc_sdp_sln_device_name(),
                message_or_unique_id,
            )
        except Exception as exception:
            self.logger.exception(
                "Execution of EndScan command is failed. "
                + "Reason: Error while invoking EndScan on %s: %s",
                tmc_sdp_sln_adapter.dev_name,
                exception,
            )
            self.__error_message_list.append(
                f"{self.component_manager.get_tmc_sdp_sln_device_name()}: "
                + f"[{ResultCode.FAILED}, {str(exception)}]"
            )
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error while invoking EndScan in SDP Subarray Leaf Node: "
                + f"{tmc_sdp_sln_adapter.dev_name}: {exception}",
            )

        return ResultCode.OK, ""

    def endscan_csp(self) -> Tuple[ResultCode, str]:
        """
        Method to invoke EndScan command on CSP Subarray Leaf Node.

        :return: (ResultCode, message)
        """

        tmc_csp_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_csp_sln_device_name()
        )
        try:
            result_code, message_or_unique_id = tmc_csp_sln_adapter.EndScan()
            if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                self.__error_message_list.append(
                    f"{tmc_csp_sln_adapter.dev_name}: "
                    + f"[{result_code[0]}, {message_or_unique_id[0]}]"
                )
                return result_code[0], message_or_unique_id[0]

            self.update_event_data_storage(
                self.component_manager.get_tmc_csp_sln_device_name(),
                message_or_unique_id,
            )
        except Exception as exception:
            self.logger.exception(
                "Execution of EndScan command is failed."
                + "Reason: Error while invoking EndScan on %s: %s",
                tmc_csp_sln_adapter.dev_name,
                exception,
            )
            self.__error_message_list.append(
                f"{self.component_manager.get_tmc_csp_sln_device_name()}: "
                + f"[{ResultCode.FAILED}, {str(exception)}]"
            )
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error while invoking EndScan in CSP Subarray Leaf Node: "
                + f"{tmc_csp_sln_adapter.dev_name}: {exception}",
            )

        return ResultCode.OK, ""

    def endscan_dishes(self) -> Tuple[ResultCode, str]:
        """
        Method to invoke EndScan command on Dish Leaf Nodes.

        :return: (ResultCode, message)
        """
        if self.is_only_on_trajectory_dishes:
            self.logger.debug("Using Trajectory dishes")
            tmc_leaf_dish_adapters = self.get_dish_adapter_by_device_name(
                self.component_manager.get_trajectory_dish_device_names()
            )
        else:
            tmc_leaf_dish_adapters = self.get_dish_adapter_by_device_name(
                self.component_manager.get_tmc_dish_ln_device_names()
            )
        for dish_leaf_node_adapter in tmc_leaf_dish_adapters:
            try:
                self.logger.info(
                    "Invoking End Scan on %s and receptor %s",
                    dish_leaf_node_adapter.dev_name,
                    tmc_leaf_dish_adapters,
                )
                (
                    result_code,
                    message_or_unique_id,
                ) = dish_leaf_node_adapter.EndScan()
                if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                    self.__error_message_list.append(
                        f"{dish_leaf_node_adapter.dev_name}: "
                        + f"[{result_code[0]}, {message_or_unique_id[0]}]"
                    )
                    continue

                self.update_event_data_storage(
                    dish_leaf_node_adapter.dev_name,
                    message_or_unique_id,
                )
            except Exception as exception:
                self.logger.exception(
                    "Execution of EndScan command has failed. "
                    + "Reason: Error while invoking EndScan on %s: %s",
                    dish_leaf_node_adapter.dev_name,
                    exception,
                )
                self.__error_message_list.append(
                    f"{dish_leaf_node_adapter.dev_name}: "
                    + f"[{ResultCode.FAILED}, {str(exception)}]"
                )
                return self.component_manager.generate_command_result(
                    ResultCode.FAILED,
                    "Error while invoking EndScan on Dish Leaf Node"
                    + f" -> {dish_leaf_node_adapter.dev_name}: {exception}",
                )

        return ResultCode.OK, ""

    def do_low(self, argin: str = None) -> Tuple[ResultCode, str]:
        """
        This method executes the End Scan workflow of the Subarray Node Low.
        It will invoke End Scan command on the CSP
        Subarray Leaf Node, SDP Subarray Leaf Node.

        return: None

        raises: DevFailed if the command execution is not successful.
        """

        self.update_command_in_progress_id("EndScan")

        result_code, message = self.init_adapters()
        if result_code == ResultCode.FAILED:
            return result_code, message

        self.logger.info("Ending scan timer")
        if self.component_manager.is_scan_timer_running():
            self.component_manager.stop_scan_timer()

        result_code, message = self.end_scan_low()
        if result_code == ResultCode.FAILED:
            return self.component_manager.generate_command_result(
                result_code,
                message,
            )

        return (ResultCode.OK, COMMAND_COMPLETION_MESSAGE)

    def end_scan_low(self) -> Tuple[ResultCode, str]:
        """
        Setting up device for low csp, sdp and mccs
        """
        for method in [self.endscan_sdp, self.endscan_csp, self.end_scan_mccs]:
            method()
            self.logger.debug(f"Execution of {method.__name__} is completed.")

        if len(self.__error_message_list) != 0:
            error_message = (
                "Error while invoking EndScan on the following devices: "
                + ", ".join(self.__error_message_list)
            )
            return ResultCode.FAILED, error_message
        return ResultCode.OK, ""

    def end_scan_mccs(self) -> Tuple[ResultCode, str]:
        """
        Ends the scanning process on the Mccs Subarray Leaf Node.

        This method sends the EndScan command to the Mccs Subarray Leaf Node,
        indicating the completion of the scanning process.

        :return: A tuple containing a return code and a string message
            indicating the status. The message is for information purposes only
        :rtype: Tuple[ResultCode, str]
        """
        tmc_mccs_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_mccs_sln_device_name()
        )
        try:
            result_code, message_or_unique_id = tmc_mccs_sln_adapter.EndScan()
            if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                self.__error_message_list.append(
                    f"{tmc_mccs_sln_adapter.dev_name}: "
                    + f"[{result_code[0]}, {message_or_unique_id[0]}]"
                )
                return result_code[0], message_or_unique_id[0]

            self.update_event_data_storage(
                self.component_manager.get_tmc_mccs_sln_device_name(),
                message_or_unique_id,
            )
        except Exception as exception:
            self.logger.exception(
                "Execution of EndScan command is failed. "
                + "Reason: Error while invoking EndScan on %s: %s",
                tmc_mccs_sln_adapter.dev_name,
                exception,
            )
            self.__error_message_list.append(
                f"{self.component_manager.get_tmc_mccs_sln_device_name()}: "
                + f"[{ResultCode.FAILED}, {exception}]"
            )
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in calling EndScan on MCCS Subarray Leaf Node "
                + f"{tmc_mccs_sln_adapter.dev_name}: {exception}",
            )
        return ResultCode.OK, ""

    def update_task_status(self, **kwargs) -> None:
        """Method to update task status with result code and exception message
        if any."""

        result = kwargs.get("result")
        status = kwargs.get("status", TaskStatus.COMPLETED)
        message = kwargs.get("message") or kwargs.get("exception")

        self.logger.info("Updating task status with kwargs: %s", kwargs)

        if status == TaskStatus.ABORTED:
            self.task_callback(
                result=(ResultCode.ABORTED, "Command has been aborted"),
                status=status,
            )
            return
        if result[0] == ResultCode.OK:
            self.logger.info(
                "command_in_progress - %s",
                self.component_manager.command_in_progress,
            )
            self.task_callback(result=result, status=status)
        else:
            self.task_callback(
                result=result,
                status=status,
                exception=Exception(message),
            )

            self.component_manager.command_in_progress = ""
            self.component_manager.command_in_progress_id = None
            self.component_manager.command_result_code = None

            with self.component_manager.event_data_manager.eventlock:
                self.component_manager.event_data_manager.clear_lrcr()

        self.component_manager.all_lrcr_ok = False
        self.component_manager.command_id = ""
