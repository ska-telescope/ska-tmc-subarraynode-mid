"""
SubarrayNode command class for SubarrayNode, a subclass of TMCCommand.
This class provides
methods to initialize adapters and executing commands
on subarray devices.
"""
import json
import logging
from datetime import datetime
from typing import Any, List, Optional, Tuple

from ska_ser_logging.configuration import configure_logging
from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import (
    AdapterFactory,
    AdapterType,
    PointingState,
    TMCCommand,
)

from ska_tmc_subarraynode.model.input import InputParameterMid

configure_logging()
LOGGER = logging.getLogger(__name__)


class SubarrayNodeCommand(TMCCommand):
    """This code defines the SubarrayNodeCommand class,
    which is a subclass of TMCCommand. This class provides
    methods for initializing adapters and executing commands
    on subarray devices.
    """

    def __init__(
        self,
        component_manager,
        adapter_factory=AdapterFactory(),
        logger: logging.Logger = LOGGER,
    ):
        super().__init__(component_manager, logger=logger)
        self.adapter_factory = adapter_factory
        self.task_callback: TaskCallbackType = self.task_callback_default

    def init_adapters(self) -> Tuple[ResultCode, str]:
        if isinstance(
            self.component_manager.input_parameter, InputParameterMid
        ):
            result, message = self.init_adapters_mid()
        else:
            result, message = self.init_adapters_low()

        return result, message

    def adapter_error_message_result(
        self, dev_name: str, exception
    ) -> Tuple[ResultCode, str]:
        """Returns ResultCode.FAILED with failure message as a tuple"""
        return (
            ResultCode.FAILED,
            f"Adapter creation failed for {dev_name}: {exception}",
        )

    def init_adapters_mid(self) -> Tuple[ResultCode, str]:
        try:
            self.adapter_factory.get_or_create_adapter(
                self.component_manager.get_tmc_csp_sln_device_name(),
                AdapterType.SUBARRAY,
            )
        except Exception as e:
            return self.adapter_error_message_result(
                self.component_manager.get_tmc_csp_sln_device_name(),
                e,
            )
        try:
            self.adapter_factory.get_or_create_adapter(
                self.component_manager.get_tmc_sdp_sln_device_name(),
                AdapterType.SUBARRAY,
            )
        except Exception as e:
            return self.adapter_error_message_result(
                self.component_manager.get_tmc_sdp_sln_device_name(),
                e,
            )

        # In case of On, Off, Standby, AssignResources, ReleaseAllResources,
        # Scan and EndScan commands, SubarrayNode does not invoke respective
        # commands on the Dish devices so Dish Leaf Node adapters are not
        # required.
        if self.component_manager.command_in_progress not in [
            "On",
            "Off",
            "Standby",
            "AssignResources",
            "ReleaseAllResources",
        ]:
            error_dev_names = []
            running_dish_leaf_dev_name = []
            num_working_dishes = 0
            for (
                dev_name
            ) in self.component_manager.get_tmc_dish_ln_device_names():
                self.logger.debug("Checking for dev_name %s", dev_name)
                dev_info = self.component_manager.get_device(dev_name)
                if not dev_info.unresponsive:
                    if dev_name not in running_dish_leaf_dev_name:
                        running_dish_leaf_dev_name.append(dev_name)
                        try:
                            self.adapter_factory.get_or_create_adapter(
                                dev_name, AdapterType.DISH
                            )
                            num_working_dishes += 1
                        except Exception as e:
                            self.logger.error(
                                "Error in creating adapter for %s: %s",
                                dev_name,
                                e,
                            )
                            error_dev_names.append(dev_name)

                if (
                    num_working_dishes == 0
                    and self.component_manager.assigned_resources != []
                ):
                    return self.component_manager.generate_command_result(
                        ResultCode.FAILED,
                        f"Error in creating dish adapters \
                        {'.'.join(error_dev_names)}",
                    )

        return ResultCode.OK, ""

    def init_adapters_low(self) -> Tuple[ResultCode, str]:
        try:
            self.adapter_factory.get_or_create_adapter(
                self.component_manager.get_tmc_mccs_sln_device_name(),
                AdapterType.SUBARRAY,
            )
        except Exception as e:
            return self.adapter_error_message_result(
                self.component_manager.get_tmc_mccs_sln_device_name(),
                e,
            )

        try:
            self.adapter_factory.get_or_create_adapter(
                self.component_manager.get_tmc_csp_sln_device_name(),
                AdapterType.SUBARRAY,
            )
        except Exception as e:
            return self.adapter_error_message_result(
                self.component_manager.get_tmc_csp_sln_device_name(),
                e,
            )

        try:
            self.adapter_factory.get_or_create_adapter(
                self.component_manager.get_tmc_sdp_sln_device_name(),
                AdapterType.SUBARRAY,
            )
        except Exception as e:
            return self.adapter_error_message_result(
                self.component_manager.get_tmc_sdp_sln_device_name(),
                e,
            )

        return ResultCode.OK, ""

    def do(self, argin: str = None) -> Tuple[ResultCode, str]:
        current_time = datetime.utcnow()
        self.component_manager.command_timestamp = current_time

        if isinstance(
            self.component_manager.input_parameter, InputParameterMid
        ):
            result = self.do_mid(argin)
        else:
            result = self.do_low(argin)

        return result

    def get_adapter_by_device_name(self, device_name: str):
        """
        The get_adapter_by_device_name method takes a device_name as
            input and searches for an adapter object in the adapter_factory
            object's adapters attribute that matches the input device_name.
            If a matching adapter object is found, it is returned. If no
            matching adapter object is found, None is returned.

        Args: device_name (str): The name of the device to search for.

        Returns:
            An adapter object if a matching device is found, otherwise None."""
        for adapter in self.adapter_factory.adapters:
            if adapter.dev_name == device_name:
                return adapter
        return self.adapter_factory.adapters[0]

    def get_dish_adapter_by_device_name(self, device_name_list: List[str]):
        """The get_dish_adapter_by_device_name method takes a list of
            device_name_list as input and searches for adapter objects in
            the adapter_factory object's adapters attribute that match any
            of the device names in the input list. It returns a list of
            matching adapter objects.

        Args: device_name_list (list): A list of device names to search for.

        Returns:
            A list of adapter objects that match the device
            names in the input list.
            If no matching adapter object is found, an empty list is
            returned."""
        adapter_list = []
        for adapter in self.adapter_factory.adapters:
            if adapter.dev_name in device_name_list:
                adapter_list.append(adapter)
        return adapter_list

    def update_command_in_progress_id(self, command_name: str) -> None:
        """The update_command_in_progress_id method updates
            command_in_progress_id
            attributes of the component_manager
            object with the given command_name.
            It also logs information about the updated command.

        Args: command_name (str): The name of the command being updated.
        Returns: None"""
        command_statuses = (
            self.component_manager.command_tracker.command_statuses
        )
        self.logger.debug(
            "The status of commands in queue are : %s", command_statuses
        )
        for command in reversed(command_statuses):
            if command_name in command[0]:
                self.component_manager.command_in_progress_id = command[0]
                break

        self.logger.info(
            f"Command in progress: \
            {self.component_manager.command_in_progress} "
            + f"Command in progress id: \
            {self.component_manager.command_in_progress_id}"
        )

    def reject_command(self, message: str) -> Tuple[ResultCode, str]:
        """
        Method to return task status as TaskStatus.REJECTED along with the
        error message
        """

        self.logger.error(
            "Command execution failed due to reason : %s",
            message,
        )
        return TaskStatus.REJECTED, message

    def _get_device_obstate(self, dev_name: str) -> ObsState:
        """Return obstate of subarray obstate"""
        device_info = self.component_manager.get_device(dev_name)
        if device_info:
            return device_info.obs_state
        return None

    def _get_device_pointing_state(self, dev_name: str) -> PointingState:
        """Return pointing state of the dish"""
        device_info = self.component_manager.get_device(dev_name)
        return device_info.pointing_state

    def clear_device_events(self) -> None:
        """Clears the device events dictionary"""
        self.logger.info("Clearing the device events dictionary")
        self.component_manager.error_dict.clear()
        self.component_manager.device_events.clear()
        self.component_manager.dish_unique_ids.clear()
        with self.component_manager.event_data_manager.eventlock:
            self.component_manager.event_data_manager.clear_lrcr()

    def do_mid(self, argin: Optional[Any] = None) -> Tuple[ResultCode, str]:
        """Abstract method from TMCCommand is
            defined here but not utilized by this Class.

        Args: argin (_type_, optional): Accepts argument if required.
            Defaults to None.
        """
        return ResultCode.OK, ""

    def do_low(self, argin: Optional[Any] = None) -> Tuple[ResultCode, str]:
        """Abstract method from TMCCommand is
            defined here but not utilized by this Class.

        Args: argin (_type_, optional): Accepts argument if required.
            Defaults to None.
        """
        return ResultCode.OK, ""

    def update_event_data_storage(self, device_name, unique_command_id):
        """
        This method populates the command_id in eventdatastorage.
        :param device_name: device name
        :type device_name: str
        :param unique_command_id: command id
        :type unique_command_id: str
        """
        self.component_manager.event_data_manager.update_event_data(
            device=device_name,
            data=(unique_command_id, json.dumps([ResultCode.UNKNOWN, ""])),
            received_timestamp=None,
            data_type="CommandResultData",
        )

    def update_task_status(self, **kwargs):
        """Abstract method from TMCCommand is
        defined here but not utilized by this Class."""

    def task_callback_default(
        # status: TaskStatus | None = None,
        # progress: int | None = None,
        # result: Any = None,
        # exception: Exception | None = None,
        self,
        **kwargs,
    ) -> None:
        """
        Default method if the taskcallback is not passed

        :param status: status of the task.
        :param progress: progress of the task.
        :param result: result of the task.
        :param exception: an exception raised from the task.
        """

        result = kwargs.get("result", None)
        status = kwargs.get("status", TaskStatus.COMPLETED)
        message = kwargs.get("message") or kwargs.get("exception", None)
        progress = kwargs.get("progress") or kwargs.get("exception", None)

        LOGGER.warning(
            "This is default task callback."
            + "There is no action taken under this callback."
            + "Please provide task callback."
        )
        LOGGER.info(
            "long running command status: %s, progress: %s ,result:%s ,"
            + "exception %s",
            status,
            progress,
            result,
            message,
        )
