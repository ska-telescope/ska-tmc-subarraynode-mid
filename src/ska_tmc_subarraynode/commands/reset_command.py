"""
Reset Command for SubarrayNode.
"""
from typing import Tuple

from ska_tango_base import SKABaseDevice
from ska_tango_base.commands import ResultCode
from ska_tmc_common.adapters import AdapterFactory
from tango import DevState

from ska_tmc_subarraynode.model.input import InputParameterMid


class Reset(SKABaseDevice.ResetCommand):
    """
    A class for SubarrayNode's Reset() command.
    """

    def __init__(
        self,
        target,
        op_state_model,
        adapter_factory=AdapterFactory(),
        logger=None,
    ):
        super().__init__(
            target=target,
            op_state_model=op_state_model,
            logger=logger,
        )
        self.component_manager = target
        self._adapter_factory = adapter_factory
        self.op_state_model = op_state_model

    def do_mid(self, argin: str = None) -> Tuple[ResultCode, str]:
        """ "
        Method to invoke Reset command on SubarrayNode.

        return:
            A tuple containing a return code and a
            string message indicating execution status of command.

        rtype:
            (ResultCode, str)

        raises:
            Exception.
        """
        self.logger.info("Resetting timer")
        try:
            if self.component_manager.is_scan_timer_running():
                self.component_manager.stop_scan_timer()
        except Exception as exception:
            log_msg = (
                "Execution of Reset command is failed."
                + f"Reason: Failed to stop scan timer: {exception}"
                + "The command is not executed successfully."
                + "The device will continue with normal operation"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in stopping scan timer",
            )
        try:
            self.component_manager.reset_sb_id()
        except Exception as e:
            log_msg = (
                "Execution of Reset command is failed."
                + f"Reason: Failed to reset sb_id on SubarrayNode: {e}"
                + "The command is not executed successfully."
                + "The device will continue with normal operation"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in clearing the sb id in Reset command",
            )

        try:
            self.component_manager.reset_scan_duration()
        except Exception as exception:
            log_msg = (
                "Execution of Reset command is failed."
                + "Reason: Failed to reset scan duration on"
                + f"SubarrayNode: {exception}"
                + "The command is not executed successfully."
                + "The device will continue with normal operation"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in resetting the scan duration in Reset command",
            )

        self.logger.info("Reset command is invoked.")
        return (ResultCode.OK, "")

    def do_low(self, argin=None) -> Tuple[ResultCode, str]:
        """ "
        Method to invoke Reset command on TMC Low Devices.

        return:
            A tuple containing a return code and
            a string message indicating execution status of command.


        rtype:
            (ResultCode, str)

        raises:
            DevFailed.
        """

        self.logger.info("Resetting timer")
        try:
            if self.component_manager.is_scan_timer_running():
                self.component_manager.stop_scan_timer()
        except Exception as exception:
            log_msg = (
                "Execution of Reset command is failed."
                + f"Reason: Failed to stop scan timer: {exception}"
                + "The command is not executed successfully."
                + "The device will continue with normal operation"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in stopping scan timer",
            )

        try:
            self.component_manager.reset_scan_duration()
        except Exception as e:
            log_msg = (
                "Execution of Reset command is failed."
                + f"Reason: Failed to reset scan duration on SubarrayNode: {e}"
                + "The command is not executed successfully."
                + "The device will continue with normal operation"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in resetting the scan duration in Reset command",
            )

        self.logger.info("Reset command is invoked.")
        return (ResultCode.OK, "")

    def is_allowed_mid(self, raise_if_disallowed: bool) -> bool:
        """
        Whether this command is allowed to run in
        the current state of the state model.

        :param raise_if_disallowed: whether to raise an error or
            simply return False if the command is disallowed

        :returns: whether this command is allowed to run
        :rtype: boolean
        """
        super().is_allowed(raise_if_disallowed=raise_if_disallowed)

        if self.op_state_model.op_state in [
            DevState.OFF,
            DevState.DISABLE,
            DevState.ON,
        ]:
            self.logger.info(
                "Reset command is not allowed in current \
                                    operational state %s"
                + self.op_state_model.op_state
            )
            return False
        return True

    def is_allowed_low(self, raise_if_disallowed: bool) -> bool:
        """
        Whether this command is allowed to run in
        the current state of the state model.

        :param raise_if_disallowed: whether to raise an error or
            simply return False if the command is disallowed

        :returns: whether this command is allowed to run
        :rtype: boolean
        """
        super().is_allowed(raise_if_disallowed=raise_if_disallowed)
        if self.op_state_model.op_state in [
            DevState.OFF,
            DevState.DISABLE,
            DevState.ON,
        ]:
            self.logger.info(
                "Reset command is not allowed in current \
                                    operational state %s"
                + self.op_state_model.op_state
            )
            return False
        return True

    def is_allowed(self, raise_if_disallowed: bool = True) -> True:
        """
        check if component manager is of instance of mid or low
        """
        if isinstance(
            self.component_manager._input_parameter, InputParameterMid
        ):
            result = self.is_allowed_mid(raise_if_disallowed)
        else:
            result = self.is_allowed_low(raise_if_disallowed)

        return result

    def do(self, argin: str = None) -> Tuple[ResultCode, str]:
        """
        do method for reset command.
        """
        if isinstance(
            self.component_manager.input_parameter, InputParameterMid
        ):
            result = self.do_mid()
        else:
            result = self.do_low()

        return result
