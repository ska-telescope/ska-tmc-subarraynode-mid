"""
Subarray Node Low provides the monitoring and control
interface required by users as well as other
TM Components (such as OET, Central Node) for a Subarray Low.
"""
# pylint: disable=attribute-defined-outside-init
from ska_tango_base.commands import ResultCode
from tango.server import device_property, run

from ska_tmc_subarraynode.manager.subarray_node_component_manager_low import (
    SubarrayNodeComponentManagerLow,
)
from ska_tmc_subarraynode.model.input import InputParameterLow
from ska_tmc_subarraynode.subarray_node import BaseSubarrayNode

__all__ = ["LowTmcSubarray", "main"]


class LowTmcSubarray(BaseSubarrayNode):
    """
    Provides the monitoring and control interface required by users as well as
    other TM Components (such as OET, Central Node) for a Subarray.

    :Device Properties:

        MccsSubarrayLNFQDN:
            This property contains the FQDN of the MCCS Subarray Leaf Node
            associated with the Subarray Node.

        MccsSubarrayFQDN:
            This property contains the FQDN of the MCCS Subarray
            associated with the Subarray Node.

    :Device Attributes:
    """

    # -----------------
    # Device Properties
    # -----------------

    MccsSubarrayLNFQDN = device_property(
        dtype="str",
        doc="This property contains the FQDN of the MCCS"
        "Subarray Leaf Node associated with the "
        "Subarray Node.",
    )

    MccsSubarrayFQDN = device_property(
        dtype="str",
        doc="This property contains the FQDN of the"
        "MCCS Subarray associated with the "
        "Subarray Node.",
    )

    MccsConfigureInterfaceURL = device_property(
        dtype="str",
        default_value="https://schema.skao.int/ska-low-mccs-configure/1.0",
        doc="This property contains the interface URL of the"
        "MCCS sub-system for Configure command",
    )

    MccsScanInterfaceURL = device_property(
        dtype="str",
        default_value="https://schema.skao.int/ska-low-mccs-scan/3.0",
        doc="This property contains the interface URL of the"
        "MCCS sub-system for Scan command",
    )

    # ---------------
    # General methods
    # ---------------

    class InitCommand(BaseSubarrayNode.InitCommand):
        """
        A class for the TMC LowTmcSubarray's init_device() method.
        """

        def do(self):
            """
            Initializes the attributes and properties of the Subarray Node Mid.

            return:
                A tuple containing a return code and a string
                message indicating status.
                The message is for information purpose only.

            rtype:
                (ReturnCode, str)

            raises:
                DevFailed if the error while subscribing the tango attribute
            """
            super().do()
            return ResultCode.OK, ""

    # --------
    # Commands
    # --------

    def create_component_manager(self):
        """create instance of component manager"""
        super().create_component_manager()

        cm = SubarrayNodeComponentManagerLow(
            self.op_state_model,
            self.obs_state_model,
            self._command_tracker,
            dev_family=self.DevFamily,
            logger=self.logger,
            _update_device_callback=self.update_device_callback,
            _update_subarray_health_state_callback=(
                self.update_subarray_health_state_callback
            ),
            _update_assigned_resources_callback=(
                self.update_assigned_resources_callback
            ),
            _update_subarray_availability_status_callback=(
                self.update_subarray_availability_status_callback
            ),
            communication_state_changed_callback=None,
            component_state_changed_callback=(
                self._component_state_changed_callback
            ),
            command_timeout=self.CommandTimeOut,
            abort_command_timeout=self.AbortCommandTimeOut,
            proxy_timeout=self.ProxyTimeout,
            _input_parameter=InputParameterLow(None),
            event_subscription_check_period=self.EventSubscriptionCheckPeriod,
            liveliness_check_period=self.LivelinessCheckPeriod,
            csp_assign_interface=self.CspAssignResourcesInterfaceURL,
            csp_scan_interface=self.CspScanInterfaceURL,
            sdp_scan_interface=self.SdpScanInterfaceURL,
            mccs_configure_interface=self.MccsConfigureInterfaceURL,
            mccs_scan_interface=self.MccsScanInterfaceURL,
            jones_uri=self.JonesURI,
        )
        cm.input_parameter.tmc_mccs_sln_device_name = (
            self.MccsSubarrayLNFQDN or ""
        )
        cm.input_parameter.mccs_subarray_dev_name = self.MccsSubarrayFQDN or ""
        cm.input_parameter.tmc_csp_sln_device_name = (
            self.CspSubarrayLNFQDN or ""
        )
        cm.input_parameter.tmc_sdp_sln_device_name = (
            self.SdpSubarrayLNFQDN or ""
        )
        cm.input_parameter._csp_subarray_dev_name = self.CspSubarrayFQDN or ""
        cm.input_parameter._sdp_subarray_dev_name = self.SdpSubarrayFQDN or ""
        cm.update_input_parameter()
        return cm


# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    """
    Runs the LowTmcSubarray.
    :param args: Arguments internal to TANGO
    :param kwargs: Arguments internal to TANGO
    :return: LowTmcSubarray TANGO object.
    """
    return run((LowTmcSubarray,), args=args, **kwargs)


if __name__ == "__main__":
    main()
