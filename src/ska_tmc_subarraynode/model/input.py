"""
This module provide Provides input parameters
to both the telescopes
"""

import logging
from typing import List

logger = logging.getLogger(__name__)
# pylint:disable=W1203


class InputParameter:
    """
    Method to Check TMC Subarray Devices
    """

    def __init__(self, changed_callback) -> None:
        self._changed_callback = changed_callback
        self._tmc_csp_sln_device_name = ""
        self._tmc_sdp_sln_device_name = ""
        self._csp_subarray_dev_name = ""
        self._sdp_subarray_dev_name = ""

    @property
    def tmc_csp_sln_device_name(self) -> str:
        """
        Return the CSP Subarray Leaf Node device names

        :return: the CSP Subarray Leaf Node device names
        :rtype: str
        """
        return self._tmc_csp_sln_device_name

    @tmc_csp_sln_device_name.setter
    def tmc_csp_sln_device_name(self, value: str) -> None:
        """
        Set the CSP Subarray Leaf Node device names to be
        managed by the SubarrayNode

        :param value: the CSP Subarray Leaf Node device names
        :type value: str
        """
        self._tmc_csp_sln_device_name = value
        if self._changed_callback is not None:
            self._changed_callback()

    @property
    def tmc_sdp_sln_device_name(self) -> str:
        """
        Return the SDP Subarray Leaf Node device names

        :return: the SDP Subarray Leaf Node device names
        :rtype: str
        """
        return self._tmc_sdp_sln_device_name

    @tmc_sdp_sln_device_name.setter
    def tmc_sdp_sln_device_name(self, value: str) -> None:
        """
        Set the SDP Subarray Leaf Node device names to be
        managed by the SubarrayNode

        :param value: the SDP Subarray Leaf Node device names
        :type value: str
        """
        self._tmc_sdp_sln_device_name = value
        if self._changed_callback is not None:
            self._changed_callback()

    @property
    def csp_subarray_dev_name(self) -> str:
        """
        Return the CSP Subarray device name

        :return: the CSP Subarray device name
        :rtype: str
        """
        return self._csp_subarray_dev_name

    @csp_subarray_dev_name.setter
    def csp_subarray_dev_name(self, value: str) -> None:
        """
        Set the CSP Subarray device names to be
        managed by the SubarrayNode

        :param value: the CSP Subarray device names
        :type value: str
        """
        self._csp_subarray_dev_name = value
        if self._changed_callback is not None:
            self._changed_callback()

    @property
    def sdp_subarray_dev_name(self) -> None:
        """
        Returns the SDP Subarray device name

        :return: the SDP Subarray device name
        :rtype: str
        """
        return self._sdp_subarray_dev_name

    @sdp_subarray_dev_name.setter
    def sdp_subarray_dev_name(self, value: str) -> None:
        """
        Set the SDP Subarray device names to be
        managed by the SubarrayNode

        :param value: the SDP Subarray device names
        :type value: str
        """
        self._sdp_subarray_dev_name = value
        if self._changed_callback is not None:
            self._changed_callback()

    def update(self, component_manager) -> List[str]:
        """update the devices in liveliness probe"""
        list_dev_names = []
        device_name = self.tmc_csp_sln_device_name
        if (
            device_name != ""
            and component_manager.get_device(device_name) is None
        ):
            component_manager.add_device_to_lp(device_name)
            list_dev_names.append(device_name)
        device_name = self.tmc_sdp_sln_device_name
        if (
            device_name != ""
            and component_manager.get_device(device_name) is None
        ):
            component_manager.add_device_to_lp(device_name)
            list_dev_names.append(device_name)

        device_name = self.csp_subarray_dev_name
        if (
            device_name != ""
            and component_manager.get_device(device_name) is None
        ):
            component_manager.add_device_to_lp(device_name)
            list_dev_names.append(device_name)

        device_name = self.sdp_subarray_dev_name
        if (
            device_name != ""
            and component_manager.get_device(device_name) is None
        ):
            component_manager.add_device_to_lp(device_name)
            list_dev_names.append(device_name)

        return list_dev_names


class InputParameterLow(InputParameter):
    """
    Method to Check TMC subarray Low Devices
    """

    def __init__(self, changed_callback) -> None:
        super().__init__(changed_callback)
        self._tmc_mccs_sln_device_name = ""
        self._mccs_subarray_dev_name = ""
        self._changed_callback = changed_callback

    @property
    def tmc_mccs_sln_device_name(self):
        """
        Returns the MCCS Subarray Leaf Node device name

        :return: the MCCS Subarray Leaf Node device name
        :rtype: str
        """
        return self._tmc_mccs_sln_device_name

    @tmc_mccs_sln_device_name.setter
    def tmc_mccs_sln_device_name(self, value):
        """
        Sets the MCCS Subarray Leaf Node device name to be
        managed by the SubarrayNode

        :param value: the MCCS Subarray Leaf Node device name
        :type value: str
        """
        self._tmc_mccs_sln_device_name = value
        if self._changed_callback is not None:
            self._changed_callback()

    @property
    def mccs_subarray_dev_name(self):
        """
        Returns the MCCS Subarray device name

        :return: the MCCS Subarray device name
        :rtype: str
        """
        return self._mccs_subarray_dev_name

    @mccs_subarray_dev_name.setter
    def mccs_subarray_dev_name(self, value):
        """
        Sets the MCCS Subarray device name to be
        managed by the SubarrayNode

        :param value: the MCCS Subarray device name
        :type value: str
        """
        self._mccs_subarray_dev_name = value
        if self._changed_callback is not None:
            self._changed_callback()

    def update(self, component_manager):
        """
        This method updates list of devices for low input parameter class.
        """
        list_dev_names = super().update(component_manager)

        list_dev_names = []

        device_name = self.tmc_mccs_sln_device_name
        if device_name and component_manager.get_device(device_name) is None:
            component_manager.add_device_to_lp(device_name)
            list_dev_names.append(device_name)

        device_name = self.mccs_subarray_dev_name
        if device_name and component_manager.get_device(device_name) is None:
            component_manager.add_device_to_lp(device_name)
            list_dev_names.append(device_name)

        for dev_info in component_manager.devices:
            if dev_info.dev_name not in list_dev_names:
                component_manager.component.remove_device(dev_info.dev_name)


class InputParameterMid(InputParameter):
    """
    Method to Check TMC Subarray Mid Devices
    """

    def __init__(self, changed_callback) -> None:
        super().__init__(changed_callback)
        self._tmc_dish_ln_device_names = []
        self._dish_dev_names = []
        self.dish_ln_prefix = ""
        self.dish_master_identifier = ""
        self.dish_fqdn = []
        self._tmc_dish_ln_device_names: List[str] = []

    @property
    def tmc_dish_ln_device_names(self) -> List[str]:
        """
        Return the TM dish device names

        :return: the TM dish device names
        :rtype: list
        """
        return self._tmc_dish_ln_device_names

    @tmc_dish_ln_device_names.setter
    def tmc_dish_ln_device_names(self, value: List[str]) -> None:
        """
        Set the TM dish device names to be
        managed by the SubarrayNode

        :param value: the TM dish device names
        :type value: list
        """
        self._tmc_dish_ln_device_names = value
        if self._changed_callback is not None:
            self._changed_callback()

    @property
    def dish_dev_names(self) -> List[str]:
        """
        Return the dish device names

        :return: the TM dish device names
        :rtype: list
        """
        return self._dish_dev_names

    @dish_dev_names.setter
    def dish_dev_names(self, value: List[str]) -> None:
        """
        Set the dish device names to be
        managed by the SubarrayNode

        :param value: the TM dish device names
        :type value: list
        """

        self._dish_dev_names = value
        if self._changed_callback is not None:
            self._changed_callback()

    def update(self, component_manager) -> None:
        """
        Update input parameter for Input Parameter
        """
        list_dev_names = []
        list_dev_names = super().update(component_manager)

        if self.dish_dev_names != []:
            for device_name in self.dish_dev_names:
                logger.debug(
                    "Adding the dishes from the SubarrayNode component %s",
                    device_name,
                )
                if component_manager.get_device(device_name) is None:
                    component_manager.add_device_to_lp(device_name)
                    list_dev_names.append(device_name)
                    dish_number = "".join(
                        number for number in device_name if number.isdigit()
                    )[-3:]
                    for dish_leaf_node in self.tmc_dish_ln_device_names:
                        if (
                            dish_number in dish_leaf_node
                            and component_manager.get_device(dish_leaf_node)
                            is None
                        ):
                            component_manager.add_device_to_lp(dish_leaf_node)
                            list_dev_names.append(dish_leaf_node)
        else:
            logger.info("Removing the dishes from the SubarrayNode component.")
            component_manager.remove_dish_devices()
