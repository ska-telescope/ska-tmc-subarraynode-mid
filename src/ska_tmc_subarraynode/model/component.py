"""
This module is used for maintaining and monitoring the
components of subarray device.
"""
import logging
from typing import List

from ska_ser_logging import configure_logging
from ska_tango_base.control_model import HealthState, ObsState
from ska_tmc_common import PointingState
from ska_tmc_common.device_info import (
    DeviceInfo,
    DishDeviceInfo,
    SdpSubarrayDeviceInfo,
    SubArrayDeviceInfo,
)
from ska_tmc_common.v1.tmc_component_manager import TmcComponent
from tango import DevState

configure_logging()
LOGGER = logging.getLogger(__name__)

# pylint:disable=W1203


class SubarrayComponent(TmcComponent):
    """
    A component class for Subarray Node

    It supports:

    * Maintaining a connection to its component

    * Monitoring its component
    """

    def __init__(self, logger: logging.Logger = LOGGER):
        super().__init__(logger)
        self.logger = logger

        self._subarray_health_state = HealthState.UNKNOWN
        self._scan_id = ""
        self._sb_id = ""
        self._subarray_id = None
        self._scan_duration = 0
        self._assigned_resources = []
        self._subarray_availability = False
        self._update_device_callback = None
        self._update_subarray_health_state_callback = None
        self._update_assigned_resources_callback = None
        self._update_subarray_availability_status_callback = None

    def set_op_callbacks(
        self,
        _update_device_callback=None,
        _update_subarray_health_state_callback=None,
        _update_subarray_availability_status_callback=None,
    ):
        """this method update device callback and subarray health state"""
        self._update_device_callback = _update_device_callback
        self._update_subarray_health_state_callback = (
            _update_subarray_health_state_callback
        )
        self._update_subarray_availability_status_callback = (
            _update_subarray_availability_status_callback
        )

    def set_obs_callbacks(self, _update_assigned_resources_callback=None):
        """This method sets obs callback"""
        self._update_assigned_resources_callback = (
            _update_assigned_resources_callback
        )

    def invoke_device_callback(self, dev_info: DeviceInfo):
        """This method invoke device callback"""
        if self._update_device_callback is not None:
            self._update_device_callback(dev_info)

    def _invoke_subarray_health_state_callback(self):
        """This method update subarray health state callback"""
        if self._update_subarray_health_state_callback is not None:
            self._update_subarray_health_state_callback(
                self.subarray_health_state
            )

    def _invoke_assigned_resources_callback(self):
        """This method update assigned resources callback"""
        if self._update_assigned_resources_callback is not None:
            self._update_assigned_resources_callback(self.assigned_resources)

    def _invoke_subarray_availability_status_callback(self):
        if self._update_subarray_availability_status_callback is not None:
            self._update_subarray_availability_status_callback(
                self.subarray_availability
            )

    @property
    def devices(self) -> List[DeviceInfo]:
        """
        Return the monitored devices.

        :return: the monitored devices
        :rtype: DeviceInfo[]
        """
        return self._devices

    def get_device(self, device_name):
        """
        Return the monitored device info by name.

        :param device_name: name of the device
        :return: the monitored device info
        :rtype: DeviceInfo
        """
        for dev_info in self.devices:
            if device_name.lower() in dev_info.dev_name.lower():
                return dev_info
        return None

    def remove_device(self, device_names):
        """
        Remove a device from the list

        :param device_name: name of the device
        """
        try:
            for device in device_names:
                for dev_info in self.devices:
                    if device == dev_info.dev_name:
                        self.logger.info(
                            f"Device to be removed is: {dev_info.dev_name}"
                        )
                        self.devices.remove(dev_info)
        except Exception as exception:
            self.logger.exception(f"Unable to remove device: {exception}")

    def update_device(self, dev_info: DeviceInfo):
        """
        Update (or add if missing) Device Information
        into the list of the component.

        :param dev_info: a DeviceInfo object
        """
        dev_names_list = [device.dev_name for device in self._devices]
        if dev_info.dev_name not in dev_names_list:
            self.logger.debug(
                "Adding device info object for: %s", dev_info.dev_name
            )
            self._devices.append(dev_info)
        else:
            self.logger.debug(
                "Updating device info object for: %s", dev_info.dev_name
            )
            index = dev_names_list.index(dev_info.dev_name)
            self._devices[index] = dev_info

        if isinstance(dev_info, DishDeviceInfo):
            if "leaf" in dev_info.dev_name:
                log_msg = (
                    f"DishLeafNode {dev_info.dev_name} pointingState is:"
                    + f"{PointingState(dev_info.pointing_state).name}"
                )
            else:
                log_msg = (
                    f"Dish LMC {dev_info.dev_name} pointingState is:"
                    + f"{PointingState(dev_info.pointing_state).name}"
                )
            self.logger.debug(log_msg)
        elif isinstance(dev_info, SdpSubarrayDeviceInfo):
            log_msg = (
                f"SDP Subarray {dev_info.dev_name} obsState is: "
                + f"{ObsState(dev_info.obs_state).name}"
            )
            self.logger.debug(log_msg)

        elif isinstance(dev_info, SubArrayDeviceInfo):
            log_msg = (
                f"For {dev_info.dev_name} obsState is: "
                + f"{ObsState(dev_info.obs_state).name}"
            )
            self.logger.debug(log_msg)
        self.invoke_device_callback(dev_info)

    def update_device_exception(self, device_info: DeviceInfo, exception: str):
        """
        Update (or add if missing) Device
        Information into the list of the component.

        :param device_info: a DeviceInfo object
        """
        if device_info not in self._devices:
            device_info.update_unresponsive(True, exception)
            self._devices.append(device_info)
            self.invoke_device_callback(device_info)
        else:
            index = self._devices.index(device_info)
            int_device_info = self._devices[index]
            int_device_info.state = DevState.UNKNOWN
            int_device_info.update_unresponsive(True, exception)
            self.invoke_device_callback(int_device_info)

    @property
    def subarray_health_state(self):
        """
        Return the aggregated subarray health state

        :return: the subarray health state
        :rtype: HealthState
        """
        return self._subarray_health_state

    @subarray_health_state.setter
    def subarray_health_state(self, value):
        """
        Set the aggregated subarray health state

        :param value: the new subarray health state
        :type value: HealthState
        """
        if self._subarray_health_state != value:
            self._subarray_health_state = value
            self._invoke_subarray_health_state_callback()

    @property
    def scan_id(self):
        """
        Return the Scan id

        :return: the Scan id
        :rtype: str
        """
        return self._scan_id

    @scan_id.setter
    def scan_id(self, value):
        """
        Set the aggregated subarray observation state

        :param value: the new subarray observation state
        :type value: ObsState
        """
        if self._scan_id != value:
            self._scan_id = value

    @property
    def assigned_resources(self):
        """
        Return the resources assigned to the component.

        :return: the resources assigned to the component
        :rtype: list of str
        """
        return self._assigned_resources

    @assigned_resources.setter
    def assigned_resources(self, value):
        """
        Set the aggregated subarray observation state

        :param value: the new subarray observation state
        :type value: ObsState
        """
        if self._assigned_resources != value:
            self._assigned_resources = value
            self._invoke_assigned_resources_callback()

    @property
    def sb_id(self):
        """
        Return the Sb_id

        :return: the Sb_id
        :rtype: str
        """
        return self._sb_id

    @sb_id.setter
    def sb_id(self, value):
        """
        Set the aggregated subarray observation state

        :param value: the new subarray observation state
        :type value: ObsState
        """
        if self._sb_id != value:
            self._sb_id = value

    @property
    def subarray_id(self):
        """
        Return the subarray_id

        :return: the subarray_id
        :rtype: str
        """
        self.logger.info(f"Accessing subarray ID {self._subarray_id}")
        return self._subarray_id

    @subarray_id.setter
    def subarray_id(self, value):
        """
        Set the subarray_id

        :param value: the new the subarray_id
        :type value:
        """
        self.logger.info(f"Setting subarray ID to {value}")
        if self._subarray_id != value:
            self._subarray_id = value

    @property
    def scan_duration(self):
        """
        Return the duration of scan

        :return: the scan duration
        :rtype: int
        """
        return self._scan_duration

    @scan_duration.setter
    def scan_duration(self, value):
        """
        Set the duration of scan

        :param value: the duration of scan
        :type value: int
        """
        if self._scan_duration != value:
            self._scan_duration = value

    @property
    def subarray_availability(self):
        """
        Return the aggregated status for subarray node availability

        :return: the subarray availability
        :rtype: bool
        """
        return self._subarray_availability

    @subarray_availability.setter
    def subarray_availability(self, value: bool):
        """
        Set the subarray node availability value

        Args:
            :value : subarray node availability
            :type value: bool
        """
        if self._subarray_availability is not value:
            self._subarray_availability = value
            self.logger.info(
                "Updated subarray availability "
                + f"value:{self._subarray_availability}"
            )
            self._invoke_subarray_availability_status_callback()

    def to_dict(self):
        """Return result HealthState in dictionary"""
        devices = []
        for dev in self.devices:
            devices.append(dev.to_dict())
        result = {
            "subarray_health_state": str(
                HealthState(self.subarray_health_state)
            ),
            "devices": devices,
        }

        return result
